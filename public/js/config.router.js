'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {

          var layout = "ng/layout";
          if(window.location.href.indexOf("material") > 0){
            layout = "tpl/blocks/material.layout.html";
            $urlRouterProvider
              .otherwise('/app/dashboard-v3');
          }else{
            $urlRouterProvider
              .otherwise('/app/dashboard-v1');
          }
          $urlRouterProvider
              .otherwise('main');
          
          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/',
                  templateUrl: layout,
                  resolve: load(['toaster', 'js/controllers/toaster.js']),
              })
              .state('app.main', {
                  url: 'main',
                  templateUrl: 'ng/main',
                  cache: false,
                  resolve: load(['js/controllers/chart.js'])
              })
              .state('app.dashboard', {
                  url: 'dashboard',
                  templateUrl: 'ng/dashboard',
                  cache: false,
                  resolve: load(['js/controllers/chart.js'])
              })
              .state('app.ui', {
                  url: '/ui',
                  template: '<div ui-view class="fade-in-up"></div>'
              })
              .state('app.ui.buttons', {
                  url: '/buttons',
                  templateUrl: 'tpl/ui_buttons.html'
              })
              .state('app.ui.icons', {
                  url: '/icons',
                  templateUrl: 'tpl/ui_icons.html'
              })
              .state('app.ui.grid', {
                  url: '/grid',
                  templateUrl: 'tpl/ui_grid.html'
              })
              .state('app.ui.widgets', {
                  url: '/widgets',
                  templateUrl: 'tpl/ui_widgets.html'
              })          
              .state('app.ui.bootstrap', {
                  url: '/bootstrap',
                  templateUrl: 'tpl/ui_bootstrap.html'
              })
              .state('app.ui.sortable', {
                  url: '/sortable',
                  templateUrl: 'tpl/ui_sortable.html'
              })
              .state('app.ui.scroll', {
                  url: '/scroll',
                  templateUrl: 'tpl/ui_scroll.html',
                  resolve: load('js/controllers/scroll.js')
              })
              .state('app.ui.portlet', {
                  url: '/portlet',
                  templateUrl: 'tpl/ui_portlet.html'
              })
              .state('app.ui.timeline', {
                  url: '/timeline',
                  templateUrl: 'tpl/ui_timeline.html'
              })
              .state('app.ui.tree', {
                  url: '/tree',
                  templateUrl: 'tpl/ui_tree.html',
                  resolve: load(['angularBootstrapNavTree', 'js/controllers/tree.js'])
              })
              .state('app.ui.toaster', {
                  url: '/toaster',
                  templateUrl: 'tpl/ui_toaster.html',
                  resolve: load(['toaster', 'js/controllers/toaster.js'])
              })
              .state('app.ui.jvectormap', {
                  url: '/jvectormap',
                  templateUrl: 'tpl/ui_jvectormap.html',
                  resolve: load('js/controllers/vectormap.js')
              })
              .state('app.ui.googlemap', {
                  url: '/googlemap',
                  templateUrl: 'tpl/ui_googlemap.html',
                  resolve: load(['js/app/map/load-google-maps.js', 'js/app/map/ui-map.js', 'js/app/map/map.js'], function(){ return loadGoogleMaps(); })
              })
              .state('app.chart', {
                  url: '/chart',
                  templateUrl: 'tpl/ui_chart.html',
                  resolve: load('js/controllers/chart.js')
              })
              // table
              .state('app.table', {
                  url: '/table',
                  template: '<div ui-view></div>'
              })
              .state('app.table.static', {
                  url: '/static',
                  templateUrl: 'tpl/table_static.html'
              })
              .state('app.domains', {
                  url: 'domains',
                  templateUrl:"ng/table_domains",
                  cache: false,
                  params:{
                        id_portfolio: -1,
                        portfolio: 'All domains',
						tab: 'All'
                        //tab: 'Brandable'
                  },
                  controller: function($scope, $http, $compile, toaster){
                        
                        var id_portfolio = $scope.$state.params.id_portfolio;
                        var portfolio = $scope.$state.params.portfolio;
                        var tab = $scope.$state.params.tab;
                        
                        $scope.domains = [];
                        $scope.pagelinks = [];
                        $state.params.range = 0;
                        $state.params.loadingMessage = 'Loading records...';
						//$state.params.loadingMessage = '<img src="/static/admin/ajax-loader-bar.gif">';
                        $state.params.addDomainsModalState = '';
                        $state.params.addDomainsModalLabel = 'Submit';
                        $state.params.addDomainsErrorState = 'hidden';
                        
                        $scope.totalItems = 0;
                        $scope.currentPage = 1;
                        $scope.maxSize = 5;
                        
                        $scope.selectPage = function(pageNumber){
                                $scope.loadPage(pageNumber);
                        };
                        //$scope.updatePagination = function(currentPage, totalRecords){
						 $scope.updatePagination = function(currentPage, totalRecords, maxSize=5)	{
                                $scope.currentPage = currentPage;
                                $scope.totalItems = totalRecords;
								$scope.maxSize = maxSize;
								//console.log($scope.maxSize);
								if( $scope.maxSize > 1 )	{
									$('.pagination').attr('style','visibility:block;');
								}
								else	{
									$('.pagination').attr('style','visibility:hidden;');
								}
                        };
                        $scope.searchDomain = function()	{
                                
                                var keyword = $('#domain-search-input').val().trim();
                                if(keyword)	{
									setTimeout($scope.loadPage($scope.currentPage, keyword), 2000);
								}
                        };
                        $scope.loadPage = function(page, keyword = '')	{
								if( $state.params.id_portfolio == -1 )	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domains_port_all').attr('class','active');
								}
								else	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domains_port_main').attr('class','active');
									$('#li_domains_port_'+$state.params.id_portfolio).attr('class','active');
								}
								
								var view_limit = $('#view_limit').val();
								var view_publish = $('#view_publish').val();
								var view_status = $('#view_status').val();
								var maxSize = 5;
								var currentPage = 1;
								var sortby = $('#sortby').val();
								var sortorder = $('#sortorder').val();
								
                                //Initial values...
								$('.overlay').show();
                                $state.params.loadingMessage = 'Loading records...';
								//$state.params.loadingMessage = '<img src="/static/admin/ajax-loader-bar.gif">';
								$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
                                $scope.domains = [];
                                
                                //var url = 'ng/json/domains?action=fetch_domain_records&id_portfolio='+$state.params.id_portfolio+'&tab='+$state.params.tab+'&page='+page;
								//var url = 'ng/json/domains?action=fetch_domain_records&id_portfolio='+$state.params.id_portfolio+'&tab='+$state.params.tab+'&page='+page+'&limit='+view_limit;
								var url = 'ng/json/domains?action=fetch_domain_records&id_portfolio='+$state.params.id_portfolio+'&tab='+$state.params.tab+'&page='+page+'&limit='+view_limit+'&sortby='+sortby+'&sortorder='+sortorder+'&published='+view_publish+'&status='+view_status;
								
								$('#domain-search-input').attr('disabled','disabled');
								$('#domain-search-button').attr('disabled','disabled');
								//$('#load_searchbox').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
								
                                if(keyword)	{
                                        url = url + '&search='+keyword.trim();
										
										//$('#domain-search-input').removeAttr('disabled'); //uncomment this when keyup
										//$('#domain-search-button').removeAttr('disabled'); //uncomment this when keyup
										
										//$('#load_searchbox').html('<input id="domain-search-input" type="search" class="form-control input-sm" placeholder="Search" onkeyup="searchDomain();" >');
										$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
								}
                                $http({
                                        method: 'get',
                                        url: url
                                }).success(function(data, status, headers, config)	{
									
										$scope.getPortfolioCounts();
									
										console.log( data );
									if( data.result_code != undefined )	{
										if( data.result_code == 0 )	{
											if(data.formatted_records)	{
												//Populate current page
												$scope.domains = data.formatted_records;
												//$scope.domains.push($scope.domains);
												
												//console.log(data.formatted_records.length);
												//console.log($scope.domains.length);
												//console.log($scope.domains);
												
												//Set variables
												//$state.params.range = '('+(25 * (page -1))+' - '+(25 * (page))+') / '+(data.rows)+' records';
												if( view_limit != 'all')	{
													if( view_limit > 25 )	{
														$state.params.range = '('+(view_limit * (page -1))+' - '+(view_limit * (page))+') / '+(data.rows)+' records';
													}
													else	{
														$state.params.range = '('+(25 * (page -1))+' - '+(25 * (page))+') / '+(data.rows)+' records';
													}
													maxSize = 5;
												}
												else	{
													$state.params.range = 'All records';
													page = 1;
													maxSize = 1;
												}
												$state.params.loadingMessage = $scope.$state.params.portfolio;												
												$('#load_list_loading_image').html('');
												
												if( data.rows == $scope.domains.length )	{
													$state.params.range = 'All records';
													page = 1;
													maxSize = 1;
												}
												
												//update Pagination
												//$scope.updatePagination(page, data.rows);
												 $scope.updatePagination(page, data.rows, maxSize);
												
												//console.log( $scope.domains[0] );
												
												setTimeout(function()	{
													//if( $scope.domains.length < 1 )	{
														//$('#forBtnDeletePortfolio').text( $scope.domains[0].portfolio_title );
														
														//$('#forBtnDeletePortfolio').html( '<a title="All" class="btn btn-default btn-rounded"><i class="fa fa-remove"></i> Remove This Portfolio</a>' );
													//}
																										
													$.each( data.formatted_records, function( i, field ) {
														//console.log(field.published);
														//console.log(field);
														if( field.published == 'Y' )	{
															var pub_str = '<i alt="Published" title="Published" onclick="updatePublished(\''+field.id_domain+'\',\'N\')" style="cursor:pointer;" class="fa fa-check text-success" ></i>';
														}
														else	{
															var pub_str = '<i alt="Unpublished" title="Unpublished" onclick="updatePublished(\''+field.id_domain+'\',\'Y\')" style="cursor:pointer;" class="fa fa-close text-danger" ></i>';
														}
														$('#col-pub-'+field.id_domain).html(pub_str);

														var edit_price_str = ' \
															<div class="form-inline form-group"> \
															<form id="frmEditPrice-'+field.id_domain+'">\
																<input id="edit-price-domainID-'+field.id_domain+'" name="edit-price-domainID-'+field.id_domain+'" value="'+field.id_domain+'" type="hidden"> \
																<input id="edit-price-domainPrice-'+field.id_domain+'" id="edit-price-domainPrice-'+field.id_domain+'" value="'+data.record[i].price+'" class="form-control" size="10" type="text"> \
																<input id="edit-price-domainName-'+field.id_domain+'" id="edit-price-domainName-'+field.id_domain+'" value="'+data.record[i].domain+'" class="form-control" type="hidden"> \
																<button type="button" alt="Close" title="Close" onclick="$(\'#divFrmEditPrice-'+field.id_domain+'\').hide(\'slow\');$(\'#label-price-'+field.id_domain+'\').show(\'slow\');" class="btn btn-danger btn-xs"> <i class="fa fa-close"></i> </button> \
																<button type="button" alt="Save New Price" title="Save New Price" onclick="saveNewPrice(\''+field.id_domain+'\')" class="btn btn-success btn-xs"> <i class="fa fa-save"></i> </button> \
															</form>\
															</div> \
															';
														$('#divFrmEditPrice-'+field.id_domain).html(edit_price_str);
														
														$('#col-pricebtn-'+field.id_domain).html('<i data-toggle="tooltip" data-placement="top" alt="Edit Price" title="Edit Price" onclick="$(\'#divFrmEditPrice-'+field.id_domain+'\').show(\'slow\');$(\'#label-price-'+field.id_domain+'\').hide(\'slow\');" class="glyphicon glyphicon-edit cursor-pointer text-success" style="cursor:pointer;margin-left:10px;"></i>');
														
														var request_logo_design = '';
														if( parseInt(field.id_request) > 0 )	{
															if(field.request_status == 'U')	{
																request_logo_design += '\
																	<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+field.id_request+'\' })" href="#/requestlogodetails/?idRequest='+field.id_request+'">\
																		<i data-toggle="tooltip" data-placement="top" alt="Unassigned" title="Unassigned" class="fa fa-user-times cursor-pointer text-info" style="margin-left:10px;"></i>\
																	</a>\
																';
															}
															else if(field.request_status == 'A')	{
																request_logo_design += '\
																	<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+field.id_request+'\' })" href="#/requestlogodetails/?idRequest='+field.id_request+'">\
																		<i data-toggle="tooltip" data-placement="top" alt="Assigned" title="Assigned" class="fa fa-user-circle cursor-pointer text-info" style="margin-left:10px;"></i>\
																	</a>\
																';
															}
															else if(field.request_status == 'P')	{
																request_logo_design += '\
																	<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+field.id_request+'\' })" href="#/requestlogodetails/?idRequest='+field.id_request+'">\
																		<i data-toggle="tooltip" data-placement="top" alt="Pending" title="Pending" class="fa fa-bar-chart cursor-pointer text-info" style="margin-left:10px;"></i>\
																	</a>\
																';
															}
															else if(field.request_status == 'AP')	{
																request_logo_design += '\
																	<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+field.id_request+'\' })" href="#/requestlogodetails/?idRequest='+field.id_request+'">\
																		<i data-toggle="tooltip" data-placement="top" alt="Approved" title="Approved" class="fa fa-thumbs-up cursor-pointer text-info" style="margin-left:10px;"></i>\
																	</a>\
																';
															}
															else if(field.request_status == 'R')	{
																request_logo_design += '\
																	<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+field.id_request+'\' })" href="#/requestlogodetails/?idRequest='+field.id_request+'">\
																		<i data-toggle="tooltip" data-placement="top" alt="Rejected" title="Rejected" class="fa fa-thumbs-down cursor-pointer text-info" style="margin-left:10px;"></i>\
																	</a>\
																';
															}
															else if(field.request_status == 'C')	{
																request_logo_design += '\
																	<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+field.id_request+'\' })" href="#/requestlogodetails/?idRequest='+field.id_request+'">\
																		<i data-toggle="tooltip" data-placement="top" alt="Completed" title="Completed" class="fa fa-check cursor-pointer text-info" style="margin-left:10px;"></i>\
																	</a>\
																';
															}
															else	{
																request_logo_design += '\
																	<i onclick="requestLogoDesign(\''+field.id_domain+'\',\''+field.domain+'\')" data-toggle="tooltip" data-placement="top" alt="Request Logo Design" title="Request Logo Design" class="glyphicon glyphicon-edit cursor-pointer text-success" style="margin-left:10px;"></i>\
																';
															}
														}
														else	{ 
																request_logo_design += '\
																	<i onclick="requestLogoDesign(\''+field.id_domain+'\',\''+field.domain+'\')" data-toggle="tooltip" data-placement="top" alt="Request Logo Design" title="Request Logo Design" class="glyphicon glyphicon-edit cursor-pointer text-success" style="margin-left:10px;"></i>\
																';
														}
														$('#divFrmRequestLogoDesign-'+field.id_domain).html(request_logo_design);
														
														var stat_disp_str = '';
														
														
														//console.log( field.domain + ' = ' + parseInt(data.record[i].price) + ' = ' + data.record[i].price );
														//console.log( field.domain + ' = ' + parseInt(field.price) );
														//console.log( field.domain + ' = ' + parseInt(field.price) );
														//console.log( field.domain + ' = ' + isNaN(parseInt(field.price)) );
														
														//if( field.status == 1 )	{

														

														if( field.IPNstatus != undefined || field.IPNstatus.trim() != '' || field.IPNstatus != null || field.IPNstatus.trim() != 'null'  )	{
															
															//console.log( field.domain + ' = ' + field.IPNstatus);
															
															var order_status = $.parseJSON(field.IPNstatus);
															if( order_status != null || order_status != undefined ||  order_status != 'null' || order_status != '')	{
																var order_status_code = order_status.Code;
																var order_status_desc = order_status.Description;
																var status_code = order_status.Code;
																var transactionID = order_status.EscrowUniqueIdentifier;
																var escrow_status = order_status.Description;
															}
															else	{
																var order_status_code = -1;
																var order_status_desc = '';
																var status_code = '';
																var transactionID = '';
																var escrow_status = '';
															}
														}
														else	{
															var order_status_code = -1;
															var order_status_desc = '';															
															var status_code = '';
															var transactionID = '';
															var escrow_status = '';
														}
														
														//console.log( field.domain + ' = ' + order_status_code);
														
														if( order_status_code > -1  )	{
															if( order_status_code == 0 && parseInt(data.record[i].price) > 0 )	{
																stat_disp_str = '<span class="forsaleholder">For Sale</span>';
															}
															else if( order_status_code == 0 && parseInt(data.record[i].price) < 1 )	{
																stat_disp_str = '<span class="offerholder">Offer</span>';
															}
															else if( order_status_code == 25 )	{
																stat_disp_str = '<span class="soldholder">Sold</span>';
															}
															else	{
																if( status_code == 5 )	{
																	var labeltitle = 'Review Required';
																}
																else if( status_code == 15 )	{
																	var labeltitle = 'Waiting for Buyer\'s Payment';
																}
																else if( status_code == 20 )	{
																	var labeltitle = 'Buyer Have Submitted a Payment';
																}
																stat_disp_str = '<span data-toggle="tooltip" data-placement="top" title=" '+labeltitle+'. Click to view more info. " style="cursor:pointer;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\',\''+field.buyerfullname+'\',\''+field.buyeremail+'\',\''+field.date_ordered+'\',\''+field.domain+'\')" class="pendingholder">Pending</span>';
															}
														}
														else	{
															if( parseInt(data.record[i].price) > 0 )	{
																stat_disp_str = '<span class="forsaleholder">For Sale</span>';
															}
															if( parseInt(data.record[i].price) < 1 || data.record[i].price.trim() == '' )	{
																stat_disp_str = '<span class="offerholder">Offer</span>';
															}
														}
														
														var stat_edit_btn = ' \
														<i alt="Edit Status" title="Edit Status" onclick="$(\'#divFrmEditStatus-'+field.id_domain+'\').show(\'slow\');$(\'#label-status-'+field.id_domain+'\').hide(\'slow\');" class="glyphicon glyphicon-edit cursor-pointer text-success" style="cursor:pointer;margin-left:10px;"></i> \
														';
														
														var edit_frm_status_str = ' \
															<div class="form-inline form-group"> \
																<div class="btn-group dropdown pull-left" style="margin-right:5px;"> \
																	<button type="button" class="btn btn-default" data-toggle="dropdown"> \
																		<span id="btn_drop_stat"> Select </span> \
																		<span class="caret"></span> \
																	</button> \
																	<ul class="dropdown-menu"> \
																		<li><a>For Sale</a></li> \
																		<li><a>Offer</a></li> \
																		<li><a>SOLD</a></li> \
																	</ul> \
																</div> \
																<button type="button" alt="Close" title="Close" onclick="$(\'#divFrmEditStatus-'+field.id_domain+'\').hide(\'slow\');$(\'#label-status-'+field.id_domain+'\').show(\'slow\');" class="btn btn-danger btn-xs"> <i class="fa fa-close"></i> </button> \
																<button type="button" alt="Save New Status" title="Save New Status" onclick="alert(\'Coming Soon\')" class="btn btn-success btn-xs"> <i class="fa fa-save"></i> </button> \
															</div>\
														';
														
														
														//stat_disp_str = stat_disp_str + stat_edit_btn;
														
														//$('#domain-status-'+field.id_domain).html(stat_disp_str);
														//$('#col-statusbtn-'+field.id_domain).html(stat_edit_btn);
														//$('#divFrmEditStatus-'+field.id_domain).html(edit_frm_status_str);
														
														var stat_sold_btn = ' \
														<i data-toggle="tooltip" data-placement="top" alt="Click to mark as Sold" title="Click to mark as Sold" onclick="markSold(\''+field.id_domain+'\',\''+field.domain+'\')" class="glyphicon glyphicon-check cursor-pointer text-info" style="cursor:pointer;margin-left:10px;"></i>';
														
														stat_disp_str = stat_disp_str + stat_sold_btn;
														
														$('#col-status-'+field.id_domain).html(stat_disp_str);
														$('[data-toggle=tooltip]').tooltip();
													});
													
													
													$('.double-scroll').doubleScroll({
														onlyIfScroll: true,
														resetOnWindowResize: true
													});

													$('.overlay').hide();
												}, 1000);
												
											}
                                        }
										else	{
											//Set variables
											$state.params.range = 'No records found';
											$state.params.loadingMessage = $scope.$state.params.portfolio;
											$('#load_list_loading_image').html('');
											
											//update Pagination to first page/no records.
											$scope.updatePagination(1, 0);
											$('.overlay').hide();

                                        }
										
										setTimeout(function()	{
											if( data.domain_count_tab != undefined )	{
												// load the domain count for each tab
												$('#all_cnt').text( '(' + data.domain_count_tab.all + ')' );
												$('#brandable_cnt').text( '(' + data.domain_count_tab.brandable + ')' );
												$('#Premium_cnt').text( '(' + data.domain_count_tab.premium + ')' );
												$('#Under1000_cnt').text( '(' + data.domain_count_tab.budget + ')' );
												//$('#menu-count-all').text( data.domain_count_tab.all );
												//$('#menu-count-sold').text( data.domain_count_tab.sold );
												
												if( $state.params.id_portfolio == -1 )	{
													$('#sidebar_menu li').removeAttr('class');
													$('#li_domains').attr('class','active');
													$('#li_domains_port_all').attr('class','active');
												}
												else	{
													$('#sidebar_menu li').removeAttr('class');
													$('#li_domains').attr('class','active');
													$('#li_domains_port_main').attr('class','active');
													$('#li_domains_port_'+$state.params.id_portfolio).attr('class','active');
												}
												
											}
										}, 1000);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
										
										//$('.overlay').hide();
										$('#domain-search-input').removeAttr('disabled');
										$('#domain-search-button').removeAttr('disabled');
										//$('#load_searchbox').html('<input id="domain-search-input" type="search" class="form-control input-sm" placeholder="Search" onkeyup="searchDomain();" >');
										
										$('.imgsort').html('<img src="img/sort_both.png">');
										if( sortorder == 'ASC' )	{
											$('#imgsort_'+sortby).html('<img src="img/sort_asc.png">');
										}
										if( sortorder == 'DESC' )	{
											$('#imgsort_'+sortby).html('<img src="img/sort_desc.png">');
										}
										
										setTimeout(function()	{	
											console.log('test james');
											console.log(data);
											//console.log(data.formatted_records.length);
											
											var portfolio_buttons = '';
												//portfolio_buttons += '<a onclick="$(\'#edit-portfolio-modal\').modal();" class="btn btn-default btn-rounded"><i class="fa fa-edit"></i> Edit Portfolio Name</a>';
											
											if( parseInt($state.params.id_portfolio) > 0 )	{
												
												$('#selportfolioID').val( parseInt($state.params.id_portfolio) );
												$('#selportfolioID').attr( 'readonly', 'readonly' );
												
												//if( data.formatted_records == null )	{
												if( data.portfolio.portfolio.domaincount < 1 )	{
													portfolio_buttons += '<a onclick="$(\'#edit-portfolio-modal\').modal();" class="btn btn-default btn-rounded"><i class="fa fa-edit"></i> Portfolio : <b>'+data.portfolio.portfolio.title+'</b> </a>';
													portfolio_buttons += '<a onclick="$(\'#btnDeletePortfolio\').click();" class="btn btn-default btn-rounded"><i class="fa fa-remove"></i> Remove This Portfolio</a>';
													//$('#frmPortfolioInfoDiv').html( '<a onclick="$(\'#btnDeletePortfolio\').click();" class="btn btn-default btn-rounded"><i class="fa fa-remove"></i> Remove This Portfolio</a>' );
													$('#frmPortfolioInfoDiv').html( portfolio_buttons );
													//$('#btnDeletePortfolio').attr('style','display:block;');
													$('#portfolio_name').val(data.portfolio.portfolio.title);
													$('#portfolio_id').val(data.portfolio.portfolio.id_portfolio);
													$('#portfolio_iduser').val(data.portfolio.portfolio.id_user);
													//$('#portfolio_cnt_domain').val(data.formatted_records.length);
													
													//for form frmPortfolioEdit
													$('#pfolio_title').val(data.portfolio.portfolio.title);
													$('#pfolio_id').val(data.portfolio.portfolio.id_portfolio);
													$('#pfolio_iduser').val(data.portfolio.portfolio.id_user);
												}
												else	{
													
													portfolio_buttons += '<a onclick="$(\'#edit-portfolio-modal\').modal();" class="btn btn-default btn-rounded"><i class="fa fa-edit"></i> Portfolio : <b>'+data.portfolio.portfolio.title+'</b> </a>';
													
													//$('#frmPortfolioInfoDiv').html( '' );
													$('#frmPortfolioInfoDiv').html( portfolio_buttons );
													//$('#btnDeletePortfolio').attr('style','display:none;');
													$('#portfolio_name').val(data.portfolio.portfolio.title);
													$('#portfolio_id').val(data.portfolio.portfolio.id_portfolio);
													$('#portfolio_iduser').val(data.portfolio.portfolio.id_user);
													//$('#portfolio_cnt_domain').val(data.formatted_records.length);
													
													//for form frmPortfolioEdit
													$('#pfolio_title').val(data.portfolio.portfolio.title);
													$('#pfolio_id').val(data.portfolio.portfolio.id_portfolio);
													$('#pfolio_iduser').val(data.portfolio.portfolio.id_user);
												}
											}
											else	{
												$('#selportfolioID').val(0);
												$('#selportfolioID').removeAttr( 'readonly' );
											}
										}, 1000);
										
                                });
                        };
						
						$scope.addDomainModal = function()	{
							$('#addNewDomains-modal').modal();
							$('#add_domain_ajax_result').hide();
							var cur_portfolio_id = $('#portfolio_id').val();
							var str_sel_read = '';
							if( cur_portfolio_id > 0 )	{
								str_sel_read = ' readonly="readonly" ';
							}
							var str_opt_port = '';
							
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=addDomainModal',
								data: {},
							}).success(function(data, status, headers, config){
								console.log( data );
								
								setTimeout(function()	{
									if( data.user != undefined )	{
										$('#notification_email').val(data.user.info.email);
									}
									
									if( data.portfolio != undefined )	{
										if( data.portfolio.record != undefined )	{
											if( data.portfolio.record.length > 0 )	{
												if( cur_portfolio_id > 0 )	{
													var port_title = '';
													var str_opt_html = '\
															<label> Portfolio </label> \
															<input type="hidden" value="'+cur_portfolio_id+'" id="selportfolioID" name="selportfolioID" >';
															for( var i=0; i<data.portfolio.record.length; i++ )	{
																if( cur_portfolio_id == data.portfolio.record[i].id_portfolio )	{
																	port_title = data.portfolio.record[i].title;
																}
															}
														str_opt_html +=	'<input disabled class="form-control" readonly type="text" value="'+port_title+'" id="selportfolioTitle" name="selportfolioTitle" >';
												}
												else	{
													var str_opt_html = '\
														<label> Portfolio </label> \
														<select '+str_sel_read+' id="selportfolioID" name="selportfolioID" class="form-control" > \
															<option value="0"> Default </option>';
															for( var i=0; i<data.portfolio.record.length; i++ )	{
																if( cur_portfolio_id > 0 )	{
																	if( cur_portfolio_id == data.portfolio.record[i].id_portfolio )	{
																		str_opt_port = ' selected="selected" ';
																	}
																	else	{
																		str_opt_port = ' ';
																	}
																}
																else	{
																	str_opt_port = ' ';
																}
																
																str_opt_html += '<option value="'+data.portfolio.record[i].id_portfolio+'" '+str_opt_port+' >'+data.portfolio.record[i].title+'</option>';
															}
													str_opt_html += ' </select> ';
												}
												$('#display_Porfolio').html(str_opt_html);
											}
										}
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
								}, 1000);
							});
							return false;
							
						};
						
						$scope.deletePortfolio = function()	{
							var portfolio_id = $('#portfolio_id').val();
							var portfolio_name = $('#portfolio_name').val();
							//alert(portfolio_id);
							if(confirm("Are you sure you want to remove this portfolio '"+portfolio_name+"'?"))	{
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=deletePortfolio',
									data: getFormData( $("#frmPortfolioInfo") )
								}).success(function(data, status, headers, config){
									setTimeout(function()	{
										//$('#btnRefresh').click();
										//$('.overlay').hide();
										if( data.result != undefined)	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										//var json_data = $.parseJSON(data);
										//console.log( json_data );
										console.log( data );
										window.location.reload();
									}, 1000);
								});
								return false;
							}
							
						};
						$scope.saveEditPortfolio = function()	{
							if(  $('#pfolio_title').val().trim() == '' )	{
								alert('Please provide a valid portfolio name.');
								$('#pfolio_title').attr('style','border-color:#d9534f;');
								$('#pfolio_title').focus();
							}
							else	{
								$('#pfolio_title').attr('style','border-color:none;');
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=saveEditPortfolio',
									data: getFormData( $("#editPortfolioForm") )
								}).success(function(data, status, headers, config)	{
									console.log( data );
									setTimeout(function()	{
										if( data.rdata != undefined)	{
											$('#menu_portfolio_'+data.rdata.id_portfolio).text(data.rdata.portfolio_name);
										}
										
										if( data.result != undefined)	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										$('#btnRefresh').click();
										$('.overlay').hide();
										$('#edit-portfolio-modal').modal('hide');
									}, 1000);
								});
							}
							return false;
						};
						$scope.uploadLogo = function(){
						};
						$scope.saveDomainNewPrice = function()	{
							var domainID = $('#edit_price_domainID').val();
							var domainName = $('#edit_price_domainName').val();
							if( $('#edit-price-domainPrice-'+domainID).val().trim() == '' )	{
								alert('Please provide a valid price for '+domainName+'');
								$('#edit-price-domainPrice-'+domainID).attr('style','border-color:#d9534f;');
								$('#edit-price-domainPrice-'+domainID).focus();
							}
							else if( parseInt($('#edit-price-domainPrice-'+domainID).val()) < 0 )	{
								alert('Price can\'t have a negative value. Please provide a valid price for '+domainName+'');
								$('#edit-price-domainPrice-'+domainID).attr('style','border-color:#d9534f;');
								$('#edit-price-domainPrice-'+domainID).focus();
							}
							else	{
								$('#edit-price-domainPrice-'+domainID).attr('style','border-color:none;');
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=save_domain_new_price',
									data: getFormData( $("#frmSaveNewPrice") )
								}).success(function(data, status, headers, config)	{
									console.log( data );
									setTimeout(function()	{
										//$('#btnRefresh').click();
										
										if( data.result != undefined)	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											if( data.result == 'SUCCESS')	{
												$('#domain-price-'+domainID).html(data.formatted_price);
												$('#edit-price-domainPrice-'+domainID).val(data.price);
												$('#divFrmEditPrice-'+domainID).hide('slow');
												$('#label-price-'+domainID).show('slow');
												if( $('#col-status-'+domainID).text() != 'Pending' )	{
													if( data.price > 0 )	{
														$('#col-status-'+domainID).html('<span class="forsaleholder">For Sale</span>');
													}
													else	{
														$('#col-status-'+domainID).html('<span class="offerholder">Offer</span>');
													}
												}
											}
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										$('.overlay').hide();
										//$('#btnRefresh').click();
									}, 1000);
								});
							}
							return false;
						};
						$scope.saveDomainPublished = function()	{
							var domainID = $('#up_domain_id').val();
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_published',
								data: getFormData( $("#frmUpdatePublished") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									//$('#btnRefresh').click();
									if( data.result != undefined)	{
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}

									//$('.overlay').hide();
									$('#btnRefresh').click();
								}, 1000);
							});
							return false;
						};
						$scope.checkAllProperties = function(domID)	{
							var tr_type = $('#tr-type-'+domID).attr('class');
							var tr_price = $('#tr-price-'+domID).attr('class');
							var tr_portfolio = $('#tr-portfolio-'+domID).attr('class');
							var tr_logo = $('#tr-logo-'+domID).attr('class');
							var tr_category = $('#tr-category-'+domID).attr('class');
							
							//if( tr_type === undefined && tr_price === undefined && tr_portfolio === undefined && tr_logo === undefined && tr_category === undefined )	{
							if( tr_price === undefined && tr_portfolio === undefined && tr_logo === undefined && tr_category === undefined )	{
								$('#tr-bydomain-'+domID).remove();
								
								var cnt_upd_checkbox = $('.chk_upd_domainID').length;
								$('#upd_cnt_domain').text( cnt_upd_checkbox);
								
								if( cnt_upd_checkbox < 1 )	{
									$('#update_buttons').remove();
								}
								
							}
						}
						$scope.saveDomainPrice = function()	{
							var domainID = $('#up_domain_id').val();
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_price',
								data: getFormData( $("#frmUpdateDomainPrice") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									//$('#btnRefresh').click();
									$('#tr-price-'+domainID).remove();
									$scope.checkAllProperties(domainID);
									
									if( data.result != undefined)	{
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									
									$('.overlay').hide();
								}, 1000);
							});
							return false;
						};
						$scope.saveDomainPortfolio = function()	{
							var domainID = $('#up_port_domain_id').val();
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_portfolio',
								data: getFormData( $("#frmUpdateDomainPortfolio") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									//$('#btnRefresh').click();
									if( data.result != undefined)	{
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										$('#tr-portfolio-'+domainID).remove();
										$scope.checkAllProperties(domainID);
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
								}, 1000);
							});
							return false;
						};
						$scope.saveDomainCategory = function()	{
							var domainID = $('#up_cat_domain_id').val();
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_category',
								data: getFormData( $("#frmUpdateDomainCategory") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									//$('#btnRefresh').click();
									$('#tr-category-'+domainID).remove();
									$scope.checkAllProperties(domainID);
									
									if( data.result != undefined)	{
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									
									$('.overlay').hide();
								}, 1000);
							});
							return false;
						};
						$scope.saveUpdateDomainLogo = function()	{
							var domainID = $('#up_logo_domain_id').val();
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_logo',
								data: getFormData( $("#frmUpdateDomainLogo") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									//$('#btnRefresh').click();
									$('#tr-logo-'+domainID).remove();
									$scope.checkAllProperties(domainID);
									if( data.result != undefined)	{
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									
									$('.overlay').hide();
								}, 1000);
							});
							return false;
						};
						$scope.saveByDomain = function()	{
							var domainID = $('#up_save_by_domain_id').val();
							//console.log(domainID);
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_by_domain',
								data: getFormData( $("#frmUpdateByDomain") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									//$('#btnRefresh').click();
									$('#tr-bydomain-'+domainID).remove();
									$scope.checkAllProperties(domainID);
									if( data.result != undefined)	{
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									
									$('.overlay').hide();
								}, 1000);
							});
							return false;
						};
						$scope.saveAllDomainSelected = function()	{
							var up_all_domain_ids = $('#up_all_domain_ids').val().trim();
							console.log(up_all_domain_ids);

							//var up_all_domain_ids = '3172,3173';
							var domainIDs = up_all_domain_ids.split(",");
							console.log(domainIDs);
							
							if( domainIDs.length < 1 || up_all_domain_ids == '' || up_all_domain_ids == null)	{
								alert('Please select a domain or check the checkboxes.');
							}
							else	{
								var obj = {};
								
								obj['domain_ids'] = up_all_domain_ids;
								
								for(var i=0; i<domainIDs.length; i++) {
									if( $('#aPrice-'+domainIDs[i]).val() != undefined ) {
										obj[''+domainIDs[i]+'-price'] = $('#aPrice-'+domainIDs[i]).val();
									}
									if( $('#aType-'+domainIDs[i]).val() != undefined ) {
										obj[''+domainIDs[i]+'-type'] = $('#aType-'+domainIDs[i]).val();
									}
									if( $('#aPortfolioID-'+domainIDs[i]).val() != undefined ) {
										obj[''+domainIDs[i]+'-portfolioID'] = $('#aPortfolioID-'+domainIDs[i]).val();
									}
									if( $('#aLogo-'+domainIDs[i]).val() != undefined ) {
										obj[''+domainIDs[i]+'-id_logo'] = $('#aLogo-'+domainIDs[i]).val();
									}
									if( $('#aDomain-'+domainIDs[i]).val() != undefined ) {
										obj[''+domainIDs[i]+'-domain'] = $('#aDomain-'+domainIDs[i]).val();
									}
									if( $('#aCategoryID-'+domainIDs[i]).val() != undefined ) {
										obj[''+domainIDs[i]+'-category'] = $('#aCategoryID-'+domainIDs[i]).val();
									}
								}

								//console.log(domainID);
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=update_all_domain',
									data: obj
									//data: getFormData( $("#frmUpdateAllDomain") )
								}).success(function(data, status, headers, config)	{
									console.log( data );
									setTimeout(function()	{
										//$('#btnRefresh').click();
										for(var i=0; i<domainIDs.length; i++) {
											$('#tr-bydomain-'+domainIDs[i]).remove();
											$scope.checkAllProperties(domainIDs[i]);
										}
										
										if( data.result != undefined)	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										$('.overlay').hide();
									}, 1000);
								});
							}
							return false;
						};
						$scope.saveCorrectedDomain = function()	{
							var ind = $('#up_corr_ind').val();
							var input_domain = $('#corr_domain-'+ind).val();
							//console.log(domainID);
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=update_corrected_domain',
								data: getFormData( $("#frmCorrectedDomain") )
							}).success(function(data, status, headers, config)	{
								console.log( data );
								setTimeout(function()	{
									if( data.result != undefined)	{
										//toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										alert( data.result + ' : '+input_domain+'  ' + data.response_details );
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									
									if( data.result == 'SUCCESS')	{
										$('#tr-correcteddomain-'+ind).remove();
										$('#btnRefresh').click();
									}
									else	{
										$('.overlay').hide();
									}
								}, 1000);
							});
							return false;
						};
                        $scope.importDomains = function()	{
                                $state.params.addDomainsModalState = 'disabled';
                                $state.params.addDomainsModalLabel = 'processing...';
                                $state.params.addDomainsErrorState = 'hidden';

                                var $addDomainsModal = $('#addNewDomains-modal');
                                $addDomainsModal.find('[name=txt_domain]').removeClass('text-success')
                                                                                .removeClass('text-danger')
                                                                                .parent().removeClass('has-error');
                                //Prepare text domains value
                                var textDomains = $addDomainsModal.find('[name=txt_domain]').val().trim()? $addDomainsModal.find('[name=txt_domain]').val().split(','): [];
                                for(var i in textDomains)
                                        textDomains[i] = textDomains[i].trim().toLowerCase();

                                //$addDomainsModal.find('[name=_file]').val().trim()? $addDomainsModal.find('[name=_file]').val(): null;
                                var zipDomains = $addDomainsModal.find('[name=_file]')[0].files[0]; //Get first file...
                                if(zipDomains){
                                       
									if( !$scope.ValidateEmail( $('#notification_email').val() )  )	{
										alert('Please provide a valid notification email.');
										$('#notification_email').attr('style','border-color:#d9534f;');
										$('#notification_email').focus();
										$state.params.addDomainsModalState = '';
									}
									else	{
										$('#notification_email').attr('style','border-color:none;');
										$state.params.addDomainsModalState = '';
										
										var formData = new FormData();
										formData.append("_file", zipDomains);
										//formData.append("id_portfolio", 13);
										
										/*
										$("#addnewDomainsForm").serializeArray().forEach(function(field) {
											formData.append(field.name, field.value);
										});​
										*/
										/*
										var fields = $("#addnewDomainsForm").serializeArray();

										$.each( fields, function( i, field ) {
											formData.append(field.name, field.value);
										});
										*/
										//formData.append("id_portfolio", $('#portfolio_id').val());
										
										//console.log(formData);
										
										
										//formData.append("replace_records", $addDomainsModal.find('[name=replace_records]').val());
										//formData.append("keep_logos", $addDomainsModal.find('[name=keep_logos]').val());
										//formData.append("fadd_publish", $addDomainsModal.find('[name=fadd_publish]').val());
										formData.append("replace_records", $addDomainsModal.find('[name=replace_records]').prop('checked'));
										formData.append("keep_logos", $addDomainsModal.find('[name=keep_logos]').prop('checked'));
										formData.append("fadd_publish", $addDomainsModal.find('[name=fadd_publish]').prop('checked'));
										formData.append("fadd_brandable", $addDomainsModal.find('[name=fadd_brandable]').prop('checked'));
										formData.append("fadd_brandable", $addDomainsModal.find('[name=fadd_brandable]').prop('checked'));
										formData.append("id_portfolio", $('#portfolio_id').val());
										formData.append("selportfolioID", $('#selportfolioID').val());
										
										console.log(formData);
										//console.log( getFormData( $("#addnewDomainsForm") ) );
										
										$('.overlay').show();
										$http({
													method:'post',
													url:'ng/json/domains?action=import_zip_domains',
													data: formData,
													//data: getFormData( $("#addnewDomainsForm") ),
													withCredentials: true,
													//headers: {'Content-Type': 'application/zip' },
													//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
													headers: {'Content-Type': undefined },
													transformRequest: angular.identity
										})
										.then(function(result){

												console.log(result.data);
										
												$scope.getPortfolioCounts();

                                                var data = result.data;
                                                var config = result.config;
                                                var status = result.status;
                                                var headers = result.headers;
                                   
                                                $state.params.addDomainsErrorType = data.result.toLowerCase().trim() == 'success'? 'success': 'danger';
                                                
                                                $state.params.addDomainsErrorState = 'visible';
                                                $state.params.addDomainsErrorLabel = data.response_text+': '+data.response_details;
                                                
                                                $state.params.addDomainsModalState = '';
                                                $state.params.addDomainsModalLabel = 'Submit';
                                                
                                                $addDomainsModal.find('[name=_file]').val('');
												
												setTimeout(function(){ 

													if( data.result != undefined)	{
														toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
													}
													else	{
														toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
														alert('Session expired.');
														window.location.reload();
													}
													
													$('.overlay').hide();
													$('#btnRefresh').click();
													$('#txt_domain').val('');
													//$('#addNewDomains-modal').modal('hide');
													
													var add_domain_ajax_result = '';
													var add_domain_ajax_result_insert = '';
													var add_domain_ajax_result_update = '';
													var add_domain_ajax_result_corrected = '';
													
													var add_domain_ajax_result_update_final = '';
													var add_domain_ajax_result_update_final_buttons = '';
													if( data.ins_domains != undefined )	{
														if( data.ins_domains.length > 0 )	{
															var inserted_domains = data.ins_domains;
															
																add_domain_ajax_result_insert += '\
																<div style="border: 1px solid #5cb85c;margin-top:10px;">\
																	<div class="alert-success" style="cursor:pointer;padding:5px" onclick="$(\'#result_new_inserted_domain\').toggle(\'slow\');">\
																		<b>'+data.ins_domains.length+'</b> New Domain(s) Successfully Added \
																		<i class="fa fa-caret-down" style="float:right;margin-right:10px"></i>\
																	</div>\
																	<div id="result_new_inserted_domain" style="border:1px solid #eee;font-size:13px">\
																		<table class="table table-condensed">\
																			<tbody>';
																			$.each(inserted_domains, function(i, e){
																			  add_domain_ajax_result_insert += ' <tr><td>'+e.domain+'</td></tr> ';
																			});
																		add_domain_ajax_result_insert += '\
																			</tbody>\
																		</table>\
																	</div>\
																</div>\
																';
															//$('#add_domain_ajax_result').html(add_domain_ajax_result);
														}
													}
													
													if( data.upd_domains != undefined )	{
														if( data.upd_domains.length > 0 )	{
															var update_domains = data.upd_domains;

															//console.log(update_domains);
														
															add_domain_ajax_result_update += '\
																  \
																  <div class="pull-right" style="margin-right:10px;font-size:10px"> \
																	<input onchange="checkLogoDifferences($(this))" name="hide_logo_diff" id="hide_logo_diff" type="checkbox" data-toggle="tooltip" data-placement="top" alt="Check this if you want to hide domains where differenes are Logo and Type only. Mostly this is applicable in brandable domains. " title="Check this if you want to hide domains where differenes are Logo and Type only. Mostly this is applicable in brandable domains. " >Hide Domains if differences are Logo and Type only. \
																  </div> \
																  \
																	<table id="tblUpdateDomain" class="table table-condensed">\
																		<tbody>';

																		var cnt_exist_req_att = 0;
																		var new_type = '', old_type = '';
																		var new_type_hint = '', old_type_hint = '';
																		var new_price = '', old_price = '';
																		var new_portfolio = '', old_portfolio = '';
																		var new_logo = '', old_logo = '';
																		var new_category = '', old_category = '';
																		var id_logo_str = '';
																		var dom_premium_price = 0;
																		var domain_new_data = new Array;
																		$.each(update_domains, function(i, e)	{
																			//console.log(i + ' - ' + e);
																			
																			dom_premium_price = parseInt(data.upd_src_domains[i].premiumprice);
																			
																			if( e.type != undefined || e.type != null )	{
																				new_type = e.type.charAt(0).toUpperCase() + e.type.slice(1).toLowerCase().trim();
																				//console.log(e.domain + ' - ' +new_type);
																				if( new_type == '' )	{
																					var src_price = new Number( data.upd_src_domains[i].premiumprice );
																					var csv_price = new Number( e.price );
																					if( csv_price >= src_price )	{
																					//if( e.price >= data.upd_src_domains[i].premiumprice )	{
																						new_type = 'Premium';
																					}
																					else	{
																						new_type = 'Budget';
																					}
																				}
																			}
																			else	{
																				var src_price = new Number( data.upd_src_domains[i].premiumprice );
																				var csv_price = new Number( e.price );
																				if( csv_price >= src_price )	{
																				// if( e.price >= data.upd_src_domains[i].premiumprice )	{
																					new_type = 'Premium';
																				}
																				else	{
																					new_type = 'Budget';
																				}
																			}
																			
																			var src_id_logo = new Number( data.upd_src_domains[i].id_logo );
																			var csv_id_logo = new Number( e.id_logo );
																			
																			//console.log(e.domain + ' - ' + csv_id_logo);
																			//console.log(data.upd_src_domains[i].domain + ' - ' + src_id_logo);
																			
																			if( csv_id_logo > 0 )	{
																				new_type = 'Brandable';
																			}
																			
																			if( src_id_logo < 1 && data.upd_src_domains[i].ispremium == true )	{
																				old_type = 'Premium';
																			}
																			else	{
																				old_type = 'Budget';
																			}
																			
																			if( src_id_logo > 0 && ( parseInt(data.upd_src_domains[i].price) >= parseInt(data.upd_src_domains[i].premiumprice) ) ) {
																				old_type = 'Brandable';
																			}

																			if( parseInt(e.id_logo) > 0 && ( parseInt(e.price) >= parseInt(data.upd_src_domains[i].premiumprice) ) ) {
																				new_type = 'Brandable';
																			}

																			if( new_type.trim() == 'Under1k' )	{
																				new_type = 'Budget';
																			}
																			if( old_type.trim() == 'Under1k' )	{
																				old_type = 'Budget';
																			}
																			
																			if( new_type == 'Brandable' )	{
																				new_type_hint = new_type + '(above '+dom_premium_price+' and has logo)';
																			}
																			else if( new_type == 'Premium' )	{
																				new_type_hint = new_type + '(above '+dom_premium_price+' but no logo)';
																			}
																			else if( new_type == 'Budget' )	{
																				new_type_hint = new_type + '(below '+dom_premium_price+')';
																			}
																			
																			if( old_type == 'Brandable' )	{
																				old_type_hint = old_type + '(above '+dom_premium_price+' and has logo)';
																			}
																			else if( old_type == 'Premium' )	{
																				old_type_hint = old_type + '(above '+dom_premium_price+' but no logo)';
																			}
																			else if( old_type == 'Budget' )	{
																				old_type_hint = old_type + '(below '+dom_premium_price+')';
																			}
																			
																			
																			
																			if( e.price != undefined || e.price != null )	{
																				//new_price = e.price;
																				new_price = new Number( e.price );
																				new_price = new Number( new_price ).toFixed(2);
																			}
																			
																			//old_price = data.upd_src_domains[i].price;
																			old_price = new Number( data.upd_src_domains[i].price );
																			old_price = old_price.toFixed(2);
																			
																			if( e.portfolio != undefined || e.portfolio != null )	{
																				new_portfolio = e.portfolio;
																				new_portfolio = new_portfolio.charAt(0).toUpperCase() + new_portfolio.slice(1).toLowerCase().trim();
																			}
																			
																			if( data.upd_src_domains[i].portfolio != undefined || data.upd_src_domains[i].portfolio != null )	{
																				old_portfolio = data.upd_src_domains[i].portfolio;
																				old_portfolio = old_portfolio.charAt(0).toUpperCase() + old_portfolio.slice(1).toLowerCase().trim();
																				if( old_portfolio == 'null' ||  old_portfolio == null )	{
																					old_portfolio = '';
																				}
																			}
																			
																			if( parseInt(data.upd_src_domains[i].id_portfolio) < 1 || data.upd_src_domains[i].id_portfolio == null  )	{
																				old_portfolio = 'Default';
																			}

																			if( parseInt(e.id_portfolio) < 1 || e.id_portfolio == null )	{
																				new_portfolio = 'Default';
																			}
																			
																			if( e.logo != undefined || e.logo != null )	{
																				new_logo = e.logo;
																			}
																			
																			if( csv_id_logo < 1 )	{
																				new_logo = '';
																			}
																			
																			old_logo = data.upd_src_domains[i].logo;
																			if( old_logo == 'null' ||  old_logo == null || src_id_logo < 1 )	{
																				old_logo = '';
																			}
																			
																			if( e.category != undefined || e.category != null )	{
																				new_category = e.category;
																			}
																			
																			if( parseInt(e.category_id) < 1 || e.category_id == null )	{
																				new_category = 'None';
																			}
																			
																			old_category = data.upd_src_domains[i].category;
																			if( old_category == 'null' ||  old_category == null )	{
																				old_category = 'None';
																			}
																			
																			
																			if( (new_type != old_type) || ( new_price != old_price ) || (new_portfolio != old_portfolio) || (new_logo != old_logo) || (new_category != old_category) )	{
																				
																				//class for none-logo and have logo
																				var def_class_str = '';
																				if(new_logo != old_logo)	{
																					def_class_str = 'hideLogo';
																				}
																				if( new_price != old_price )	{
																					def_class_str = 'hidePrice';
																				}
																				if(new_portfolio != old_portfolio)	{
																					def_class_str = 'hidePortfolio';
																				}
																				if(new_category != old_category)	{
																					def_class_str = 'hideCategory';
																				}
																				
																				var indDomainID = parseInt(data.upd_src_domains[i].id_domain);
																				//var domain_new_data = new Array;
																				
																				cnt_exist_req_att++;
																				
																				add_domain_ajax_result_update += ' \
																				<tr id="tr-bydomain-'+data.upd_src_domains[i].id_domain+'" class="'+def_class_str+'"> \
																					<td>  \
																						<input value="'+data.upd_src_domains[i].id_domain+'" type="checkbox" class="chk_upd_domainID" name="upd_domain_id[]"> \
																						<a style="font-weight:bold;text-decoration:underline" onclick="$(\'#diff_'+i+'\').toggle(\'slow\');" >'+e.domain+' </a> \
																						<i onclick="$(\'#diff_'+i+'\').toggle(\'slow\');" style="float:right;cursor:pointer;" class="fa fa-caret-down"></i> \
																						<div id="diff_'+i+'" style="display:none; margin-top:5px;border:1px solid #ccc;"> \
																							<div class=""> \
																								<table class="table table-condensed"> \
																									<tbody>\
																										<tr>\
																											<td>\
																												<div class="row">\
																													<div class="col-xs-3 col-sm-3 col-md-3"></div>\
																													<div class="col-xs-3 col-sm-3 col-md-3"><b>Old</b></div>\
																													<div class="col-xs-3 col-sm-3 col-md-3">\
																														<b>New (from CSV)</b></div>\
																												</div>\
																											</td>\
																										</tr>';

																										if( new_type != old_type )	{
																											
																											domain_new_data['new_type-'+indDomainID] = new_type;
																											
																											add_domain_ajax_result_update += ' \
																											<tr id="tr-type-'+data.upd_src_domains[i].id_domain+'" class="ttype">\
																												<td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"><b>Type</b></div>\
																														<div class="col-xs-3 col-sm-3 col-md-3">';
																														
																															add_domain_ajax_result_update += '<span data-toggle="tooltip" data-placement="top" title=" '+old_type_hint+' ">';
																																add_domain_ajax_result_update += old_type;
																															add_domain_ajax_result_update += '</span>';
																															
																															add_domain_ajax_result_update +=  '\
																														</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															<span data-toggle="tooltip" data-placement="top" title=" '+new_type_hint+' ">'+new_type+' </span>\
																														</div>\
																													</div>\
																													<input value="'+new_type+'" type="hidden" id="newTypeByDomain-'+indDomainID+'" name="newTypeByDomain"> \
																													<input value="'+new_type+'" type="hidden" id="aType-'+indDomainID+'"> \
																												</td>\
																											</tr>';
																										}
																										if( new_price != old_price )	{
																											
																											domain_new_data['new_price-'+indDomainID] = new_price;
																											
																											add_domain_ajax_result_update += ' \
																											<tr id="tr-price-'+data.upd_src_domains[i].id_domain+'" class="tprice">\
																												<td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"><b>Price</b></div>\
																														<div class="col-xs-3 col-sm-3 col-md-3">'+old_price+'</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+new_price+' \
																															<i onclick="saveIndDomainPrice(\''+data.upd_src_domains[i].id_domain+'\',\''+new_price+'\')" style="cursor:pointer;margin-left:20px;" class="fa fa-save" alt="Save this new data" title="Save this new data" ></i> \
																														</div> \
																													</div>\
																													<input value="'+new_price+'" type="hidden" id="newPriceByDomain-'+indDomainID+'" name="newPriceByDomain"> \
																													<input value="'+new_price+'" type="hidden" id="aPrice-'+indDomainID+'" > \
																												</td>\
																											</tr>';
																										}
																										if( new_portfolio != old_portfolio )	{
																											var new_portfolioID = new Number( e.id_portfolio );
																											
																											domain_new_data['new_portfolioID-'+indDomainID] = new_portfolioID;
																											
																											add_domain_ajax_result_update += ' \
																											<tr id="tr-portfolio-'+data.upd_src_domains[i].id_domain+'" class="tport">\
																												<td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"><b>Portfolio</b></div>\
																														<div class="col-xs-3 col-sm-3 col-md-3">'+old_portfolio+'</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+new_portfolio+' \
																															<i onclick="saveIndDomainPortfolio(\''+data.upd_src_domains[i].id_domain+'\',\''+new_portfolioID+'\')" style="cursor:pointer;margin-left:20px;" class="fa fa-save" alt="Save this new data" title="Save this new data"></i> \
																														</div>\
																													</div>\
																													<input value="'+new_portfolioID+'" type="hidden" id="newPortfolioIDByDomain-'+indDomainID+'" name="newPortfolioIDByDomain"> \
																													<input value="'+new_portfolioID+'" type="hidden" id="aPortfolioID-'+indDomainID+'"> \
																												</td>\
																											</tr>';
																										}
																										if( new_logo != old_logo )	{
																											domain_new_data['new_logo-'+indDomainID] = new_logo;
																											
																											if( new_logo.trim() == '' || new_logo == null )	{
																												new_logo = 'None';
																											}
																											if( old_logo.trim() == '' || old_logo == null )	{
																												old_logo = 'None';
																											}
																											
																											
																											var src_imglogo = '';
																											if( src_id_logo > 0 )	{
																												src_imglogo = '<img src="'+zen_api_url+'image.php?id_logo='+src_id_logo+'&amp;width=110&amp;height=50" alt="'+old_logo+'" title="'+old_logo+'">';
																											}
																											else	{
																												src_imglogo = old_logo;
																											}

																											var new_imglogo = '';
																											if( csv_id_logo > 0 )	{
																												new_imglogo = '<img src="'+zen_api_url+'image.php?id_logo='+csv_id_logo+'&amp;width=110&amp;height=50" alt="'+new_logo+'" title="'+new_logo+'">';
																											}
																											else	{
																												new_imglogo = new_logo;
																											}
																											
																											add_domain_ajax_result_update += ' \
																											<tr id="tr-logo-'+data.upd_src_domains[i].id_domain+'" class="tlogo">\
																												<td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"><b>Logo</b></div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+src_imglogo+' \
																														</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+new_imglogo+' \
																															<i onclick="saveIndDomainLogo(\''+data.upd_src_domains[i].id_domain+'\',\''+csv_id_logo+'\',\''+e.domain+'\')" style="cursor:pointer;margin-left:20px;" class="fa fa-save" alt="Save this new data" title="Save this new data"></i> \
																														</div>\
																													</div>\
																													<input value="'+csv_id_logo+'" type="hidden" id="newLogoByDomain-'+indDomainID+'" name="newLogoByDomain"> \
																													<input value="'+e.domain+'" type="hidden" id="newDomainByDomain-'+indDomainID+'" name="newDomainByDomain"> \
																													<input value="'+csv_id_logo+'" type="hidden" id="aLogo-'+indDomainID+'" > \
																													<input value="'+e.domain+'" type="hidden" id="aDomain-'+indDomainID+'" > \
																												</td>\
																											</tr>';
																										}
																										if( new_category != old_category )	{
																											var new_categoryID = new Number( e.category_id );
																											
																											domain_new_data['new_categoryID-'+indDomainID] = new_categoryID;
																											
																											add_domain_ajax_result_update += ' \
																											<tr id="tr-category-'+data.upd_src_domains[i].id_domain+'" class="tcat">\
																												<td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"><b>Category</b></div>\
																														<div class="col-xs-3 col-sm-3 col-md-3">'+old_category+'</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+new_category+' \
																															<i onclick="saveIndDomainCategory(\''+data.upd_src_domains[i].id_domain+'\',\''+new_categoryID+'\')" style="cursor:pointer;margin-left:20px;" class="fa fa-save" alt="Save this new data" title="Save this new data"></i> \
																														</div>\
																													</div>\
																													<input value="'+new_categoryID+'" type="hidden" id="newCategoryIDByDomain-'+indDomainID+'" name="newCategoryIDByDomain"> \
																													<input value="'+new_categoryID+'" type="hidden" id="aCategoryID-'+indDomainID+'" > \
																												</td>\
																											</tr>';
																										}
																										add_domain_ajax_result_update += ' \
																									</tbody> \
																								</table> \
																							</div> \
																							<div style="margin-left:10px;margin-bottom:10px;"> \
																								<button type="button" onclick="$(\'#diff_'+i+'\').hide(\'slow\');" class="btn btn-default btn-md"> <i style="cursor:pointer;" class="fa fa-close"></i> Close </button>\
																								<button type="button" onclick="saveIndByDomain(\''+indDomainID+'\')" class="btn btn-default btn-md"> <i style="cursor:pointer;" class="fa fa-save"></i> Update </button>\
																							</div> \
																						</div> \
																					</td> \
																				</tr> \
																				';
																				
																				//console.log( ' DomainID = ' + domain_new_data );
																				
																			}
																			
																		});
																		
																	add_domain_ajax_result_update += '\
																		</tbody>\
																	</table>\
															';
															
															add_domain_ajax_result_update_final_buttons += '\
																	<div id="update_buttons" style="margin-left:10px;margin-bottom:10px;"> \
																		<button type="button" onclick="$(\'#result_update_domain\').hide(\'slow\');" class="btn btn-success btn-md"> <i style="cursor:pointer;" class="fa fa-close"></i> Close </button>\
																		<button type="button" onclick="$(\'#tblUpdateDomain input:checkbox\').prop(\'checked\', true);"  class="btn btn-success btn-md"> <i style="cursor:pointer;" class="fa fa-check-square-o"></i> Check All </button>\
																		<button type="button" onclick="$(\'#tblUpdateDomain input:checkbox\').prop(\'checked\', false);"  class="btn btn-success btn-md"> <i style="cursor:pointer;" class="fa fa-square"></i> Uncheck All </button>\
																		<button type="button" onclick="saveAllDomain()" class="btn btn-success btn-md"> <i style="cursor:pointer;" class="fa fa-save"></i> Update All </button>\
																	</div> \
															';
															
															add_domain_ajax_result_update_final += '\
															<div style="border: 1px solid #f0ad4e;margin-top:10px;">\
																<div class="alert-warning" style="cursor:pointer;padding:5px" onclick="$(\'#result_update_domain\').toggle(\'slow\');">\
																	<b>'+data.upd_domains.length+'</b> Domain(s) already exist, <b id="upd_cnt_domain">'+cnt_exist_req_att+'</b> Domain(s) require attention: \
																	<i class="fa fa-caret-down" style="float:right;margin-right:10px"></i>\
																</div>\
																<div id="result_update_domain" style="border:1px solid #eee;font-size:13px">';
																
																	add_domain_ajax_result_update_final += add_domain_ajax_result_update;
																	if( cnt_exist_req_att > 0 )	{
																		add_domain_ajax_result_update_final += add_domain_ajax_result_update_final_buttons;
																	}
																	
																add_domain_ajax_result_update_final += '</div>';
																
															add_domain_ajax_result_update_final += '</div>';
													
															add_domain_ajax_result_update = add_domain_ajax_result_update_final;
														}
													}
													
													
													var cnt_corrected_domains = 0;
													if( data.cor_domains != undefined )	{														
														$.each(data.cor_domains, function(i, e){
															cnt_corrected_domains++;
														});
													}
													
													if( cnt_corrected_domains > 0 )	{
														var corrected_domains = data.cor_domains;
														add_domain_ajax_result_corrected += '\
															<div style="border: 1px solid #d9534f;margin-top:10px;">\
																<div class="alert-danger" style="cursor:pointer;padding:5px" onclick="$(\'#res_cor_dom\').toggle(\'slow\');">\
																	<b>'+cnt_corrected_domains+'</b> Domain(s) require attention: \
																	<i class="fa fa-caret-down" style="float:right;margin-right:10px"></i>\
																</div>\
																<div id="res_cor_dom" style="border:1px solid #eee;font-size:13px">\
																  \
																  <div class="pull-right" style="margin-right:10px;font-size:10px"> \
																	<input onchange="checkInvalidDomainsResult($(this))" name="inv_dom_name" id="inv_dom_name" type="checkbox" checked="checked" data-toggle="tooltip" data-placement="top" alt="Check this if you want to show all invalid domains" title="Check this if you want to show all invalid domains" >Invalid Domain Name \
																	<input onchange="checkDuplicateEntries($(this))" name="dupl_entries" id="dupl_entries"  type="checkbox" data-toggle="tooltip" data-placement="top" alt="Check this if you want to show all duplicate entries" title="Check this if you want to show all duplicate entries">Duplicate Entries \
																  </div> \
																  \
																	<table class="table table-condensed">\
																		<tbody>';
																		$.each(corrected_domains, function(i, e)	{
																		  //console.log(i + ' = ' +e);
																		  if( e.domain.trim() != '' )	{
																			  var edit_str = '';
																			  if( data.error_corrections_constant[i] == 'INVALID_DOMAIN_NAME,' && e.isfixed == 0 )	{
																				  edit_str += '<input onclick="$(\'#corr_'+i+'\').show(\'slow\')" class="btn btn-success" type="button" value="Edit">';
																			  }
																			  if( e.isfixed == 1 )	{
																				  edit_str += '<span data-toggle="tooltip" data-placement="top" title="You have already fixed this domain." class="label label-info"> <i class="glyphicon glyphicon-thumbs-up"></i> Fixed </span>';
																			  }
																			  
																			var cor_imglogo = '';
																			if( e.id_logo > 0 )	{
																				cor_imglogo = '<img src="'+zen_api_url+'image.php?id_logo='+e.id_logo+'&amp;width=110&amp;height=50" alt="'+e.logo+'" title="'+e.logo+'">';
																			}
																			else	{
																				cor_imglogo = e.logo;
																			}
																		  
																		  
																			if( data.error_corrections_constant[i] == 'INVALID_DOMAIN_NAME,' )	{
																				var class_str = 'invalidDomainNames';
																			}
																			else if( data.error_corrections_constant[i] == 'MULTIPLE_COPIES,' )	{
																				var class_str = 'duplicateEntries';
																			}
																			
																		  add_domain_ajax_result_corrected += ' \
																			<tr id="tr-correcteddomain-'+i+'" class="'+class_str+'"> \
																				<td> \
																					<div class="row">\
																						<div class="col-xs-4 col-sm-4 col-md-4">'+e.domain+' '+edit_str+' \</div>\
																						<div class="col-xs-4 col-sm-4 col-md-4">'+data.error_corrections[i]+'</div>\
																						<div class="clearfix"></div>\
																						<div id="corr_'+i+'" style="display:none; border: 1px solid #aaa;margin:10px;"> \
																							<table class="table table-condensed"> \
																								<tbody>\
																									<tr><td>\
																										<div class="row">\
																											<div class="col-xs-3 col-sm-3 col-md-3"> \
																												Domain Name \
																											</div>\
																											<div class="col-xs-4 col-sm-4 col-md-4"> \
																												<input id="id_domain_corrections-'+i+'" name="id_domain_corrections-'+i+'" type="hidden" value="'+e.id_domain_corrections+'"> \
																												<input id="id_user-'+i+'" name="id_user-'+i+'" type="hidden" value="'+e.id_user+'"> \
																												<input id="corr_domain-'+i+'" name="corr_domain-'+i+'" type="text" value="'+e.domain+'"> \
																												<input onclick="saveCorrDomain(\''+i+'\')" type="button" value="Save" class="btn btn-success"> \
																											</div> \
																											<i onclick="$(\'#cor_other_info_'+i+'\').toggle(\'slow\')" class="fa fa-caret-down" style="float:right;margin-right:10px;cursor:pointer" alt="View Other Info" title="View Other Info"></i> \
																											<i onclick="$(\'#corr_'+i+'\').hide(\'slow\')" class="fa fa-close" style="float:right;margin-right:10px;cursor:pointer" alt="Close" title="Close"></i> \
																										</div>\
																										<div id="cor_other_info_'+i+'" class="row" style="display:none;"> \
																											<table class="table table-condensed"> <tbody>';
																											
																											if( e.price != undefined || e.price != null )	{
																												add_domain_ajax_result_corrected += ' \
																												<tr><td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															Price \
																														</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+e.price+' \
																															<input id="corr_price-'+i+'" name="corr_price-'+i+'" type="hidden" value="'+e.price+'"> \
																														</div>\
																													</div>\
																												</td></tr>';
																											}
																											
																											if( cor_imglogo != undefined || cor_imglogo != null )	{
																												if( cor_imglogo.trim() == '' )	{
																													cor_imglogo = 'None';
																												}
																												add_domain_ajax_result_corrected += ' \
																												<tr><td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															Logo \
																														</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+cor_imglogo+' \
																															<input id="corr_id_logo-'+i+'" name="corr_id_logo-'+i+'" type="hidden" value="'+e.id_logo+'"> \
																														</div>\
																													</div>\
																												</td></tr>';
																											}
																											
																											if( e.portfolio != undefined || e.portfolio != null )	{
																												add_domain_ajax_result_corrected += ' \
																												<tr><td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															Portfolio \
																														</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+e.portfolio+' \
																															<input id="corr_id_portfolio-'+i+'" name="corr_id_portfolio-'+i+'" type="hidden" value="'+e.id_portfolio+'"> \
																														</div>\
																													</div>\
																												</td></tr>';
																											}
																											
																											if( e.category != undefined || e.category != null )	{
																												add_domain_ajax_result_corrected += ' \
																												<tr><td>\
																													<div class="row">\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															Category \
																														</div>\
																														<div class="col-xs-3 col-sm-3 col-md-3"> \
																															'+e.category+' \
																															<input id="corr_category_id-'+i+'" name="corr_category_id-'+i+'" type="hidden" value="'+e.category_id+'"> \
																														</div>\
																													</div>\
																												</td></tr>';
																											}
																											
																												add_domain_ajax_result_corrected += ' \
																											</tbody></table> \
																										</div> \
																									</td></tr>\
																								</tbody>\
																							</table> \
																						</div>\
																					</div>\
																				</td> \
																			</tr> \
																			';
																		  } // end if
																		});
																	add_domain_ajax_result_corrected += '\
																		</tbody>\
																	</table>\
																</div>\
															</div>\
															';
													}
													

													add_domain_ajax_result = add_domain_ajax_result_insert + add_domain_ajax_result_update + add_domain_ajax_result_corrected;
													
													var add_domain_ajax_result_new = '\
															<div style="border: 1px solid #aaa;padding:10px;margin-bottom:10px;"> \
																<div onclick="$(\'#main_result_add_domain\').toggle(\'slow\');" style="cursor:pointer;"> \
																	<b>RESULTS</b><i class="fa fa-caret-down" style="float:right;margin-right:10px"></i> \
																</div>\
																<div id="main_result_add_domain"> \
																	'+add_domain_ajax_result+' \
																</div> \
															</div>\
														';
														
													//$('#add_domain_ajax_result').html(add_domain_ajax_result);
													$('#add_domain_ajax_result').html(add_domain_ajax_result_new);
													
													$('#add_domain_ajax_result').show();
													
													$('[data-toggle=tooltip]').tooltip();
													
													$('.duplicateEntries').hide();
												}, 1000);
												
												
                                   
									   },
									   function(error){
																	
													$state.params.addDomainsErrorState = 'visible';
													$state.params.addDomainsErrorLabel = 'Ajax error: Unable to upload response, try again later.';
													
													$state.params.addDomainsModalState = '';
													$state.params.addDomainsModalLabel = 'Submit';     
													setTimeout(function(){ 
														$('.overlay').hide();
													}, 1000);
									   });
									   
									}
									   //Skip further if zip is being added.
                                   return false;
                                }
                                
                                if(textDomains.length)	{
									if( !$scope.ValidateEmail( $('#notification_email').val() )  )	{
										alert('Please provide a valid notification email.');
										$('#notification_email').attr('style','border-color:#d9534f;');
										$('#notification_email').focus();
										$state.params.addDomainsModalState = '';
									}
									 else	{										 
										$('#notification_email').attr('style','border-color:none;');
										$state.params.addDomainsModalState = '';
											
											//console.log(JSON.stringify(textDomains));
											//console.log( getFormData( $("#addnewDomainsForm") ) );
									
											$('.overlay').show();
											//Process text domains...
											$http({
															method: 'post',
															//data: JSON.stringify(textDomains),
															data: getFormData( $("#addnewDomainsForm") ),
															url: 'ng/json/domains/?action=import_text_domains'
											
											}).then(function(result){
													var data = result.data;
													var config = result.config;
													var status = result.status;
													var headers = result.headers;

													$state.params.addDomainsErrorType = data.result.toLowerCase().trim() == 'success'? 'success': 'danger';
													$addDomainsModal.find('[name=txt_domain]')
																	.addClass('text-'+$state.params.addDomainsErrorType)
																	.parent().addClass('has-error')
																			.focus();
																	
													$state.params.addDomainsErrorState = 'visible';
													$state.params.addDomainsErrorLabel = data.response_text+': '+data.response_details;
													
													$state.params.addDomainsModalState = '';
													$state.params.addDomainsModalLabel = 'Submit';
													
													setTimeout(function(){
														
														if( data.result != undefined)	{
															toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
														}
														else	{
															toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
															alert('Session expired.');
															window.location.reload();
														}

														$('.overlay').hide();
														$('#btnRefresh').click();
														$('#txt_domain').val('');
														$('#addNewDomains-modal').modal('hide');
														
														$('#sidebar_menu li').removeAttr('class');
														$('#li_domains').attr('class','active');
														
													}, 1000);
													


											},
											function(){
																	
													$state.params.addDomainsErrorState = 'visible';
													$state.params.addDomainsErrorLabel = 'Ajax error: Unable to upload response, try again later.';
													
													$state.params.addDomainsModalState = '';
													$state.params.addDomainsModalLabel = 'Submit';
													
													setTimeout(function(){ 
														$('.overlay').hide();
													}, 1000);
											});
									}
                                }
								else	{
									$addDomainsModal.find('[name=txt_domain]').addClass('text-danger').parent().addClass('has-error').focus();

									$state.params.addDomainsModalState = '';
									$state.params.addDomainsModalLabel = 'Submit';
									
									$state.params.addDomainsErrorState = 'visible';
									$state.params.addDomainsErrorLabel = 'Please enter domains to import first.';
									$state.params.addDomainsErrorType = 'danger';
									
									setTimeout(function(){ 
										$('.overlay').hide();
									}, 1000);
								}
								return false;
                        };
						$scope.ValidateEmail = function(email) {
							var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
							return expr.test(email);
						};
                        $scope.autocompleteItems = [];
                        $scope.selectMatch = function(index){
                                console.log($scope.autocompleteItems);
                                console.log(index);
                        };
						//$scope.getWhois = function(id,domain_name)	{
						$scope.getWhois = function(id)	{
								//console.log(id);
								//console.log(domain_name);
								var domain_name = $('#domain_sel_'+id).val();

								console.log(id);
								console.log(domain_name);								
								
								$('#whois_timestamp_'+id).html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
								$('#load_whois_data_'+id).html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
								
								$('#whois-modal-'+id).modal();
								var data = {
										'id_domain': id,
										'domain': domain_name
								};
							
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=getWhois',
									data: data
								}).success(function(data, status, headers, config)	{
									console.log( data );
									var r = data;
									setTimeout(function()	{
										if( data.result != undefined)	{
											//toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											var who_is_str = '';
											if(data.result == 'ERROR')	{
												$('#whois_timestamp_'+id).html( r.time + '<span class=" text-danger"> '+r.result+' : '+r.response_details+' </span>');
												$('#load_whois_data_'+id).html(who_is_str);
											}
											else	{
												$('#whois_timestamp_'+id).html( r.time );
												
												if (r.whoisinfo.registrant === undefined || r.whoisinfo.registrant === null)	{	
													var registrant_name = '';
													var registrant_address = '';
												}
												else	{	
													var registrant_name = r.whoisinfo.registrant.name;	
													var registrant_address = r.whoisinfo.registrant.address;
												}
												
												if (r.whoisinfo.domainName === undefined || r.whoisinfo.domainName === null)	{	var domainName = '';	}
												else	{	var domainName = r.whoisinfo.domainName;	}
												
												if (r.whoisinfo.registered === undefined || r.whoisinfo.registered === null)	{	var registered = '';	}
												else	{	var registered = r.whoisinfo.registered;	}
												
												if (r.whoisinfo.domain_registrar === undefined || r.whoisinfo.domain_registrar === null)	{	var domain_registrar = '';	}
												else	{	var domain_registrar = r.whoisinfo.domain_registrar;	}
												
												if (r.whoisinfo.domain_created === undefined || r.whoisinfo.domain_created === null)	{	var domain_created = '';	}
												else	{	var domain_created = r.whoisinfo.domain_created;	}
												
												if (r.whoisinfo.domain_expires === undefined || r.whoisinfo.domain_expires === null)	{	var domain_expires = '';	}
												else	{	var domain_expires = r.whoisinfo.domain_expires;	}
												
												if (r.whoisinfo.domain_updated === undefined || r.whoisinfo.domain_updated === null)	{	var domain_updated = '';	}
												else	{	var domain_updated = r.whoisinfo.domain_updated;	}

												if (r.whoisinfo.tld === undefined || r.whoisinfo.tld === null)	{	var tld = '';	}
												else	{	var tld = r.whoisinfo.tld;	}
												
												var j;
												
												who_is_str += '\
												 <div class="col-sm-12" >\
													<div class="form-group">\
														<label><b>Domain : </b> '+domainName+' </label> <br>\
														<label><b>Registered : </b> '+registered+'  </label> <br>\
														<label><b>Registrar : </b> '+domain_registrar+'  </label> <br>\
														<label><b>Created : </b> '+domain_created+' </label> <br>\
														<label><b>Expires : </b> '+domain_expires+' </label> <br>\
														<label><b>Updated : </b> '+domain_updated+'  </label> <br>\
														<label><b>Tld : </b> '+tld+' </label> <br>\
														<label><b>Registrant Name : </b> '+registrant_name+' </label> <br>\
														<label><b>Registrant Email : </b> '+r.user_email+' </label> <br>\
														<label><b>Registrant Address : </b> '+registrant_address+' </label> <br>';

												if( r.whoisinfo.status.length > 0 )	{
													who_is_str += '<label><b>Domain Status : </b> </label> <br>';
													for(j=0; j < r.whoisinfo.status.length; j++)	{
														who_is_str += '		<label> '+r.whoisinfo.status[j]+' </label> <br>';
													}
												}

												if( r.whoisinfo.servers.length > 0 )	{
													who_is_str += '<label><b>Servers : </b> </label> <br>';
													for(j=0; j < r.whoisinfo.servers.length; j++)	{
														who_is_str += '		<label> '+r.whoisinfo.servers[j].server+' </label> <br>';
													}
												}
												
												if( r.whoisinfo.name_servers.length > 0 )	{
													who_is_str += '<label><b>Name Servers : </b> </label> <br>';
													for(j=0; j < r.whoisinfo.name_servers.length; j++)	{
														who_is_str += '		<label> '+r.whoisinfo.name_servers[j]+' </label> <br>';
													}
												}

												who_is_str += '	</div></div>';
												who_is_str += ' <div class="clearfix" ></div>';
												who_is_str += ' <div class="col-sm-12" >';
														if( r.whoisinfo.raw.length > 0 )	{
															who_is_str += '<hr>';
															for(j=0; j < r.whoisinfo.raw.length; j++)	{
																who_is_str += '		<label> '+r.whoisinfo.raw[j]+' </label> <br>';
															}
														}
												who_is_str += ' </div>';
												who_is_str += ' <div class="clearfix" ></div>';
																
												$('#load_whois_data_'+id).html(who_is_str);
												
											}
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										$('.overlay').hide();
									}, 1000);
								});
							
							return false;
						}
                        $scope.getColumns = function(val){
                                        return $http.get('ng/json/domains/?action=search_column_records', {
                                                params: {
                                                  keyword: val,
                                                  sensor: false
                                                }
                                              }).then(function(res){
                                              
                                                $scope.autocompleteItems = res.data.record;
                                                
                                                var autocomplete = [];
                                                angular.forEach(res.data.record, function(item){
                                                        autocomplete.push(item.col_title);
                                                });
                                                return autocomplete;
                                              });
                        };
                        $scope.columns = [];
                        $scope.loadDomainColumns = function(val){
                                $('#showHideCols-modal').modal();
                                $http({
                                        method: 'get',
                                        url: 'ng/json/domains/?action=fetch_column_records'
                                }).success(function(data, status, headers, config)	{
										if( data.result_code != undefined)	{
											if(data.result_code == 0)	{
													if(data.record)	{	
															var records = [];
															data.record.forEach(function(e, i){
																			if(
																					//skip these
																					e.key != 'domain' &&
																					e.key != 'price' &&
																					e.key != 'offer_price' &&
																					e.key != 'id_logo'
																					
																			)       records[records.length] = e;
															});
															//Populate current page
															$scope.columns = records;

													}
													else   {	$scope.columns = [];	}
											}
											else   {	$scope.columns = [];	}
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
                                });
                        };
                        $scope.removeFromSelectedColumns = function(key){
                                var records = [];
                                $scope.columns.forEach(function(e, i){
                                        if(key != e.key)
                                                records[records.length] = e;
                                });
                                $scope.columns = records;
                        };
                        $scope.addToSelectedColumns = function(){
                                
                                var title = $('#addColumnToList-title').val();
                                var comment = $('#addColumnToList-comment').val();
                                $('#addColumnToList-title').val('');
                                $('#addColumnToList-comment').val('');
                                
                                if(!title){
                                        $("#addColumnToList-title").focus();
                                        return false;
                                }
                                if(!comment){
                                        $("#addColumnToList-comment").focus();
                                        return false;
                                }

                                var key = title.toLowerCase().trim().replace(/ /g,'_');
                                var order = 0;
                                for(var i =0; i < $scope.columns.length; i++){
                                        //update order to last one
                                        if(order < parseInt($scope.columns[i].order))
                                                order = parseInt($scope.columns[i].order);
                                        //check if title/key is already added
                                        if(key == $scope.columns[i].col_title.toLowerCase().trim().replace(/ /g,'_')){
                                                $("#addColumnToList-title").focus();
                                                return false;                                                
                                        }
                                }
                                order = order +1;
                                $scope.columns[$scope.columns.length] = {'col_title':title, 'col_comment':comment, 'key':key, 'order':order};
                        };
                        $state.params.UpdateColsErrorMessage = 'hidden';
                        $state.params.UpdateColsResponseState = '';
                        $state.params.UpdateColsMessageTitle = '';
                        $state.params.UpdateColsMessageDetails = '';
                        $state.params.UpdateColsButtonState = '';
                        $state.params.UpdateColsButtonLabel = 'Update';
                        $scope.updateColumnsStructure = function(){

                                        var input = $('#showHideCols-modal').find('input[data-self]');

                                        var temp = {};
                                        var index = 0;
                                        input.each(function(i, e){
                                          if($(e).data('self') == 'order'){
                                            index++;
                                            temp[index-1] = {};
                                          }
                                          temp[index-1][$(e).data('self')] = $(e).val();
                                        });
                                        var records = [];
                                        $.each(temp, function(i, e){
                                          records.push(e);
                                        });
                                        //Grab Values from form...
                                        $scope.columns = records;

                                        $state.params.UpdateColsButtonState = 'disabled';
                                        $state.params.UpdateColsButtonLabel = 'Processing...';
                                        
                                        $http({
                                                        method: 'post',
                                                        data: JSON.stringify($scope.columns),
                                                        url: 'ng/json/domains/?action=save_column_records'
                                        
                                        }).then(function(result){

                                                var data = result.data;
                                                var config = result.config;
                                                var status = result.status;
                                                var headers = result.headers;

                                                console.log(data);

												if( data.result != undefined)	{
													toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												
                                                $state.params.UpdateColsErrorMessage = 'visible';
                                                $state.params.UpdateColsResponseState = data.result.toLowerCase() == 'success'? 'success': 'danger';
                                                $state.params.UpdateColsMessageTitle = data.response_text;
                                                $state.params.UpdateColsMessageDetails = data.response_details;
                                                
                                                $state.params.UpdateColsButtonState = '';
                                                $state.params.UpdateColsButtonLabel = 'Update';
                                        },
                                        function(){
                                                //Error in ajax
                                                $state.params.UpdateColsErrorMessage = 'visible';
                                                $state.params.UpdateColsResponseState = 'danger';
                                                $state.params.UpdateColsMessageTitle = 'Ajax error';
                                                $state.params.UpdateColsMessageDetails = 'Please try again after refreshing the page.';
                                                
                                                $state.params.UpdateColsButtonState = '';
                                                $state.params.UpdateColsButtonLabel = 'Update';
                                        });
                        };
                        $scope.domainSortUpdate = function(event, ui){
							var items = [];
							var prev_order_index = [];
							var new_order_index = [];
							var min_id = 0;
				
							$("#domain-rows").find('.sort-item').each(function(i, e) {
								items.push($(e).attr('data-id'));
								prev_order_index.push($(e).attr('data-order-no'));
							});
				
							for(var i = 0; i <= prev_order_index.length; i++)	{
								new_order_index[i] = i + Math.min.apply( Math, prev_order_index )
							}

							$('.overlay').show();
							$('#showloadingbar').html('<img src="/static/admin/ajax-loader-bar.gif">');
							var data = { 'sort_items': items, 'order': new_order_index };
							$http({
								method: "POST",
								url: 'ng/json/domains/?action=sort_column_records',
								data: data,
							}).then(
								function(r) {
										r = r.data
										console.log(r);
										if( r.result != undefined)	{
											toaster.pop(r.result.toLowerCase(), r.response_text, r.response_details);
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										$('.overlay').hide();
										$('#showloadingbar').html('');
								},
								function(r){
										console.log(r);
										//$('.overlay').hide();
										$('#showloadingbar').html('');
								}
							);
                        };
                        $scope.deleteDomainRows = function(id_domain){
                                $('#confirm-modal').modal();
                                var domain = $("tr[data-id="+id_domain+"]").attr('data-domain');
                                $('#confirm-modal').find('.message').html('Are you sure to remove domain "'+domain+'"? <br> This cannot be undone, all data related to "'+domain+'" will be removed.');
                                $('#confirm-modal').find('input.key').val('id_domain');
                                $('#confirm-modal').find('input.value').val(id_domain);
                                //Further working is defined in page itself
                        };
                        $scope.eDomain = [];
                        $scope.updateDomain = function(id_domain){
                                var $form = $('#edit-domain-modal').find('form');
                                var $form = $form.serializeArray();
                                var formdata = {};
                                $form.forEach(function (e, i) {
                                  formdata[e.name] = e.value;
                                });
                                formdata = JSON.stringify(formdata);

								$('.overlay').show();
								$('#loading_bar_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
                                $http({
                                        method: "POST",
                                        url: 'ng/json/domains/?action=update_domain_record',
                                        data: formdata
                                }).then(
                                        function(r) {
                                                r = r.data;
												console.log(r);
	
												if( r.result_code != undefined)	{
													if(r.result_code == 0)	{	
															$('#edit-domain-modal').modal('hide');
															$scope.loadPage($scope.currentPage);
															$scope.getPortfolioCounts();
													}
													toaster.pop(r.result.toLowerCase(), r.response_text, r.response_details);
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												
												$('.overlay').hide();
												$('#loading_bar_EDIT').html('');
                                        },function(r){
                                                $('#edit-domain-modal').modal('hide');
												$('.overlay').hide();
												$('#loading_bar_EDIT').html('');
                                        }
                                );
                        };
                        $scope.saveDomainLogo = function(id_domain)	{
							if( $.trim( $('#files').val() ) == '' )	{
								alert('Please select a file to upload a logo.');
							}
							else	{
                                $state.params.addDomainsModalState = 'disabled';
                                $state.params.addDomainsModalLabel = 'processing...';
                                $state.params.addDomainsErrorState = 'hidden';
								
								var $uploadLogoDomainsModal = $('#uploadlogo-modal');
								var files = $uploadLogoDomainsModal.find('[name=files]')[0].files[0]; //Get first file...
								if( files )	{
                                    var formFileData = new FormData();
                                    formFileData.append("files", files);
                                    formFileData.append("l_id_domain", $uploadLogoDomainsModal.find('[name=l_id_domain]').val());
									
									$('#loading_bar_UPLOADLOGO').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
									$http({
											method: "POST",
											url: 'ng/json/domains/?action=save_domain_logo',
											data: formFileData,
											withCredentials: true,
											headers: {'Content-Type': undefined },
											transformRequest: angular.identity
									}).then(
											function(r) {
												console.log(r);
												r = r.data;

												if( r.result_code != undefined )	{		
													if(r.result_code == 0)	{	
															//$('#uploadlogo-modal').modal('hide');
															$scope.loadPage($scope.currentPage);
															$scope.getPortfolioCounts();
															
															$state.params.addDomainsErrorType = r.result.toLowerCase().trim() == 'success'? 'success': 'danger';
															$state.params.addDomainsErrorState = 'visible';
															$state.params.addDomainsErrorLabel = r.response_text+': '+r.response_details;
															$state.params.addDomainsModalState = '';
															$state.params.addDomainsModalLabel = 'Submit';
															
															$('#logo_domain_img_main').html('<img src="'+zen_api_url+'image.php?id_logo='+r.logo_id+'&width=150&height=100">');
															//$('#imglogo_domain_'+id_domain).html('<a data-img="'+r.logo_id+'&width=110&height=50" rel="popover111" data-id="'+r.logo_id+'" href=""><img src="'+zen_api_url+'image.php?id_logo='+r.logo_id+'&width=150&height=100"></a>');
															$('#loading_bar_UPLOADLOGO').html('');
															$('#files').val('');
													}
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												$('#loading_bar_UPLOADLOGO').html('');
											},function(r){
												
													$state.params.addDomainsErrorState = 'visible';
													$state.params.addDomainsErrorLabel = 'Ajax error: Unable to upload response, try again later.';
													$state.params.addDomainsModalState = '';
													$state.params.addDomainsModalLabel = 'Submit';  
												
													//$('#uploadlogo-modal').modal('hide');
													$('#loading_bar_UPLOADLOGO').html('');
											}
									);
								}
							}
                        };
                        $scope.updateBulkDomains = function()	{
                                var $form = $('#edit-bulk-domain-modal').find('form').find('[name]');
                                var $form = $form;

                                var key;
                                var records = {};
								var cnt_domain_form = 0;
                                $form.each(function(i, e){

                                  if(e.name == 'id_domain'){
                                    key = e.value;
                                    records[key] = {};
									cnt_domain_form++;
                                  }
                                  
                                  records[key][e.name] = $(e).val();
                                });
                                var cnt_records = 0;
								$('.overlay').show();
                                $.each(records, function(i, e){
                                        var formdata = JSON.stringify(e);
										//var formdata = getFormData( $("#formbulkedit-"+i) );
                                        $http({
                                                method: "POST",
                                                url: 'ng/json/domains/?action=bulk_update_domain_record',
                                                data: formdata
                                        }).then(
                                                function(r) {
													r = r.data;
													
													if( r.result_code != undefined )	{
                                                        if(r.result_code == 0)	{
															cnt_records++;
															console.log(cnt_records);
															if( cnt_records == cnt_domain_form )	{	
																toaster.pop(r.result.toLowerCase(), r.response_text, r.response_details);
																$('.overlay').hide();
                                                                $('#edit-bulk-domain-modal').modal('hide');
                                                                $scope.loadPage($scope.currentPage);
                                                                $scope.getPortfolioCounts();
																$('#bulk-select').val(0);
															}
                                                        }
													}
													else	{
														toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
														alert('Session expired.');
														window.location.reload();
													}
                                                },function(r){
                                                        $('#edit-bulk-domain-modal').modal('hide');
                                                }
                                        );
                                });
                        };
                        $scope.getPortfolioCounts = function(){						
                                $http({
                                        method: "GET",
                                        url: 'ng/json/domains/?action=get_portfolio_records'
                                }).then(
                                        function(r) {
											r = r.data;
											if( r.result_code != undefined)	{
                                                if(r.result_code == 0)	{
                                                
                                                        var pofolios = r.record;
                                                        
                                                        
                                                        var total = parseInt(r.default);
                                                        pofolios.forEach(function(e, i){
                                                                
                                                                total = total + parseInt(e.Total);
                                                                
                                                                $('#portfolios-nav-ul').find('b.pofolio-'+e.id_portfolio).text(e.Total);
                                                                $('#portfolios-nav-ul').find('b.pofolio-'+e.id_portfolio).next().text(e.title);
                                                        });
                                                        //Create All domains entry
                                                        $('#portfolios-nav-ul').find('b.pofolio--1').text(total);
                                                        $('#portfolios-nav-ul').find('b.pofolio--1').next().text('All domains');
                                                        //Create Default portfolio entry
                                                        $('#portfolios-nav-ul').find('b.pofolio-0').text(r.default);
                                                        $('#portfolios-nav-ul').find('b.pofolio-0').next().text('Default');
                                                        
                                                        console.log();														
                                                }
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}											
                                        },function(r){
                                        }
                                );                                
                        };
                        $scope.bulkEditDomains = [];
                        $scope.updatePrice = function(id_domain)	{
							if($('#makeoffer-'+id_domain).is(':checked'))
									$('#price-'+id_domain).val(0);
                        };
                        $scope.bulkEditSelection = function(val){
                                $scope.bulkSelectedOption = val;
                                
                                $scope.bulkEditDomains = [];
                                //get selected domains
                                $('.domain_checkbox').each(function(i, e){
                                        if($(e).is(":checked"))
                                                $scope.bulkEditDomains.push($(e).attr('data-id_domain'));
                                });
                                
								if( $scope.bulkEditDomains.length < 1 )	{
									alert('Please select from the list.');
								}
								else	{
									var stat_dir  = '/static/';
									$('#loading_bar_BULK_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
									$('#loading_bar_BULK_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
									console.log('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
									
									
									if($scope.bulkSelectedOption == 'BULKEDIT')	{	
											$('.overlay').show();
											$http({
												method: "GET",
												url: 'ng/json/domains/?action=get_domain_records&domains='+$scope.bulkEditDomains.join(','),
											}).then(
												function(r) {
													var d = r.data;
													console.log(d);
													$scope.bulkEditDomains = d;
													setTimeout(function(){
														$.each( d, function(i, r)	{	
															console.log(r);
															
															if( r.result == undefined )	{
																toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
																alert('Session expired.');
																window.location.reload();
															}
														
														   //if(r.result_code == 0)	{	
														   if(r != null)	{
															 if(r.domain != null)	{  
															 
 																var str_portfolio_html = '';
																if( r.portfolio != null || r.portfolio != undefined )	{
																	if( r.portfolio.length > 0 )	{
																		str_portfolio_html += '<option value="0">Default</option>';
																		$.each(r.portfolio, function(i, e)	{
																			str_portfolio_html += '<option value="'+e.id_portfolio+'">'+e.title+'</option>';
																		});
																		$('#portfolio-'+r.domain.id_domain).html(str_portfolio_html);
																	}
																}
																
																var id_pofolio = r.domain.id_portfolio? r.domain.id_portfolio: 0;
																 $('#portfolio-'+r.domain.id_domain).val(id_pofolio); 
																
																//load latest category data
																var str_cat_html = '';
																if( r.category.length > 0 )	{
																	str_cat_html += '<option value="0">-</option>';
																	$.each(r.category, function(i, e)	{
																		str_cat_html += '<option value="'+e.id_category+'">'+e.title+'</option>';
																	});
																	$('#category-'+r.domain.id_domain).html(str_cat_html);
																}
																
																var id_cat = r.domain.id_category? r.domain.id_category: 0;
																 $('#category-'+r.domain.id_domain).val(id_cat); 

																var status = r.domain.status? r.domain.status: 1;
																 $('#status-'+r.domain.id_domain).val(status); 

																var published = r.domain.published == 'Y'? 1: 0;
																//if(published)	{
																	
																//$('#published-'+r.domain.id_domain).prop('checked', false);
																console.log( 'Domain ID = ' + (r.domain.id_domain) );
																console.log( 'Published = ' + (r.domain.published) );
																 if(r.domain.published == 'Y')	{
																	//$('#published-'+r.domain.id_domain).attr('checked', 1);
																	$('#published-'+r.domain.id_domain).prop('checked', true);
																	$('#published-'+r.domain.id_domain).val('Y');
																	//$("#edit-bulk-domain-modal [id=published-"+r.domain.id_domain+"]").prop('checked', true);
																 }
																else	{
																	//$('#published-'+r.domain.id_domain).removeAttr('checked');
																	$('#published-'+r.domain.id_domain).prop('checked', false);
																	$('#published-'+r.domain.id_domain).val('N');
																	//$("#edit-bulk-domain-modal [id=published-"+r.domain.id_domain+"]").prop('checked', false);
																}

																var makeoffer = parseInt(r.domain.makeoffer)? 1: 0;
																
																//$('#makeoffer-'+r.domain.id_domain).prop('checked', false);
																console.log( 'Price = ' + parseInt(r.domain.price) );
																if( parseInt(r.domain.price) < 1 )	{
																	//$('#makeoffer-'+r.domain.id_domain).attr('checked', 1);
																	$('#makeoffer-'+r.domain.id_domain).prop('checked', true);
																	//$("#edit-bulk-domain-modal [id=makeoffer-"+r.domain.id_domain+"]").prop('checked', true);
																}
																else	{
																	//$('#makeoffer-'+r.domain.id_domain).removeAttr('checked');
																	$('#makeoffer-'+r.domain.id_domain).prop('checked', false);
																	//$("#edit-bulk-domain-modal [id=makeoffer-"+r.domain.id_domain+"]").prop('checked', false);
																}
																
																$('#bulk-brandable-id_logo_edit-'+r.domain.id_domain).val(r.domain.id_logo);
																console.log( 'Logo = ' + parseInt(r.domain.id_logo) );
																if( parseInt(r.domain.id_logo) < 1 )	{
																	$("#bulk-brandable-"+r.domain.id_domain).prop('checked', false);
																	$("#bulk-brandable-"+r.domain.id_domain).val('N');
																}
																else	{
																	$("#bulk-brandable-"+r.domain.id_domain).prop('checked', true);
																	$("#bulk-brandable-"+r.domain.id_domain).val('Y');
																}
																
																$('#price-'+r.domain.id_domain).val(r.domain.price);
																$('#bulk-domain-description-'+r.domain.id_domain).val(r.domain.description);
																$('#bulk-domain-keywords-'+r.domain.id_domain).val(r.domain.keywords);
																$('#bulk-acquisition_cost-'+r.domain.id_domain).val(r.domain.acquisition_cost);
																$('#bulk-renewal_fees-'+r.domain.id_domain).val(r.domain.renewal_fees);
															 }
														   }
														});
														$('#loading_bar_BULK_EDIT').html('');
														$('.overlay').hide();
													}, 1500);
												}
												/*
												function(r)	{
													console.log(r);
													$('#loading_bar_BULK_EDIT').html('');
													$('.overlay').hide();
												}
												*/
											);
											if(!$scope.bulkEditDomains)	{
												$('#edit-bulk-domain-modal').modal('hide');
												//$('#loading_bar_BULK_EDIT').html('');
												//$('.overlay').hide();
											}
											else    {	
												$('#edit-bulk-domain-modal').modal();
												//$('#loading_bar_BULK_EDIT').html('');
												//$('.overlay').hide();
											}
									}
									if($scope.bulkSelectedOption == 'BULKDELETE')	{
										
											$('#sidebar_menu li').removeAttr('class');
											$('#li_domains').attr('class','active');
										
											$('#confirm-modal').modal();
											$('#confirm-modal').find('.message').html('Are you sure to remove selected domains? <br> This cannot be undone, all data related to these domains will be removed.');
											$('#confirm-modal').find('input.key').val('id_domains');
											$('#confirm-modal').find('input.value').val($scope.bulkEditDomains.join(','));
											//Further working is defined in page itself
									}
									if($scope.bulkSelectedOption == 'BULKPUBLISH')	{
										$('.overlay').show();
										var obj = {};
											obj['domainIDs'] = $scope.bulkEditDomains.join(',');
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/domains/?action=bulkpublish',
											data: obj
										}).success(function(data, status, headers, config)	{
											console.log( data );
											setTimeout(function()	{
												
												if( data.result != undefined )	{
													toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												
												//$('.overlay').hide();
												$('#btnRefresh').click();
											}, 1000);
											
										});
									}
									if($scope.bulkSelectedOption == 'BULKUNPUBLISH')	{
										$('.overlay').show();
										var obj = {};
											obj['domainIDs'] = $scope.bulkEditDomains.join(',');
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/domains/?action=bulkunpublish',
											data: obj
										}).success(function(data, status, headers, config)	{
											console.log( data );
											setTimeout(function()	{
												
												if( data.result != undefined )	{
													toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												//$('.overlay').hide();
												$('#btnRefresh').click();
											}, 1000);
											
										});
									}
								}
                        };
                        $scope.editDomainRows = function(id_domain)	{
								$('.overlay').show();
								$('#logo_domain_img').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
								$('#loading_bar_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
                                $('#edit-domain-modal').modal();
                                var domain = $("tr[data-id="+id_domain+"]").attr('data-domain');
                                var str_cat_html = '';
                                $http({
                                        method: "GET",
                                        url: 'ng/json/domains/?action=get_domain_record&domain='+domain,
                                }).then(
                                        function(r) {
												console.log(r);
                                                r = r.data;

												if( r.result_code == undefined )	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												
                                                //if(r.result_code == 0){
												if(r != null){
                                                        
                                                        $scope.eDomain = r.domain;
                                                        
                                                        setTimeout(function()	{
															
																var str_portfolio_html = '';
																if( r.portfolio != null || r.portfolio != undefined )	{
																	if( r.portfolio.length > 0 )	{
																		str_portfolio_html += '<option value="0">Default</option>';
																		console.log(r.portfolio);
																		$.each(r.portfolio, function(i, e)	{
																			console.log(e);
																			str_portfolio_html += '<option value="'+e.id_portfolio+'">'+e.title+'</option>';
																		});
																		$("#edit-domain-modal #id_portfolio").html(str_portfolio_html);
																	}
																}
															
															if(r.domain != null)	{
																var id_pofolio = r.domain.id_portfolio? r.domain.id_portfolio: 0;
																$("#edit-domain-modal [name=id_portfolio]").val(id_pofolio);
																
																
                                                                var id_cat = r.domain.id_category? r.domain.id_category: 0;
                                                                $("#edit-domain-modal [name=id_category]").val(id_cat);

                                                                var status = r.domain.status? r.domain.status: 1;
                                                                $("#edit-domain-modal [name=status]").val(status);

                                                                if(r.domain.published == 'Y')	{
																	//$("#edit-domain-modal [name=published]").attr('checked', 1);
																	$("#edit-domain-modal [name=published]").prop('checked', true);
																}
                                                                else	{
																	//$("#edit-domain-modal [name=published]").removeAttr('checked');
																	$("#edit-domain-modal [name=published]").attr('checked', false);
																}
                                                                
                                                                if(r.domain.price <= 0)	{
																	//$("#edit-domain-modal [name=makeoffer]").attr('checked', 1);
																	$("#edit-domain-modal [name=makeoffer]").prop('checked', true);
																}
                                                                else	{
																	//$("#edit-domain-modal [name=makeoffer]").removeAttr('checked');
																	$("#edit-domain-modal [name=makeoffer]").prop('checked', false);
																}
                                                                
                                                                if($("#edit-domain-modal [name=makeoffer]").is(":checked"))	{
                                                                    //$('[name=price]').parent().slideDown();
																}
                                                                else	{
																	//$('[name=price]').parent().slideUp();
																}
																	
                                                                if( r.domain.id_logo > 0 )	{
																	//$("#f_brandable").attr('checked', 1);
																	$("#f_brandable").prop('checked', true);
																}
                                                                else	{
																	//$("#f_brandable").removeAttr('checked');
																	$("#f_brandable").prop('checked', false);
																}
																
																
																$("#edit-domain-modal [name=price]").val(r.domain.price);
																$("#edit-domain-modal [name=acquisition_cost]").val(r.domain.acquisition_cost);
																$("#edit-domain-modal [name=renewal_fees]").val(r.domain.renewal_fees);
																
																
																console.log(r.category.length);
																
																// update the category dropdown
																if( r.category.length > 0 )	{
																	str_cat_html += '<option value="0">-</option>';
																	$.each(r.category, function(i, e)	{
																		str_cat_html += '<option value="'+e.id_category+'">'+e.title+'</option>';
																	});
																	$("#edit-domain-modal #id_category").html(str_cat_html);
																}
																
																$("#edit-domain-modal #id_category").val(r.domain.category);
																
																
																$('#logo_domain_img').html('<img src="'+zen_api_url+'image.php?id_logo='+r.domain.id_logo+'&width=110&height=50">');
																$('.overlay').hide();
																$('#loading_bar_EDIT').html('');
																
															}
															else	{
																$('.overlay').hide();
																$('#loading_bar_EDIT').html('<span class="status-message text-center bg-danger" style="padding:2px 5px;font-size:13px;">Error: Can\'t load the domain info. Please retry again.</span>');
																alert('Error: Can\'t load the domain info.');
															}
                                                        }, 1000);
                                                                                                                
                                                }
												else   { 
													$('#loading_bar_EDIT').html('<span class="status-message text-center bg-danger" style="padding:2px 5px;font-size:13px;">Error: Can\'t load the domain info. Please retry again.</span>');
													alert('Error: Can\'t load the domain info.');
													$scope.eDomain = [];
													$('.overlay').hide();
												}
                                        },
                                        function(r){
					        console.log(r);
							console.log(r.domain.id_logo);
					        $scope.eDomain = [];
				        }
                                );
                        };
						
						$scope.loadRequestLogoDesign = function()	{
							$('#request-logo-design-modal').modal();
							/*
							$('.overlay').show();
							$http({
								method: 'get',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=load_mark_as_sold_initial_data'
								//data: { userid : userid }
							}).success(function(data, status, headers, config)	{
								console.log(data);
								setTimeout(	function()	{
									if( data.result_code != undefined )	{										
										var sel_buyers = '<select class="form-control" id="sold_buyer" name="sold_buyer" onchange="checkBuyer($(this))">';
										sel_buyers += '<option value="-1"> --- Select a buyer --- </option>';
										$.each( data.resultdata, function( i, field ) {
											sel_buyers += '<option value="'+ field.id_user +'"> ' + field.first_name + ' ' + field.last_name + '</option>';
										});
										sel_buyers += '<option value="0"> --- Guest User --- </option></select>';
										sel_buyers += '<span id="load_guest_user"></span>';
										$('#load_all_buyers').html(sel_buyers);	
										$('#markSoldModal').modal();
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
								}, 1000);
							});
							*/
							return false;
						};
						$scope.saveRequestLogo = function()	{
							if( $.trim( $('#req_logo_domain_main').val() ) == '' )	{
								alert('Please provide a domain.');
								$('#req_logo_message').attr('style','border-color:none;');
							}
							else if( $.trim( $('#req_logo_message').val() ) == '' )	{
								alert('Please provide a valid message.');
								$('#req_logo_message').attr('style','border-color:#d9534f;');
								$('#req_logo_message').focus();
							}
							else	{
								$('#req_logo_message').attr('style','border-color:none;');
								$('#btnAddLogoRequest').attr('disabled','disabled');
								//$('#showloading').attr('style','display:block;margin-top:10px;');
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=addRequestLogo',
									data: getFormData( $("#frmLogoRequest") )
								}).success(function(data, status, headers, config){
									console.log(data);
									//$('.overlay').hide();
									$scope.logos = data.items;
									setTimeout(function()	{
										if( data.result_code != undefined )	{
											console.log(data);
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											$('#btnAddLogoRequest').removeAttr('disabled');
											$('#request-logo-design-modal').modal('hide');
											//$('.overlay').hide();
											$('#btnRefresh').click();
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
									}, 1000);
									
								});
							}
							return false;
						};
						
						$scope.loadMarkAsSoldData = function()	{
							$('.overlay').show();
							$http({
								method: 'get',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/domains/?action=load_mark_as_sold_initial_data'
								//data: { userid : userid }
							}).success(function(data, status, headers, config)	{
								console.log(data);
								setTimeout(	function()	{
									if( data.result_code != undefined )	{										
										var sel_buyers = '<select class="form-control" id="sold_buyer" name="sold_buyer" onchange="checkBuyer($(this))">';
										sel_buyers += '<option value="-1"> --- Select a buyer --- </option>';
										$.each( data.resultdata, function( i, field ) {
											sel_buyers += '<option value="'+ field.id_user +'"> ' + field.first_name + ' ' + field.last_name + '</option>';
										});
										sel_buyers += '<option value="0"> --- Guest User --- </option></select>';
										sel_buyers += '<span id="load_guest_user"></span>';
										$('#load_all_buyers').html(sel_buyers);	
										$('#markSoldModal').modal();
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
								}, 1000);
							});
							return false;
						};
						
						$scope.saveMarkAsSold = function()	{
							var domain = $('#sold_domain_name').val();
							if(confirm("Are you sure you really want to make this "+domain+" mark as sold ? This cannot be undoned."))	{

								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/domains/?action=save_domain_sold',
									data: getFormData( $("#frmSaveSoldDomain") )
								}).success(function(data, status, headers, config)	{
									console.log(data);
									setTimeout(	function()	{
										if( data.result_code != undefined )	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											$('#btnRefresh').click();
											$('#markSoldModal').modal('hide');
											//$('.overlay').hide();
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
											$('.overlay').hide();
										}
									}, 1000);
								});
							
							}
							return false;
						}
						
                        $scope.uploadDomainLogo = function(id_domain){
							
								$('#sidebar_menu li').removeAttr('class');
								$('#li_domains').attr('class','active');
							
								$('#logo_domain_img_main').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
								$('#loading_bar_UPLOADLOGO').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
                                $('#uploadlogo-modal').modal();
                                var domain = $("tr[data-id="+id_domain+"]").attr('data-domain');
                                
                                $http({
                                        method: "GET",
                                        url: 'ng/json/domains/?action=get_domain_record&domain='+domain,
                                }).then(
                                        function(r) {
												console.log(r);
                                                r = r.data;
												
												if( r.result_code == undefined )	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												
                                                //if(r.result_code == 0){
												if(r != null){
                                                        $scope.eDomain = r.domain;                                                        
                                                        setTimeout(function(){
																$('#logo_domain_img_main').html('<img src="'+zen_api_url+'image.php?id_logo='+r.domain.id_logo+'&width=150&height=100">');
																$('#loading_bar_UPLOADLOGO').html('');
                                                        }, 1000);
                                                                                                                
                                                }
												else   { 
													$('#loading_bar_UPLOADLOGO').html('<span class="status-message text-center bg-danger" style="padding:2px 5px;font-size:13px;">Error: Can\'t load the domain logo. Please retry again.</span>');
													alert('Error: Can\'t load the domain info.');
													$scope.eDomain = [];	
												}
                                        },
                                        function(r){
					        console.log(r);
							console.log(r.domain.id_logo);
					        $scope.eDomain = [];
				        }
                                );
                        };
                  },
                  resolve: load([
					'static/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.js',
					'http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
					'libs/jquery/doubleScroll/jquery.doubleScroll.js',
					
					'static/global/plugins/Tokenize-master/jquery.tokenize.css',
					'static/global/plugins/Tokenize-master/jquery.tokenize.js'
					
				])
              })
              .state('app.domain', {
                  url: 'domain/:domain',
                  templateUrl:"ng/domain_dashboard",
                  cache: false,
                  params: {
                        domain: ''
                  },
                  controller: function($scope, $http, toaster, $stateParams){

                        $scope.domain = {};
                        
						/*
                        $http({
                                method: "POST",
                                url: 'ng/json/domains/?action=update_domain_whois',
                                data: {domain: $scope.$state.params.domain}
                        }).success(function(data, status, headers, config){
							console.log(data);
						});
						*/
						
                        $scope.loadDomain = function()	{
							
								$('#sidebar_menu li').removeAttr('class');
								$('#li_domains').attr('class','active');
								$('#li_domains_port_main').attr('class','active');
							
								$('.overlay').show();
                                $http({
									method: "GET",
									url: 'ng/json/domains/?action=get_domain_record&domain='+$scope.$state.params.domain
                                }).then(
									function(r) {
										console.log(r);
										var data = r.data;

										if( data.result_code == undefined)	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										if(data.result_code == 0)	{
												
												if(data.domain.id_portfolio == 0)
														data.domain.portfolio_title = 'Default';
												
												$scope.domain = data.domain;
												$scope.visitors_count = data.visitors_count;
												$scope.visitors_day = data.visitors_day;
												$scope.visitors_day2 = data.visitors_day2;
												
												/*
												$scope.c_labels = [];
												$scope.c_values = [];
												if($scope.domain.statistics.visitors.record){
												
														$.each($scope.domain.statistics.visitors.data.c_labels, function(i, e){
																$scope.c_labels[$scope.labels.length] = [i, e];
														});
														$.each($scope.domain.statistics.visitors.data.values, function(i, e){
																$scope.c_values[$scope.c_values.length] = [i, e];
														});
														console.log($scope.c_values);
												}
												*/
												
											setTimeout(function()	{
												if(data.whois.result == 'ERROR')	{
													$('#whois_timestamp').html( data.whois.time + '<span class=" text-danger"> '+data.whois.result+' : '+data.whois.response_details+' </span>');
												}
												else	{
													
													 var datagraph = '\
														[ \
															{\
															  data: [[1, 3],[2, 5],[3, 1],[4, 1],[5, 5],[6, 2]] ,\
															  lines: { show: true, lineWidth: 1, fill:true, fillColor: { colors: [{opacity: 0.2}, {opacity: 0.8}] } } \
															}\
														],\
														{ \
														  colors: [\'#e8eff0\'], \
														  series: { shadowSize: 3 }, \
														  xaxis:{ show:false },\
														  yaxis:{ font: { color: \'#a1a7ac\' } },\
														  grid: { hoverable: true, clickable: true, borderWidth: 0, color: \'#dce5ec\' },\
														  tooltip: true,\
														  tooltipOpts: { content: \'%s of %x.1 is %y.4\',  defaultTheme: false, shifts: { x: 10, y: -25 } }\
														}\
													';
													
													$('#pageviewstat').attr('ui-options',datagraph);
													
													
													  var testtest = '\
													  <div ui-refresh="'+$scope.formatted_visitors+'" ui-jq="plot" ui-options="\
														[\
															{ \
															  data: '+$scope.formatted_visitors+'  , \
															  lines: { show: true, lineWidth: 1, fill:true, fillColor: { colors: [{opacity: 0.2}, {opacity: 0.8}] } } \
															}\
														], \
														{\
														  colors: [\'#e8eff0\'], \
														  series: { shadowSize: 3 }, \
														  xaxis:{ show:false }, \
														  yaxis:{ font: { color: \'#a1a7ac\' } }, \
														  grid: { hoverable: true, clickable: true, borderWidth: 0, color: \'#dce5ec\' }, \
														  tooltip: true, \
														  tooltipOpts: { content: \'%s of %x.1 is %y.4\',  defaultTheme: false, shifts: { x: 10, y: -25 } } \
														} \
													  " style="height:240px" > \
													  </div>\
													  ';
														$('#load_graph_test').html(testtest);
													
													if( data.domain.id_logo > 0 )	{
														$('#for_id_logo').attr('class','panel wrapper text-center');
													}
													else	{
														$('#for_id_logo').attr('class','panel wrapper text-center hidden');
													}
													$('#admin_note').val( data.domain.admin_note );
													

													$('#whois_timestamp').html( data.whois.time );
													
													if (data.whois.whoisinfo.registrant === undefined || data.whois.whoisinfo.registrant === null)	{	
														var registrant_name = '';
														var registrant_address = '';
													}
													else	{	
														var registrant_name = data.whois.whoisinfo.registrant.name;	
														var registrant_address = data.whois.whoisinfo.registrant.address;
													}
													
													if (data.whois.whoisinfo.domainName === undefined || data.whois.whoisinfo.domainName === null)	{	var domainName = '';	}
													else	{	var domainName = data.whois.whoisinfo.domainName;	}
													
													if (data.whois.whoisinfo.registered === undefined || data.whois.whoisinfo.registered === null)	{	var registered = '';	}
													else	{	var registered = data.whois.whoisinfo.registered;	}
													
													if (data.whois.whoisinfo.domain_registrar === undefined || data.whois.whoisinfo.domain_registrar === null)	{	var domain_registrar = '';	}
													else	{	var domain_registrar = data.whois.whoisinfo.domain_registrar;	}
													
													if (data.whois.whoisinfo.domain_created === undefined || data.whois.whoisinfo.domain_created === null)	{	var domain_created = '';	}
													else	{	var domain_created = data.whois.whoisinfo.domain_created;	}
													
													if (data.whois.whoisinfo.domain_expires === undefined || data.whois.whoisinfo.domain_expires === null)	{	var domain_expires = '';	}
													else	{	var domain_expires = data.whois.whoisinfo.domain_expires;	}
													
													if (data.whois.whoisinfo.domain_updated === undefined || data.whois.whoisinfo.domain_updated === null)	{	var domain_updated = '';	}
													else	{	var domain_updated = data.whois.whoisinfo.domain_updated;	}

													if (data.whois.whoisinfo.tld === undefined || data.whois.whoisinfo.tld === null)	{	var tld = '';	}
													else	{	var tld = data.whois.whoisinfo.tld;	}
													
													var j;
													var who_is_str = '';
													
													who_is_str += '\
														<table class="table table-responsive table-striped table-hover" >\
															<tr><td>Domain</td><td>'+domainName+'</td></tr>\
															<tr><td>Registrar</td><td>'+domain_registrar+'</td></tr>\
															<tr><td>Registered</td><td>'+registered+'</td></tr>\
															<tr><td>Registrant</td><td>'+registrant_name+'</td></tr>\
															<tr><td>Registrant address</td><td>'+registrant_address+'</td></tr>\
															<tr><td>Create date</td><td>'+domain_created+'</td></tr>\
															<tr><td>Expires</td><td>'+domain_expires+'</td></tr>\
															<tr><td>Last updated</td><td>'+domain_updated+'</td></tr>';
															
															if( data.whois.whoisinfo.status.length > 0 )	{
																who_is_str += '<tr><td>Domain Status</td><td>';
																for(j=0; j < data.whois.whoisinfo.status.length; j++)	{
																	who_is_str += '<div>'+data.whois.whoisinfo.status[j]+'</div>';
																}
																who_is_str += '</td></tr>';
															}
															
															if( data.whois.whoisinfo.servers.length > 0 )	{
																who_is_str += '<tr><td>Servers</td><td>';
																for(j=0; j < data.whois.whoisinfo.servers.length; j++)	{
																	who_is_str += '<div> '+data.whois.whoisinfo.servers[j].server+' </div>';
																}
																who_is_str += '</td></tr>';
															}
															
															if( data.whois.whoisinfo.name_servers.length > 0 )	{
																who_is_str += '<tr><td>Name Servers</td><td>';
																for(j=0; j < data.whois.whoisinfo.name_servers.length; j++)	{
																	who_is_str += '<div> '+data.whois.whoisinfo.name_servers[j]+' </div>';
																}
																who_is_str += '</td></tr>';
															}
															
														who_is_str += '</table>';
														
													
													who_is_str += ' <div class="clearfix" ></div>';
													who_is_str += ' <div class="col-sm-12" >';
															if( data.whois.whoisinfo.raw.length > 0 )	{
																who_is_str += '<hr>';
																who_is_str += '\
																				<button onclick="$(\'#whoisinfo_raw_data\').toggle(\'slow\');" type="button" class="btn btn-default pull-right">\
																					<i class="fa fa-caret-down"></i>\
																				</button>';
																
																who_is_str += '<div id="whoisinfo_raw_data">';
																for(j=0; j < data.whois.whoisinfo.raw.length; j++)	{
																	who_is_str += '		<label> '+data.whois.whoisinfo.raw[j]+' </label> <br>';
																}
																who_is_str += '</div>';
															}
													who_is_str += ' </div>';
													who_is_str += ' <div class="clearfix" ></div>';
													$('#loadWhoIsData').html(who_is_str);
												}
												$('.overlay').hide();
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_domains').attr('class','active');
												$('#li_domains_port_main').attr('class','active');
												
											}, 1000);
																								
										}
										else {
											console.log( 'return false;' );
											$scope.$state.go('app.domains'); //Move to domains table if not found
										}
									}
									/*
									function(r){
											$scope.$state.go('app.domains'); //Move to domains table on error
									}
									*/
                                );
                        };
                        $scope.adminNote = '';
                        $scope.admin_note_button = 'Update';
                        $scope.admin_note_button_disabled = '';
                        $scope.updateAdminNote = function(){
							if( $('#admin_note').val().trim() == '' )	{
								alert('Please provide a valid admin notes.');
								$('#admin_note').attr('style','border-color:#d9534f;');
								$('#admin_note').focus();
							}
							else	{
								$('#admin_note').attr('style','none;');
                                $scope.admin_note_button = 'Updating..';
                                $scope.admin_note_button_disabled = 'disabled';
								$('.overlay').show();
                                $http({
                                        method: "POST",
                                        url: 'ng/json/domains/?action=update_admin_note',
                                        data: { domain: $scope.$state.params.domain, admin_note: $('#admin_note').val().trim(), id_domain: $('#id_domain_edit').val() }
                                }).success(function(data, status, headers, config){
										console.log(data);
                                        $scope.admin_note_button = 'Update';
                                        $scope.admin_note_button_disabled = '';

										if( data.result != undefined)	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										setTimeout(function()	{
											$('.overlay').hide();
										}, 1000);
                                });
							}
                        };
                        $scope.updateDomain = function(id_domain)	{
                        
                                var $form = $('#edit-domain-modal').find('form');
                                var $form = $form.serializeArray();
                                var formdata = {};
                                $form.forEach(function (e, i) {
                                  formdata[e.name] = e.value;
                                });
                                formdata = JSON.stringify(formdata);

								$('.overlay').show();
								$('#loading_bar_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');								
                                $http({
                                        method: "POST",
                                        url: 'ng/json/domains/?action=update_domain_record',
                                        data: formdata
                                }).then(
                                        function(r)	{
											r = r.data;

											if( r.result != undefined )	{
												if(r.result_code == 0)	{	
													$('#edit-domain-modal').modal('hide');
													$scope.loadDomain();
												}													
												toaster.pop(r.result.toLowerCase(), r.response_text, r.response_details);
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											
											//$('.overlay').hide();
											$('#loading_bar_EDIT').html('');
                                        },function(r)	{
											$('#edit-domain-modal').modal('hide');
											//$('.overlay').hide();
											$('#loading_bar_EDIT').html('');
                                        }
                                );
                        };
                        $scope.editDomainRows = function(id_domain){
								$('.overlay').show();
								$('#logo_domain_img').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
								$('#loading_bar_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
                                $('#edit-domain-modal').modal();
                                var str_cat_html = '';
                                $http({
										method: "GET",
										url: 'ng/json/domains/?action=get_domain_record&domain='+$scope.domain.domain,
                                }).then(
                                        function(r) {
											
											r = r.data;
											if( r.result_code != undefined)	{
                                                if(r.result_code == 0){
                                                        
                                                        $scope.eDomain = r.domain;
                                                        
                                                        setTimeout(function(){
															
															var str_portfolio_html = '';
															if( r.portfolio != null || r.portfolio != undefined )	{
																if( r.portfolio.length > 0 )	{
																	str_portfolio_html += '<option value="0">Default</option>';
																	console.log(r.portfolio);
																	$.each(r.portfolio, function(i, e)	{
																		console.log(e);
																		str_portfolio_html += '<option value="'+e.id_portfolio+'">'+e.title+'</option>';
																	});
																	$("#edit-domain-modal #id_portfolio").html(str_portfolio_html);
																}
															}
															
															if(r.domain != null)	{
																
																var id_pofolio = r.domain.id_portfolio? r.domain.id_portfolio: 0;
																$("#edit-domain-modal [name=id_portfolio]").val(id_pofolio);
																
                                                                var id_cat = r.domain.id_category? r.domain.id_category: 0;
                                                                $("#edit-domain-modal [name=id_category]").val(id_cat);

                                                                var status = r.domain.status? r.domain.status: 1;
                                                                $("#edit-domain-modal [name=status]").val(status);
																
                                                                if(r.domain.published == 'Y')	{
																	//$("#edit-domain-modal [name=published]").attr('checked', 1);
																	$("#edit-domain-modal [name=published]").prop('checked', true);
																}
                                                                else    {
																	//$("#edit-domain-modal [name=published]").removeAttr('checked');
																	$("#edit-domain-modal [name=published]").attr('checked', false);
																}
                                                                
                                                                if(r.domain.price <= 0)	{
																	//$("#edit-domain-modal [name=makeoffer]").attr('checked', 1);
																	$("#edit-domain-modal [name=makeoffer]").prop('checked', true);
																}
                                                                else	{
																	//$("#edit-domain-modal [name=makeoffer]").removeAttr('checked');
																	$("#edit-domain-modal [name=makeoffer]").prop('checked', false);
																}
                                                                
                                                                if($("#edit-domain-modal [name=makeoffer]").is(":checked"))	{
                                                                    //$('[name=price]').parent().slideDown();
																}
                                                                else	{
																	//$('[name=price]').parent().slideUp();
																}
																
                                                                if( r.domain.id_logo > 0 )	{
																	//$("#f_brandable").attr('checked', 1);
																	$("#f_brandable").prop('checked', true);
																}
                                                                else	{
																	//$("#f_brandable").removeAttr('checked');
																	$("#f_brandable").prop('checked', false);
																}
																
																$("#edit-domain-modal [name=price]").val(r.domain.price);
																$("#edit-domain-modal [name=acquisition_cost]").val(r.domain.acquisition_cost);
																$("#edit-domain-modal [name=renewal_fees]").val(r.domain.renewal_fees);
																
																console.log(r.category.length);
																
																// update the category dropdown
																if( r.category.length > 0 )	{
																	str_cat_html += '<option value="0">-</option>';
																	$.each(r.category, function(i, e)	{
																		str_cat_html += '<option value="'+e.id_category+'">'+e.title+'</option>';
																	});
																	$("#edit-domain-modal #id_category").html(str_cat_html);
																}
																
																$("#edit-domain-modal #id_category").val(r.domain.category);
																
																
																$('#logo_domain_img').html('<img src="'+zen_api_url+'image.php?id_logo='+r.domain.id_logo+'&width=110&height=50">');
																$('.overlay').hide();
																$('#loading_bar_EDIT').html('');
																
															}
															else	{
																$('.overlay').hide();
																$('#loading_bar_EDIT').html('<span class="status-message text-center bg-danger" style="padding:2px 5px;font-size:13px;">Error: Can\'t load the domain info. Please retry again.</span>');
																alert('Error: Can\'t load the domain info.');
															}
															
															$('.overlay').hide();
															
                                                        }, 1000);
                                                                                                                
                                                }
												else   { 
													$('#loading_bar_EDIT').html('<span class="status-message text-center bg-danger" style="padding:2px 5px;font-size:13px;">Error: Can\'t load the domain info. Please retry again.</span>');
													alert('Error: Can\'t load the domain info.');
													$scope.eDomain = [];
													$('.overlay').hide();
												}
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
                                        },
                                        function(r){
											console.log(r);
											console.log(r.domain.id_logo);
											$scope.eDomain = [];
										}
                                );
                        };
                  }
              })
              .state('app.domains.sort', {
                  url: '/sort',
                  templateUrl:"ng/table_sort",
                  cache: false,
                  params:{
                        id_portfolio: -1,
                        portfolio: 'All domains',
						tab: 'Brandable'
                        //tab: 'Brandable'
                  },
                  controller: function($scope, $http, $compile, toaster){
                        var id_portfolio = $scope.$state.params.id_portfolio;
                        var portfolio = $scope.$state.params.portfolio;
                        var tab = $scope.$state.params.tab;
						
						console.log($scope.$state.params);
						
                        $scope.domains = [];
                        $scope.pagelinks = [];
                        $state.params.range = 0;
                        $state.params.loadingMessage = 'Loading records...';
						//$state.params.loadingMessage = '<img src="/static/admin/ajax-loader-bar.gif">';
                        $state.params.addDomainsModalState = '';
                        $state.params.addDomainsModalLabel = 'Submit';
                        $state.params.addDomainsErrorState = 'hidden';
                        
                        $scope.totalItems = 0;
                        $scope.currentPage = 1;
                        $scope.maxSize = 5;
                        
                        $scope.selectPage = function(pageNumber)	{
							$scope.loadPageSort(pageNumber);
                        };
                        $scope.updatePagination = function(currentPage, totalRecords, maxSize=5)	{
							$scope.currentPage = currentPage;
							$scope.totalItems = totalRecords;
							$scope.maxSize = maxSize;
							console.log($scope.maxSize);
							if( $scope.maxSize > 1 )	{
								$('.pagination').attr('style','visibility:block;');
							}
							else	{
								$('.pagination').attr('style','visibility:hidden;');
							}
							//console.log($scope.currentPage);
							//$log.info('Page changed to: ' + $scope.currentPage);
                        };
                        $scope.searchDomain = function()	{
							var keyword = $('#domain-search-input').val().trim();
							if(keyword)	{
								setTimeout($scope.loadPageSort($scope.currentPage, keyword), 2000);
							}
                        };
                        $scope.loadPageSort = function(page, keyword = '')	{
                                
								$('#sidebar_menu li').removeAttr('class');
								$('#li_domains').attr('class','active');
								$('#li_domains_sort').attr('class','active');
								
								$('.overlay').show();
								var view_limit = $('#view_limit').val();
								var maxSize = 5;
								var currentPage = 1;
								var tab = $scope.$state.params.tab;
								
                                //Initial values...
                                $state.params.loadingMessage = 'Loading records...';
								//$state.params.loadingMessage = '<img src="/static/admin/ajax-loader-bar.gif">';
                                $scope.sortdomains = [];
                                
								if( tab == 'All' )	{
									tab = 'Brandable';
								}
								
								//console.log( $state.params.tab );
								
                                var url = 'ng/json/domains?action=fetch_domain_records&id_portfolio='+$state.params.id_portfolio+'&tab='+tab+'&page='+page+'&limit='+view_limit;
                                if(keyword)
                                        url = url + '&search='+keyword.trim();
                                $http({
                                        method: 'get',
                                        url: url
                                }).success(function(data, status, headers, config)	{
									if( data.result_code != undefined)	{
                                        if(data.result_code == 0)	{
                                                if(data.formatted_records){
                                                        //Set variables
                                                        //$state.params.range = '('+(25 * (page -1))+' - '+(25 * (page))+') / '+(data.rows)+' records';
														if( view_limit != 'all')	{
															if( view_limit > 25 )	{
																$state.params.range = '('+(view_limit * (page -1))+' - '+(view_limit * (page))+') / '+(data.rows)+' records';
															}
															else	{
																$state.params.range = '('+(25 * (page -1))+' - '+(25 * (page))+') / '+(data.rows)+' records';
															}
															maxSize = 5;
														}
														else	{
															$state.params.range = 'All records';
															page = 1;
															maxSize = 1;
														}
														$state.params.loadingMessage = $scope.$state.params.portfolio;

;
                                                        //Populate current page
                                                        $scope.sortdomains = data.formatted_records;
                                                        //$scope.sortdomains.push($scope.sortdomains);

														
														console.log(data.rows);
														console.log($scope.sortdomains.length);
														if( data.rows == $scope.sortdomains.length )	{
															$state.params.range = 'All records';
															page = 1;
															maxSize = 1;
														}
														
														//console.log(page);
														
                                                        //update Pagination
                                                        $scope.updatePagination(page, data.rows, maxSize);

														//console.log($scope.sortdomains);
														//console.log($scope.sortdomains.length);
														setTimeout(function()	{ 
															$.each($scope.sortdomains, function(i, e)	{
																$('#load_sort_logo-'+e.id_domain).html(e.sort_logo);
																 //console.log(i + ' - ' + e.domain + ' - '+ e.id_domain);
																 
																 var str_marker_published = '';
																 var str_marker_offer = '';
																 var str_marker = '';
																 
																 if( e.published == 'Y' )	{
																	 //str_marker_published = '<b data-toggle="tooltip" data-placement="top" alt="Currently Published" title="Currently Published" class="badge bg-success"><i class="fa fa-check"></i></b>';
																	 str_marker_published = '<span data-toggle="tooltip" data-placement="top" alt="Currently Published" title="Currently Published" class="badge1 bg-success1 text-success"><i class="fa fa-check"></i></span>';
																	 
																	 if( data.record[i].price < 1 )	{
																		// str_marker_offer = '<b data-toggle="tooltip" data-placement="top" alt="Offer Domain" title="Offer Domain" class="badge bg-primary" style="margin-left:3px;"><i class="fa fa-envelope"></i></b>';
																		 
																		 str_marker_offer = '<b data-toggle="tooltip" data-placement="top" alt="Offer Domain" title="Offer Domain" class="badge1 bg-primary1 text-primary" style="margin-left:1px;"><i class="fa fa-envelope"></i></b>';
																	 }
																	else	{	str_marker_offer = '';	}
																	
																	//str_marker_offer = '';
																	
																	 str_marker = str_marker_published + str_marker_offer;
																	 $('#marker-'+e.id_domain).html(str_marker);
																 }
																 else	{
																	 //str_marker_published = '<b data-toggle="tooltip" data-placement="top" alt="Currently Unpublished" title="Currently Unpublished" class="badge bg-danger"><i class="fa fa-close"></i></b>';
																	 
																	 str_marker_published = '<span data-toggle="tooltip" data-placement="top" alt="Currently Published" title="Currently Published" class="badge1 bg-danger1 text-danger"><i class="fa fa-close"></i></span>';
																	 
																	 if( data.record[i].price < 1 )	{
																		 //str_marker_offer = '<b data-toggle="tooltip" data-placement="top" alt="Offer Domain" title="Offer Domain" class="badge bg-primary" style="margin-left:3px;"><i class="fa fa-envelope"></i></b>';
																		 
																		 str_marker_offer = '<b data-toggle="tooltip" data-placement="top" alt="Offer Domain" title="Offer Domain" class="badge1 bg-primary1 text-primary" style="margin-left:1px;"><i class="fa fa-envelope"></i></b>';
																	 }
																	else	{	str_marker_offer = '';	}
																	
																	//str_marker_offer = '';
																	
																	str_marker = str_marker_published + str_marker_offer;
																	 $('#marker-'+e.id_domain).html(str_marker);
																 }
																 
															});
															$('[data-toggle=tooltip]').tooltip();

														});
                                                }
                                        }else{
                                                        //Set variables
                                                        $state.params.range = 'No records found';
                                                        $state.params.loadingMessage = $scope.$state.params.portfolio;
                                                        
                                                        //update Pagination to first page/no records.
                                                        $scope.updatePagination(1, 0);
														
														$('#sidebar_menu li').removeAttr('class');
														$('#li_domains').attr('class','active');
														$('#li_domains_sort').attr('class','active');
														
                                        }										
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
									
									setTimeout(function()	{ 
										$('#sidebar_menu li').removeAttr('class');
										$('#li_domains').attr('class','active');
										$('#li_domains_sort').attr('class','active');

										$('.overlay').hide();
									}, 1000);
									
                                });
                        };
                  },
                  resolve: load(['static/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.js'])              
              })
              .state('app.sales', {
                  url: 'sales?:id_sale:type',
                  templateUrl: 'ng/table_sales',
                  cache: false,
				  controller: function($scope, $http, toaster, $stateParams)	{
                        var id_sale = $scope.$state.params.id_sale;
                        var type = $scope.$state.params.type;
						
						$scope.refreshInquries = function()	{
							var status = $('#current_menu_sel').val();
							$scope.getInquries(status);
						}
						
						$scope.getInquries = function(status=1)	{
							
							$('#sidebar_menu li').removeAttr('class');
							$('#li_sales').attr('class','active');
							
							$('#current_menu_sel').val(status);
							
							$scope.checkNavigation(status);
							
							var ajax_url = 'ng/json/sales/?action=getInquries&status='+status;
							$('.overlay').show();
							$http({
								method: 'get',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: ajax_url,
							}).success(function(data, status, headers, config)	{
								console.log(data);
								$scope.inquiries = data.inquiries.inquiries;
								setTimeout(function(){
									if( data.result != undefined)	{
										if( data.inquiries.inquiries != undefined )	{
											var status_label = '';
											$.each( data.inquiries.inquiries, function( i, inquiry ) {
												if(inquiry.offer.offer)	{
													if( inquiry.id_inquiry > 0 )	{
														//inquiry is an offer
														var pad = "0000000";
														var inq_id = (pad+inquiry.id_inquiry).slice(-pad.length);
														var inq_id = 'OFR'+inq_id;
														
														var disp_inq_id = '<a class="clickable" ui-sref="app.salesviewoffers({ idInquiry: \''+inquiry.id_inquiry+'\' })" href="#/salesviewoffers/?idInquiry='+inquiry.id_inquiry+'">'+inq_id+'</a>';
														
														var disp_inq_view = '<a class="clickable" ui-sref="app.salesviewoffers({ idInquiry: \''+inquiry.id_inquiry+'\' })" href="#/salesviewoffers/?idInquiry='+inquiry.id_inquiry+'">View</a>';
														
														if( inquiry.IPNstatus != undefined || inquiry.IPNstatus != '' || inquiry.IPNstatus != null || inquiry.IPNstatus != 'null'  )	{
															var order_status = $.parseJSON(inquiry.IPNstatus);
															if( order_status == null || order_status == undefined ||  order_status == 'null' || order_status == '')	{
																var status_code = -1;
																var transactionID = '';
																var escrow_status = '';
															}
															else	{
																var status_code = order_status.Code;
																var transactionID = order_status.EscrowUniqueIdentifier;
																var escrow_status = order_status.Description;
															}
															var status_label = '';
															if( status_code == 0 )	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')"> Cancelled </span>';
															}
															else if( status_code == 5 )	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" class="text-danger" data-toggle="tooltip" data-placement="top" title="Your Attention is Required!"> Review Required ! </span>';
															}
															else if( status_code == 15 )	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" > Waiting for Buyer\'s Payment </span>';
															}
															else if( status_code == 20 )	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')"> Payment Submitted </span>';
															}
															else if( status_code == 25 )	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" class="text-danger" data-toggle="tooltip" data-placement="top" title="Your Attention is Required!"> Payment Accepted ! </span>';
															}
															else if( status_code == -1 )	{
																status_label = 'Not Applicable';
															}
															else	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" class="text-success"> '+escrow_status+' </span>';
															}
															$('#disp-inq-ordStatus-'+inquiry.id_inquiry).html(status_label);
															$('[data-toggle=tooltip]').tooltip();
														}
														else	{
															$('#disp-inq-ordStatus-'+inquiry.id_inquiry).html('Not Applicable');
														}
													}
													else	{
														var disp_inq_id = '';
														var disp_inq_view = '';
													}
												}
												else	{
													if( inquiry.id_inquiry > 0 )	{
														//inquiry is buynow
														var pad = "0000000";
														var inq_id = (pad+inquiry.id_inquiry).slice(-pad.length);
														var inq_id = 'INQ'+inq_id;
														
														var disp_inq_id = '<a class="clickable" ui-sref="app.salesview({ idInquiry: \''+inquiry.id_inquiry+'\' })" href="#/salesview/?idInquiry='+inquiry.id_inquiry+'">'+inq_id+'</a>';
														
														var disp_inq_view = '<a class="clickable" ui-sref="app.salesview({ idInquiry: \''+inquiry.id_inquiry+'\' })" href="#/salesview/?idInquiry='+inquiry.id_inquiry+'">View</a>';
														
														//var t = '{"Code":5,"Description":"Buyer Accepted","EscrowUniqueIdentifier":902492}';
														if( inquiry.IPNstatus != undefined || inquiry.IPNstatus != '' || inquiry.IPNstatus != null || inquiry.IPNstatus != 'null'  )	{
															var order_status = $.parseJSON(inquiry.IPNstatus);
															if( order_status == null || order_status == undefined ||  order_status == 'null' || order_status == '')	{
																var status_code = -1;
																var transactionID = '';
																var escrow_status = '';
															}
															else	{
																var status_code = order_status.Code;
																var transactionID = order_status.EscrowUniqueIdentifier;
																var escrow_status = order_status.Description;
															}
															var status_label = '';
															if( status_code == 0 )	{	//Cancelled
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')"> Cancelled </span>';
															}
															else if( status_code == 5 )	{	//New Inquiry
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" class="text-danger" data-toggle="tooltip" data-placement="top" title="Your Attention is Required!"> Review Required! </span>';
															}
															else if( status_code == 15 )	{	//Pending
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" > Pending </span>';
															}
															else if( status_code == 20 )	{	//Pending
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')"> Pending </span>';
															}
															else if( status_code == 25 )	{	//Sold
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" class="text-danger" data-toggle="tooltip" data-placement="top" title="Your Attention is Required!"> Sold </span>';
															}
															else if( status_code == -1 )	{
																status_label = 'Not Applicable';
															}
															else	{
																status_label = '<span style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\'\,\''+transactionID+'\',\''+escrow_status+'\')" class="text-success"> '+escrow_status+' </span>';
															}
															$('#disp-inq-ordStatus-'+inquiry.id_inquiry).html(status_label);
															$('[data-toggle=tooltip]').tooltip();
														}
														else	{
															$('#disp-inq-ordStatus-'+inquiry.id_inquiry).html('Not Applicable');
														}
													}
													else	{
														var disp_inq_id = '';
														var disp_inq_view = '';
													}
												}

												var str_for_mobile = '\
													<span class="hidden-lg hidden-md hidden-sm"> \
														<i onclick="$(\'#mobile_sales_info_'+inquiry.id_inquiry+'\').toggle(\'slow\');" style="margin-left:5px;cursor:pointer;" class="fa fa-caret-down"> </i> \
													</span>\
													<div id="mobile_sales_info_'+inquiry.id_inquiry+'" class="hidden-lg hidden-md hidden-sm" style="margin-left:10px;">\
														<div class="row"> \
															<div class="col-xs-3"> <b> Domain </b> </div> <div class="col-xs-9"> '+inquiry.offer.domain+' </div> \
															<div class="col-xs-3"> <b> Buyer </b> </div> <div class="col-xs-9"> '+inquiry.buyer.buyerfullname+'   </div>\
															<div class="col-xs-3"> <b> Status </b> </div> <div class="col-xs-9"> '+status_label+'  </div> \
															<div class="col-xs-3"> <b> Date </b> </div> <div class="col-xs-9"> '+inquiry.f_date+'  </div> \
														</div>\
													</div>\
												';
												
												//$('#disp-inq-id-'+inquiry.id_inquiry).html(disp_inq_id);
												$('#disp-inq-id-'+inquiry.id_inquiry).html(disp_inq_id + str_for_mobile);
												$('#disp-inq-view-'+inquiry.id_inquiry).html(disp_inq_view);
											
											});
										}
									}
									else	{	
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
								}, 1000);
								
							});
							return false;
						}
						
						$scope.checkNavigation = function(id=1)	{
							$('#stat_'+id).attr('class','active');
							$('#stat_'+id).siblings().removeAttr('class');
							
							var inq_table_header = 'New Inquiries';
							if( id==1 )	{	inq_table_header = 'New Inquiries';		}
							if( id==2 )	{	inq_table_header = 'Ongoing Negotiations';		}
							if( id==0 )	{	inq_table_header = 'Reminders';		}
							if( id==3 )	{	inq_table_header = 'Pending Sale';		}
							if( id==4 )	{	inq_table_header = 'Sold';		}
							if( id==5 )	{	inq_table_header = 'Archive';		}
							
							$('#inq_header_label').html(inq_table_header);
							return false;
						}
                  }
              })
              .state('app.logos', {
                  url: 'logos',
                  templateUrl: 'ng/table_logos',
                  cache: false,
                  resolve: load([
                            '/static/global/plugins/Tokenize-master/jquery.tokenize.css',
							'/static/global/plugins/Tokenize-master/jquery.tokenize.js', 
                          ]),
                  controller: function($scope, $http, toaster, $stateParams)	{
                        $scope.$state.toaster = toaster;
						$scope.setActiveMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_market').attr('class','active');
							$('#li_market_logos').attr('class','active');
							return false;
						};
						$scope.addRequestLogo = function()	{
							if( $.trim( $('#req_logo_domain').val() ) == '' )	{
								alert('Please provide a domain.');
								$('#req_logo_message').attr('style','border-color:none;');
							}
							else if( $.trim( $('#req_logo_message').val() ) == '' )	{
								alert('Please provide a valid message.');
								$('#req_logo_message').attr('style','border-color:#d9534f;');
								$('#req_logo_message').focus();
							}
							else	{
								$('#req_logo_message').attr('style','border-color:none;');
								$('#btnAddLogoRequest').attr('disabled','disabled');
								//$('#showloading').attr('style','display:block;margin-top:10px;');
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: 'ng/json/logos/?action=addRequestLogo',
									data: getFormData( $("#frmLogoRequest") )
								}).success(function(data, status, headers, config){
									console.log(data);
									//$('.overlay').hide();
									$scope.logos = data.items;
									setTimeout(function()	{
										if( data.result_code != undefined )	{
											console.log(data);
											if( data.items != undefined )	{
												if( data.items.length > 0 )	{
													toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
													$.each( data.items, function( i, request ) {
														var disp_req_view = '<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+request.id_request+'\' })" href="#/requestlogodetails/?idRequest='+request.id_request+'">View</a>';
														$('#disp_req_view-'+request.id_request).html(disp_req_view);
													});
													$('#btnAddLogoRequest').removeAttr('disabled');
													$('#request-logo-design-modal').modal('hide');
													$('#for_empty').attr('class','hidden');
												}
											}
											else	{
												$('#for_empty').removeAttr('class');
											}
											$('.overlay').hide();
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
									}, 1000);
									
								});
							}
							return false;
						};
						$scope.getRequestLogos = function()	{	
							$scope.setActiveMenu();
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/logos/?action=getRequestLogos'
							}).success(function(data, status, headers, config){
								console.log(data);
								$scope.logos = data.items;
								setTimeout(function()	{
									if( data.result_code != undefined )	{
										console.log(data);
										if( data.items != undefined )	{
											if( data.items.length > 0 )	{
												$.each( data.items, function( i, request ) {
													var disp_req_view = '<a class="clickable" ui-sref="app.requestlogodetails({ idRequest: \''+request.id_request+'\' })" href="#/requestlogodetails/?idRequest='+request.id_request+'">View</a>';
													$('#disp_req_view-'+request.id_request).html(disp_req_view);
												});
												$('#for_empty').attr('class','hidden');
											}
										}
										else	{
											$('#for_empty').removeAttr('class');
										}
									}
									else	{
										toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										alert('Session expired.');
										window.location.reload();
									}
									$('.overlay').hide();
								}, 1000);
								


							});
							return false;
						};
                  }
              })
              .state('app.requestlogodetails', {
                  url: 'requestlogodetails/?&id_request=:idRequest',
				  cache: false,
					views: {
						'':{
							templateUrl: function(params)	{	
								$('#sidebar_menu li').removeAttr('class');
								$('#li_market').attr('class','active');
								$('#li_market_logos').attr('class','active');
								
								//console.log(params);
								return 'ng/request_details/?id_request=' + params.idRequest;
							},
							cache: false,
							controller: function($scope, $http, toaster, $stateParams)	{
								//$stateParams.idRequest;
								$scope.$state.toaster = toaster;
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_market').attr('class','active');
									$('#li_market_logos').attr('class','active');
									return false;
								};
						
								$scope.getRequestLogoDetails = function(id)	{	
									$scope.setActiveMenu();
									$('.overlay').show();
									$http({
										method: 'post',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/json/logos/?action=getRequestLogoDetails&id_request=' + id,
									}).success(function(data, status, headers, config){
										console.log(data);
										setTimeout(function()	{
											
											if( data.result_code != undefined )	{
												console.log(data);
												$scope.logo = data.data;
												if( data.data != undefined )	{
													var timestamp = new Date().getTime();
													$('#disp_domain').html(data.data.domain);
													$('#disp_id_request').html(data.data.id_request);
													$('#disp_designer').html(data.data.designer_fullname);
													$('#disp_date_requested').html(data.data.requested_date);
													$('#disp_status').html(data.data.text_status);
													$('#disp_message').html(data.data.message);
													$('#disp_avatar').html('<img src="'+api_url+'image.php?id_user='+data.data.id_user+'&width=128&height=128&timestamp='+timestamp+'" >');
												}
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											
											$('.overlay').hide();
										}, 1000);
										


									});
									return false;
								};
								
							}
						}
					}
              })
			  
              .state('app.unreadmessages', {
                  url: 'unreadmessages',
                  templateUrl: 'ng/unreadmessages',
                  cache: false,
                  controller: function($scope, $http, toaster, $stateParams)	{
                        $scope.$state.toaster = toaster;
						
						$scope.getAllUnreadMessages = function()	{	
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/sales/?action=getAllUnreadMessages'
							}).success(function(data, status, headers, config){
								console.log(data);
								if( data.result_code != undefined )	{
									setTimeout(function()	{
										//$('#btnRefresh').clik();
										
										//$('#sidebar_menu li').removeAttr('class');
										//$('#li_sales').attr('class','active');
										
										var str_mssg = '';
										if( data.count.count > 0 )	{
											$.each( data.count.inquiries, function( i, inquiry ) {
												var formatted_message;
												if( inquiry.message.length < 20 )	{
													formatted_message = inquiry.message;
												}
												else	{
													formatted_message = inquiry.message.substr(0, 20) + ' ...';
												}

												if(inquiry.offer)	{
													var inq_link = ' ui-sref="app.salesviewoffers({ idInquiry: \''+inquiry.idlink+'\' })" href="#/salesviewoffers/?idInquiry='+inquiry.idlink+'" ';
												}
												else	{
													var inq_link = ' ui-sref="app.salesview({ idInquiry: \''+inquiry.idlink+'\' })" href="#/salesview/?idInquiry='+inquiry.idlink+'" ';
												}
												
												str_mssg += ' \
													<a '+inq_link+' class="list-group-item"> \
														<span class="pull-left thumb-sm avatar m-r"> \
														  <img alt="'+inquiry.buyerfullname+'" title="'+inquiry.buyerfullname+'" src="'+inquiry.api_url+'/image.php?id_user='+inquiry.id_buyer+'&amp;width=128&amp;height=128"> \
														  <i class="on b-white bottom"></i> \
														</span> \
														<span class="clear"> \
														  <span>'+inquiry.buyerfullname+'</span> \
														  <small class="text-muted">'+formatted_message+'</small> \
														  <small class="text-muted clear text-ellipsis"><i class="fa fa-clock-o fa-fw m-r-xs"></i>'+inquiry.received_on_formatted+'</small> \
														</span> \
													</a> \
												';
											});
										}
										else	{
												str_mssg += '\
												  <a class="list-group-item clearfix"> \
													<span class="clear"> \
													  <span>No unread messages found.</span> \
													  <small class="text-muted clear text-ellipsis"></small> \
													</span> \
												  </a> \
												  ';
										}
										$('#ul_unread_messages').html(str_mssg);
										$('#span_unread_messages_count').html(data.count.count);
										$('.overlay').hide();
									}, 1000);
								}
								else	{
									toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
									alert('Session expired.');
									window.location.reload();
								}

							});
							return false;
						};
						
                  }
              })			  
              .state('app.salesview', {
                  url: 'salesview/?&id_inquiry=:idInquiry',
				  //templateUrl: 'ng/sales_view/?id_inquiry=',
				  cache: false,
                  resolve: load([
                            '/static/global/plugins/bootstrap-datepicker/css/datepicker.css',
							'/static/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', 
                          ]),
					views: {
						'':{
							templateUrl: function(params)	{	
								$('#sidebar_menu li').removeAttr('class');
								$('#li_sales').attr('class','active');
								
								console.log(params);
								return 'ng/sales_view/?id_inquiry=' + params.idInquiry;
							},
							cache: false,
							controller: function($scope, $http, toaster, $stateParams)	{
								
								$('#sidebar_menu li').removeAttr('class');
								$('#li_sales').attr('class','active');
								
								//$scope.idInq = $stateParams.idInquiry;
								$stateParams.idInquiry;
								$scope.processForm = function()	{
									if( $.trim( $('#message').val() ) == '' )	{
										alert('Please provide a valid response or message.');
										$('#message').attr('style','border-color:#d9534f;');
										$('#message').focus();
									}
									else	{
										$('#message').attr('style','border-color:none;');
										$('#processbtn').attr('disabled','disabled');
										$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/sales_view/?id_inquiry=' + $stateParams.idInquiry,
											data: getFormData( $("#frmsalesview") )
										}).success(function(data, status, headers, config){
											console.log(data);
											if( data != undefined )	{
												$('#message').val('');
												$('#processbtn').removeAttr('disabled');
												$('#showloading').attr('style','display:none;');
												//$('.overlay').hide();
												setTimeout(function()	{
													//$('#btnRefreshSalesInquiry').click();
													$('#btnRefresh').click();
													
													$('#sidebar_menu li').removeAttr('class');
													$('#li_sales').attr('class','active');
												}, 1000);
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											//window.location.reload();
										});
									}
									return false;
								};
								$scope.processFormFollowUp = function()	{
									if( $.trim( $('#followup').val() ) == '' )	{
										alert('Please provide a valid new offer price.');
									}
									else	{
										$('#processbtnFollowUp').attr('disabled','disabled');
										$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/sales_view/?id_inquiry=' + $stateParams.idInquiry,
											data: getFormData( $("#frmFollowUp") )
										}).success(function(data, status, headers, config){
											console.log(data);
											if( data != undefined )	{
												$('#processbtnFollowUp').removeAttr('disabled');
												$('#showloading').attr('style','display:none;');
												//$('.overlay').hide();
												setTimeout(function()	{
													//$('#btnRefreshSalesInquiry').click();
													$('#btnRefresh').click();
													
													$('#sidebar_menu li').removeAttr('class');
													$('#li_sales').attr('class','active');
												}, 1000);
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											//window.location.reload();
										});
									}
									return false;
								};
								$scope.saveChatMessageSalesInquiry = function(id)	{
									if( $.trim( $('#message-'+id).val() ) == '' )	{
										$('#message-'+id).attr('style','border-color:#d9534f;');
										$('#message-'+id).focus();
										alert('Please provide a valid message.');
									}
									else	{
										$('#message-'+id).attr('style','none;');
										$('#btnSaveChatMessageID-'+id).attr('disabled','disabled');
										$('#btnSaveAngularChatMessage-'+id).attr('disabled','disabled');
										//$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/sales/?&action=salesInquiryEditMessage',
											data: getFormData( $("#frmEditChatMessage-"+id) )
										}).success(function(data, status, headers, config){
											
											console.log( data );
											
											$('#btnSaveChatMessageID-'+id).removeAttr('disabled');
											$('#btnSaveAngularChatMessage-'+id).removeAttr('disabled');
											//$('#showloading').attr('style','display:none;');
											setTimeout(function()	{
												
												if( data.result != undefined)	{
													toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}

												//$('#btnRefreshSalesInquiry').click();
												$('#btnRefresh').click();
												//$('.overlay').hide();
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_sales').attr('class','active');
												
											}, 1000);
										});
									}
									
									return false;
								};
								$scope.getSalesInquiry = function(id)	{
									
									$('#sidebar_menu li').removeAttr('class');
									$('#li_sales').attr('class','active');
									
									$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'get',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/json/sales/?action=getsalesinquiry&id_inquiry=' + id,
									}).success(function(data, status, headers, config)	{
										
										console.log(data);
										
										if( data.inquiries == undefined )	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										

										
										//console.log(data.main);
										
										$scope.inquiries = data.inquiries;
										setTimeout(function(){
											if( data.labels != undefined )	{
												if( data.labels.header_info != undefined )	{
													$('#display_status_message').html(data.labels.header_info);
												}
												$('#label_date_submitted').html(data.labels.label_date_submitted);
												$('#label_buyerfullname').html(data.labels.label_buyerfullname);
												$('#label_email').html(data.labels.label_email);
											}
											if( data.main != undefined )	{
												$('#sfollowup_id_inquiry').val(data.main.id_inquiry);
												$('#sfollowup_id_buyer').val(data.main.id_buyer);
												$('#sfollowup_domain').val(data.main.domain);
												$('#newResponse_id_inquiry').val(data.main.id_inquiry);
												$('#newResponse_id_buyer').val(data.main.id_buyer);
												$('#newResponse_domain').val(data.main.domain);
												
												if(data.main.status == 1)	{
													var move_ongoing = ' \
														<div class="clearfix"></div><br /> \
														<div class="checkbox-list"> \
															<label><input type="checkbox" value="2" name="status" checked="checked"> Move domain to ongoing negotiations</label> \
														</div> \
														';
													$('#checkbox_list').html(move_ongoing);
												}
											}
											$('#sfollowup_ID_USER').val(data.s_ID_USER);
											$('#newResponse_ID_USER').val(data.s_ID_USER);
											if( data.domain != undefined )	{
												$('#domain_name_label').html(data.domain.domain);
												$('#domain_price_label').html(data.domain.price);
											}
											if( data.transactions != undefined )	{
												$('#escrow_unique_identifier_label').html(data.transactions.IPNstatus_EscrowUniqueIdentifier);
												$('#transaction-column').attr('onclick','viewStatusInfo(\''+data.transactions.IPNstatus_Code+'\',\''+data.transactions.IPNstatus_EscrowUniqueIdentifier+'\',\''+data.transactions.IPNstatus_Description+'\')');
												$('#transaction-column').html(data.transactions.display_status);
											}
											
											$scope.inquiries = data.inquiries;
											$.each( $scope.inquiries, function(i, r)	{
												var timestamp = new Date().getTime();
												if( r.sender == 'C' )	{
													//console.log('<img src="'+api_url+'image.php?id_user='+r.id_user+'&width=128&height=128&timestamp='+timestamp+'" >');
													 $('#fullsender-'+r.id_inquiry).html('You');
													 $('#avatarsender-'+r.id_inquiry).html('<img src="'+api_url+'image.php?id_user='+r.id_user+'&width=128&height=128&timestamp='+timestamp+'" >');
													 
													 var btnEditChatMessage = '<button onclick="editChatMessage(\''+r.id_inquiry+'\');" id="btnEditChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">Edit</button>';
													 $('#forButtonEditChatMessage-'+r.id_inquiry).html(btnEditChatMessage);
													 $('#btnCancelChatMessageID-'+r.id_inquiry).attr('onclick','closeChatMessage(\''+r.id_inquiry+'\')');
													 $('#btnSaveChatMessageID-'+r.id_inquiry).attr('onclick','saveChatMessage(\''+r.id_inquiry+'\')');
												}
												if( r.sender == 'B' )	{
													//console.log('<img src="'+api_url+'image.php?id_user='+r.id_buyer+'&width=128&height=128&timestamp='+timestamp+'" >');
													 $('#fullsender-'+r.id_inquiry).html('Buyer'); 
													 $('#avatarsender-'+r.id_inquiry).html('<img src="'+api_url+'image.php?id_user='+r.id_buyer+'&width=128&height=128&timestamp='+timestamp+'" >');
												}
											});
											
											/*
											var str_msg_history = '';
											$.each( $scope.inquiries, function(i, r)	{
												var timestamp = new Date().getTime();
												if( r.sender == 'C' )	{
													//console.log('<img src="'+api_url+'image.php?id_user='+r.id_user+'&width=128&height=128&timestamp='+timestamp+'" >');
													var fullsender = 'You';
													var avatarsender = '<img src="'+api_url+'image.php?id_user='+r.id_user+'&width=128&height=128&timestamp='+timestamp+'" >';
													
													 var btnEditChatMessage = '<button onclick="editChatMessage(\''+r.id_inquiry+'\');" id="btnEditChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">Edit</button>';
													 var btnCloseChatMessage = ' onclick="closeChatMessage(\''+r.id_inquiry+'\')" ';
													 var btnSaveChatMessage = ' onclick="saveChatMessage(\''+r.id_inquiry+'\')" ';
													 
													 //$('#forButtonEditChatMessage-'+r.id_inquiry).html(btnEditChatMessage);
													 //$('#btnCancelChatMessageID-'+r.id_inquiry).attr('onclick','closeChatMessage(\''+r.id_inquiry+'\')');
													 //$('#btnSaveChatMessageID-'+r.id_inquiry).attr('onclick','saveChatMessage(\''+r.id_inquiry+'\')');
												}
												if( r.sender == 'B' )	{
													var fullsender = 'Buyer';
													var avatarsender = '<img src="'+api_url+'image.php?id_user='+r.id_buyer+'&width=128&height=128&timestamp='+timestamp+'" >';
													 var btnEditChatMessage = '';
													 var btnCloseChatMessage = '';
													 var btnSaveChatMessage = '';
												}
												str_msg_history += '\
												<div class="media msg" style="padding:10px;">\
													<div class="media-body">\
														<span class="pull-left thumb-sm avatar m-r" id="avatarsender-'+r.id_inquiry+'">\
															'+avatarsender+' \
														</span>\
														<span class="clear">\
															<small class="pull-right time"><i class="fa fa-clock-o"></i> '+r.received_on+' </small>\
															<h5 class="media-heading"> <span id="fullsender-'+r.id_inquiry+'"></span></h5>\
															<span id="spanEditChatMessageLabel-'+r.id_inquiry+'" class="col-lg-10">\
																'+r.message+' \
																<span id="forButtonEditChatMessage-'+r.id_inquiry+'">\
																	'+btnEditChatMessage+' \
																</span>\
															</span>\
															<div id="divEditChatMessage-'+r.id_inquiry+'" style="display:none;">\
																<form id="frmEditChatMessage_'+r.id_inquiry+'">\
																	<div class="form-group">\
																		<div>\
																			<input type="hidden" value="'+r.id_inquiry+'" >\
																			<textarea id="message" name="message" class="form-control" rows="3" placeholder="Write a reply...">'+r.message+'</textarea>\
																		</div>\
																		<div style="margin-top:5px">\
																			<button '+btnSaveChatMessage+' id="btnSaveChatMessageID-'+r.id_inquiry+'" type="button" class="btn btn-success">Save</button>\
																			<button '+btnCloseChatMessage+' id="btnCancelChatMessageID-'+r.id_inquiry+'" type="button" class="btn btn-success">Cancel</button>\
																		</div>\
																	</div>\
																</form>\
															</div>\
														</span>\
													</div>\
												</div>\
												';
											});
											$('#load_message_history').html(str_msg_history);
											*/
											
											if( data.main.status > 1 )	{
												$('#checkbox_list').html('');
											}
											
											//if( data.transactions.IPNstatus_Code != 5 )	{
												//$('#status_message').html('');
											//}
											

											
											$('#showloading').attr('style','display:none;');
											$('.overlay').hide();
											
											$('#sidebar_menu li').removeAttr('class');
											$('#li_sales').attr('class','active');
											
										}, 1000);
									});
									return false;
								};
								
							}
						}
					}
              })
              .state('app.salesviewoffers', {
                  url: 'salesviewoffers/?&id_inquiry=:idInquiry',
				  //templateUrl: 'ng/sales_view/?id_inquiry=',
					views: {
						'':{
							templateUrl: function(params)	{
								
								$('#sidebar_menu li').removeAttr('class');
								$('#li_sales').attr('class','active');
								
								console.log(params);
								return 'ng/sales_view_offers/?id_inquiry=' + params.idInquiry;
							},
							cache: false,
							controller: function($location, $state ,$scope, $http, toaster, $stateParams, $sce)	{
								
								$('#sidebar_menu li').removeAttr('class');
								$('#li_sales').attr('class','active');
								
								console.log($scope);
								
								$stateParams.idInquiry;
								//$scope.go('issue',  $stateParams, {reload: true, inherit: false, notify: true});
								$scope.makeNewOffer = function()	{
									if( $.trim( $('#enq-new-offer').val() ) == '' )	{
										alert('Please provide a valid new offer price.');
										$('#enq-new-offer').attr('style','border-color:#d9534f;');
										$('#enq-new-offer').focus();
										$('#enq-message').attr('style','border-color:none;');
									}
									else if( $.trim( $('#enq-message').val() ) == '' )	{
										alert('Please provide a valid message to the buyer.');
										$('#enq-message').attr('style','border-color:#d9534f;');
										$('#enq-message').focus();
										$('#enq-new-offer').attr('style','border-color:none;');
									}
									else	{
										$('#enq-new-offer').attr('style','border-color:none;');
										$('#enq-message').attr('style','border-color:none;');
										$('#btn_make_new_offer').attr('disabled','disabled');
										$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/sales_view_offers/?id_inquiry=' + $stateParams.idInquiry,
											data: getFormData( $(".enquiryFormCounterOffer") )
										}).success(function(data, status, headers, config){
											console.log(data);
											if( data != undefined)	{
												$('#btn_make_new_offer').removeAttr('disabled');
												$('#showloading').attr('style','display:none;');
												$('#enq-new-offer').val('');
												$('#enq-message').val('');
												//$('.overlay').hide();
												//window.location.reload();
												setTimeout(function()	{
													$('#reviewOfferModal').modal('hide');
													//$('#btnRefreshSalesOffer').click();
													$('#btnRefresh').click();
													
													$('#sidebar_menu li').removeAttr('class');
													$('#li_sales').attr('class','active');
													
												}, 1000);
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
										});
									}
									return false;
								};
								$scope.getOfferMessages = function()	{
									var id_parent = $('#id_parent').val();
									if( id_parent < 1 )	{
										alert('ID parent inquiry is missing!');
									}
									else	{
										$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'get',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/sales/?action=getOfferMessages&id_parent=' + id_parent,
										}).success(function(data, status, headers, config)	{

											console.log( data );
											
										  if( data.result == undefined )	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
										  }
										  else	{
											$scope.offer_messages = data.all_messages;
											setTimeout(function(){
												var str_html = '';
												
												$.each(data.all_messages, function (i, r) {
																										
													if(r.sender == 'C')	{
														var user_id = r.id_user;
														var btnEditChatMessage = '<button onclick="editChatMessage(\''+r.id_inquiry+'\');" id="btnEditChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">Edit</button>';
														
														//$('#forButtonEditChatMessage-'+r.id_inquiry).html(btnEditChatMessage);
													}
													else	{
														var user_id = r.id_buyer;
														var btnEditChatMessage = '';
													}
													
													str_html += '\
														<div class="media msg">\
															<div class="media-body">\
																<span id="avatarsender-'+user_id+'" class="pull-left thumb-sm avatar m-r">\
																	<img src="'+api_url+'image.php?id_user='+user_id+'&width=128&height=128">\
																</span>\
																<span class="clear">\
																	<small class="pull-right time"><i class="fa fa-clock-o"></i> '+r.time+'</small>\
																	<h5 class="media-heading">'+r.senderfullname+'</h5>\
																	<span id="spanEditChatMessageLabel-'+r.id_inquiry+'">\
																		'+r.message+' \
																		<span id="forButtonEditChatMessage-'+r.id_inquiry+'">'+btnEditChatMessage+'</span> \
																	</span>\
																	\
																	<div id="divEditChatMessage-'+r.id_inquiry+'" style="display:none;">\
																		<form id="frmEditChatMessage-'+r.id_inquiry+'">\
																			<div class="form-group">\
																				<div>\
																					<input type="hidden" value="'+r.id_inquiry+'" name="EditChatInquiryMessageID" id="EditChatInquiryMessageID" >\
																					<input name="id_inquiry-'+r.id_inquiry+'" id="id_inquiry-'+r.id_inquiry+'" value="'+r.id_inquiry+'" type="hidden">\
																					<input name="id_user-'+r.id_inquiry+'" id="id_user-'+r.id_inquiry+'" value="'+r.id_user+'" type="hidden">\
																					<input name="id_buyer-'+r.id_inquiry+'" id="id_buyer-'+r.id_inquiry+'" value="'+r.id_buyer+'" type="hidden">\
																					<input name="id_parent-'+r.id_inquiry+'" id="id_parent-'+r.id_inquiry+'" value="'+r.id_parent+'" type="hidden">\
																					<input name="domain-'+r.id_inquiry+'" id="domain-'+r.id_inquiry+'" value="'+r.domain+'" type="hidden">\
																					<input name="sender-'+r.id_inquiry+'" id="sender-'+r.id_inquiry+'" value="C" type="hidden">\
																					<input name="email-'+r.id_inquiry+'" id="email-'+r.id_inquiry+'" value="'+r.email+'" type="hidden">\
																					<input name="oldmessage-'+r.id_inquiry+'" id="oldmessage-'+r.id_inquiry+'" value="'+r.message+'" type="hidden">\
																					<textarea id="message-'+r.id_inquiry+'" name="message-'+r.id_inquiry+'" class="form-control" rows="3" placeholder="Write a reply...">'+r.message+'</textarea>\
																				</div>\
																				<div style="margin-top:5px">\
																					<!-- <button style="display:none;" ng-click="saveChatMessageSalesOffer(\''+r.id_inquiry+'\')" id="btnSaveAngularChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">SaveAngular</button> --> \
																				   \
																					<button onclick="saveChatMessage(\''+r.id_inquiry+'\')" type="button" class="btn btn-success" id="btnSaveChatMessageID-'+r.id_inquiry+'" >Save</button> \
																					<button onclick="closeChatMessage(\''+r.id_inquiry+'\')" id="btnCancelChatMessageID-'+r.id_inquiry+'" type="button" class="btn btn-success">Cancel</button>\
																				</div>\
																			</div>\
																		</form>\
																	</div>\
																	\
																</span>\
															</div>\
														</div>\
													';
												});

												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_sales').attr('class','active');
												
												$('#load_message_history').html(str_html);
												$('.overlay').hide();
											});
										  }
										});
									}
									return false;
								};
								$scope.sendOfferMessage = function()	{
									if( $('#message_offer').val().trim() == '' )	{
										alert('Please provde a valid message');
										$('#message_offer').attr('style','border-color:#d9534f;');
										$('#message_offer').focus();
									}
									else	{
										//var test = getFormData( $("#frmOfferMessageHistosry") );
										//console.log( test );
										$('#message_offer').attr('style','border-color:none;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/sales/?action=saveOfferMessages',
											data: getFormData( $("#frmOfferMessageHistosry") )
										}).success(function(data, status, headers, config){
											console.log(data);
											$scope.offer_messages = data;
											setTimeout(function(){
												var str_html = '';
												
												$.each(data, function (i, r) {
													if(r.sender == 'C')	{
														var user_id = r.id_user;
														var btnEditChatMessage = '<button onclick="editChatMessage(\''+r.id_inquiry+'\');" id="btnEditChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">Edit</button>';
													}
													else	{
														var user_id = r.id_buyer;
														var btnEditChatMessage = '';
													}
													
													str_html += '\
														<div class="media msg">\
															<div class="media-body">\
																<span id="avatarsender-'+user_id+'" class="pull-left thumb-sm avatar m-r">\
																	<img src="'+api_url+'image.php?id_user='+user_id+'&width=128&height=128">\
																</span>\
																<span class="clear">\
																	<small class="pull-right time"><i class="fa fa-clock-o"></i> '+r.time+'</small>\
																	<h5 class="media-heading">'+r.senderfullname+'</h5>\
																	<span id="spanEditChatMessageLabel-'+r.id_inquiry+'">\
																		'+r.message+' \
																		<span id="forButtonEditChatMessage-'+r.id_inquiry+'">'+btnEditChatMessage+'</span> \
																	</span>\
																	\
																	<div id="divEditChatMessage-'+r.id_inquiry+'" style="display:none;">\
																		<form id="frmEditChatMessage-'+r.id_inquiry+'">\
																			<div class="form-group">\
																				<div>\
																					<input type="hidden" value="'+r.id_inquiry+'" name="EditChatInquiryMessageID" id="EditChatInquiryMessageID" >\
																					<input name="id_inquiry-'+r.id_inquiry+'" id="id_inquiry-'+r.id_inquiry+'" value="'+r.id_inquiry+'" type="hidden">\
																					<input name="id_user-'+r.id_inquiry+'" id="id_user-'+r.id_inquiry+'" value="'+r.id_user+'" type="hidden">\
																					<input name="id_buyer-'+r.id_inquiry+'" id="id_buyer-'+r.id_inquiry+'" value="'+r.id_buyer+'" type="hidden">\
																					<input name="id_parent-'+r.id_inquiry+'" id="id_parent-'+r.id_inquiry+'" value="'+r.id_parent+'" type="hidden">\
																					<input name="domain-'+r.id_inquiry+'" id="domain-'+r.id_inquiry+'" value="'+r.domain+'" type="hidden">\
																					<input name="sender-'+r.id_inquiry+'" id="sender-'+r.id_inquiry+'" value="C" type="hidden">\
																					<input name="email-'+r.id_inquiry+'" id="email-'+r.id_inquiry+'" value="'+r.email+'" type="hidden">\
																					<input name="oldmessage-'+r.id_inquiry+'" id="oldmessage-'+r.id_inquiry+'" value="'+r.message+'" type="hidden">\
																					<textarea id="message-'+r.id_inquiry+'" name="message-'+r.id_inquiry+'" class="form-control" rows="3" placeholder="Write a reply...">'+r.message+'</textarea>\
																				</div>\
																				<div style="margin-top:5px">\
																					<!-- <button style="display:none;" ng-click="saveChatMessageSalesOffer(\''+r.id_inquiry+'\')" id="btnSaveAngularChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">SaveAngular</button> --> \
																				   \
																					<button onclick="saveChatMessage(\''+r.id_inquiry+'\')" type="button" class="btn btn-success" id="btnSaveChatMessageID-'+r.id_inquiry+'" >Save</button> \
																					<button onclick="closeChatMessage(\''+r.id_inquiry+'\')" id="btnCancelChatMessageID-'+r.id_inquiry+'" type="button" class="btn btn-success">Cancel</button>\
																				</div>\
																			</div>\
																		</form>\
																	</div>\
																	\
																</span>\
															</div>\
														</div>\
													';
												});

												$('#message_offer').val('');
												$('#load_message_history').html(str_html);
												$('.overlay').hide();
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_sales').attr('class','active');
												
											});
										});
									}
									return false;
								};
								$scope.saveChatMessageSalesOffer = function(id,offr_mssg)	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_sales').attr('class','active');
												
									if( $.trim( $('#message-'+id).val() ) == '' )	{
										$('#message-'+id).attr('style','border-color:#d9534f;');
										$('#message-'+id).focus();
										alert('Please provide a valid message.');
									}
									else	{
										$('#message-'+id).attr('style','none;');
										$('#btnSaveChatMessageID-'+id).attr('disabled','disabled');
										$('#btnSaveAngularChatMessage-'+id).attr('disabled','disabled');
										//$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/sales/?&action=salesInquiryEditMessage',
											data: getFormData( $("#frmEditChatMessage-"+id) )
										}).success(function(data, status, headers, config){
											
											console.log( data );
											
											$('#btnSaveChatMessageID-'+id).removeAttr('disabled');
											$('#btnSaveAngularChatMessage-'+id).removeAttr('disabled');
											//$('#showloading').attr('style','display:none;');
											setTimeout(function()	{
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												
												if(offr_mssg == 1)	{
													$('#btnSendOfferMessage').click(); // refresh offer messages
												}
												else	{
													//$('#btnRefreshSalesOffer').click();
													$('#btnRefresh').click();
												}
												//$('.overlay').hide();
												
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_sales').attr('class','active');
												
											}, 1000);
										});
									}
									
									return false;
								};
								$scope.getSalesOffer = function(id)	{
									
									$('#sidebar_menu li').removeAttr('class');
									$('#li_sales').attr('class','active');
									
									$scope.allinquiries = [];
									$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'get',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/json/sales/?action=getsalesoffer&id_inquiry=' + id,
									}).success(function(data, status, headers, config)	{

										console.log(data);
										console.log(data.main);
										console.log(data.main.latest_offer);
										
										$scope.maindata = data.main;
										//$scope.inquiries = data.inquiries.reverse();
										//var allinquiries = data.inquiries.reverse();
										
										$scope.inquiries = data.inquiries;
										setTimeout(function(){
											$scope.buyers_last_offer = data.main.latest_offer;
											//$scope.inquiries = data.inquiries.reverse();
											$scope.inquiries = data.inquiries;
											$.each( $scope.inquiries, function(i, r)	{
												if( r.sender == 'C' )	{
													 //$('#sender-'+r.id_inquiry).html('You');
													var str_sender = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+r.id_user+'">\
																<img alt="You" title="You" class="img-circle" src="'+api_url+'image.php?id_user='+r.id_user+'&amp;width=180&amp;height=180">\
															</span>\
														';
														
													 $('#sender-'+r.id_inquiry).html(str_sender);
													 $('#inqstatus-'+r.id_inquiry).html('COUNTER OFFER');
													 $('#inqstatusMobile-'+r.id_inquiry).html('COUNTER OFFER');
													 
													 var btnEditChatMessage = '<button onclick="editChatMessage(\''+r.id_inquiry+'\');" id="btnEditChatMessage-'+r.id_inquiry+'" type="button" class="btn btn-success">Edit</button>';
													 $('#forButtonEditChatMessage-'+r.id_inquiry).html(btnEditChatMessage);
													 $('#btnCancelChatMessageID-'+r.id_inquiry).attr('onclick','closeChatMessage(\''+r.id_inquiry+'\')');
													 $('#btnSaveChatMessageID-'+r.id_inquiry).attr('onclick','saveChatMessage(\''+r.id_inquiry+'\')');
												}
												if( r.sender == 'B' )	{
													 //$('#sender-'+r.id_inquiry).html('Buyer'); 
													var str_sender = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+r.id_buyer+'">\
																<img alt="'+r.buyerfullname+'" title="'+r.buyerfullname+'" class="img-circle" src="'+api_url+'image.php?id_user='+r.id_buyer+'&amp;width=180&amp;height=180">\
															</span>\
														';
														
													 $('#sender-'+r.id_inquiry).html(str_sender);
													 $('#inqstatus-'+r.id_inquiry).html('RESUBMITTED');
													 $('#inqstatusMobile-'+r.id_inquiry).html('RESUBMITTED');
												}
												if( r.id_inquiry == data.main.id_inquiry)	{
													$('#inqstatus-'+r.id_inquiry).html('SUBMITTED');
													$('#inqstatusMobile-'+r.id_inquiry).html('SUBMITTED');
												}
												$('#inqoffer-'+r.id_inquiry).html(r.offer);
											});
											
											
											if( data.main.status == 3 || data.main.status == 4 )	{
												
												//headers
												var latest_offer_received_on = '';
												var latest_offer_message = '';
												var accepted_offer = '';
												var whoaccepted = '';
												var header_accepted_main = '';
												if( data.main.latest_offer_decided_by == 'C'  )	{
													header_accepted_main = 'You accepted buyer\'s offer';
													whoaccepted = 'Seller';
													accepted_offer = data.main.latest_buyer_offer;
													latest_offer_message = data.main.latest_buyer_offer_message;
													latest_offer_received_on = data.main.latest_buyer_offer_received_on;
												}
												else	{
													header_accepted_main = 'Buyer accepted your offer';
													whoaccepted = 'Buyer';
													accepted_offer = data.main.latest_offer;
													latest_offer_message = data.main.latest_offer_message;
													latest_offer_received_on = data.main.latest_offer_received_on;
												}
												
												$('#check_decline_status').remove();
												
												$('#btnAccept').attr('disabled','disabled');
												$('#btnCounterOffer').attr('disabled','disabled');
												$('#btnDecline').attr('disabled','disabled');
												
												$('#btnAccept').attr('class','btn disabled');
												$('#btnCounterOffer').attr('class','btn disabled');
												$('#btnDecline').attr('class','btn disabled');

												var ID__Inquiry = '';
												var Decided__By = '';
												// table row
												$('#check_inquiry_status').attr('style','border: 2px solid #55a119;');
												if( data.main.latest_offer_decided_by == 'C'  )	{
													//$('#check_inquiry_status_decidedby').html('<b>You <i class="fa fa-thumbs-up" ></i> <b>');
													var str_decided_by = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_user+'">\
																<img alt="You" title="You" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_user+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Seller \
														';
													var str_sender = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_buyer+'">\
																<img alt="'+data.main.buyerfullname+'" title="'+data.main.buyerfullname+'" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_buyer+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Buyer \
														';
														
													$('#check_inquiry_status_decidedby').html( str_decided_by + ' <i class="fa fa-thumbs-up" ></i>');
													
													//$('#check_inquiry_status_sender').html('Buyer');
													$('#check_inquiry_status_sender').html(str_sender);

													
													$('#inquiry-'+data.main.latest_buyer_offer_id_inquiry).attr('style','border: 2px solid #55a119;');
													ID__Inquiry = data.main.latest_buyer_offer_id_inquiry;
													Decided__By = '<b>You <i class="fa fa-thumbs-up" ></i> <b>';
												}
												else	{
													var str_decided_by = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_buyer+'">\
																<img alt="'+data.main.buyerfullname+'" title="'+data.main.buyerfullname+'" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_buyer+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Buyer \
														';
													var str_sender = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_user+'">\
																<img alt="You" title="You" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_user+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Seller \
														';
														
													$('#check_inquiry_status_decidedby').html( str_decided_by + ' <br> Buyer <i class="fa fa-thumbs-up" ></i>');
													//$('#check_inquiry_status_sender').html('<b>You<b>');
													$('#check_inquiry_status_sender').html(str_sender);
													$('#inquiry-'+data.main.latest_offer_id_inquiry).attr('style','border: 2px solid #55a119;');
													ID__Inquiry = data.main.latest_offer_id_inquiry;
													Decided__By = '<b>Buyer <i class="fa fa-thumbs-up" ></i> <b>';
												}

												
												var check_inquiry_status_message = '';
												check_inquiry_status_message += '<b>US $'+accepted_offer+'</b><p>'+latest_offer_message+'</p>';
												check_inquiry_status_message += '\
													<div class="hidden-lg hidden-md hidden-sm">\
														<p><b>Decided By : </b> '+Decided__By+' </p>\
														<p><b>Status : </b> <span id="inqstatusMobile-'+ID__Inquiry+'">ACCEPTED</span> </p>\
														<p><b>Time : </b> '+data.main.received_on+' </p>\
													</div>\
													';
										
												$('#check_inquiry_status_custom').html('ACCEPTED');
												$('#check_inquiry_status_message').html(check_inquiry_status_message);
												$('#check_inquiry_status_time').html(latest_offer_received_on);
												

												var header_accepted_info = 'Final offer price of this domain is $'+accepted_offer+' accepted by the '+whoaccepted;
												
												var header_html = '';
												header_html += '\
													   <h2><b id="header_accepted_main">'+header_accepted_main+'</b></h2>\
													   <p class="text-success container-fluid notification success" >\
														  <i class="fa fa-check-circle fa-2"></i>\
														  <i class="fa fa-exclamation-circle fa-2"></i> \
														  <span id="header_accepted_info">'+header_accepted_info+'</span>\
													  </p>\
													';
												$('#header_check_status').html(header_html);
												
												$('#loadSendMessageButton').html('<button onclick="$(\'#div_message_history\').show(\'slow\');$(\'#btnLoadOfferMessages\').click();$(\'#message_offer\').attr(\'style\',\'border-color:#none;\');" class="btn btn-success" id="btnSendOfferMessage" >Send Message</button>');

												if( data.transaction != null )	{
													var IPNstatus = $.parseJSON(data.transaction.IPNstatus);
													//console.log( IPNstatus );
													
													var status_code = IPNstatus.Code;
													var transaction_id = IPNstatus.EscrowUniqueIdentifier;
													var transaction_desc = IPNstatus.Description;
													var date_order = data.transaction.date_order;
													
													var display_desc = '';
													if( status_code == 5 )	{
														display_desc = 'Review Required';
													}
													else if( status_code == 15 )	{
														display_desc = 'Waiting for Buyer\'s Payment';
													}
													else	{
														display_desc = transaction_desc;
													}
													
													display_desc = '<a style="cursor:pointer;text-decoration:underline;" onclick="viewStatusInfo(\''+status_code+'\', \''+transaction_id+'\',\''+transaction_desc+'\')">'+display_desc+'</a>';
													transaction_id = '<b>'+transaction_id+'</b>';
													
												}
												else	{
													//console.log( 'No escrow created.' );
													var status_code = '';
													var transaction_id = '';
													var transaction_desc = '';
													var date_order = '';
													var display_desc = 'No Order / Transaction Found';
												}
												
												var transaction_html = '';
													transaction_html += '\
														<div class="panel panel-default">\
															<div class="row">\
																<div class="col-md-12">\
																	<div class="panel-heading" >\
																		<span onclick="$(\'#div_orderinfo\').toggle(\'slow\');" style="cursor:pointer;"> Order Info <i class="fa fa-caret-down"></i> </span>\
																		<span class="pull-right">\
																			<span id="btnSendOfferMessage" style="cursor:pointer;text-decoration:underline;" onclick="$(\'#div_message_history\').show(\'slow\');$(\'#btnLoadOfferMessages\').click();$(\'#message_offer\').attr(\'style\',\'border-color:#none;\');" alt="View Messages" title="View Messages"><i class="fa fa-fw m-r-xs  fa-toggle-down"></i> Send Message </span>\
																		</span>\
																	</div>\
																</div>\
															</div>\
															<div id="div_orderinfo" container-fluid>\
															<table class="table table-condensed">\
																<tbody>\
																	<tr>\
																		<td>\
																			<div class="row">\
																				<div class="col-md-2">Escrow Transaction ID</div>\
																				<div class="col-md-10"> '+transaction_id+' </div>\
																			</div>\
																		</td>\
																	</tr>\
																	<tr>\
																		<td>\
																			<div class="row">\
																				<div class="col-md-2">Status</div>\
																				<div class="col-md-10"> '+display_desc+' </div>\
																			</div>\
																		</td>\
																	</tr>\
																	<tr>\
																		<td>\
																			<div class="row">\
																				<div class="col-md-2">Date Ordered Created</div>\
																				<div class="col-md-10"> '+date_order+' </div>\
																			</div>\
																		</td>\
																	</tr>\
																	<tr>\
																		<td>\
																			<div class="row">\
																				<div class="col-md-2">Price</div>\
																				<div class="col-md-10"> '+accepted_offer+' </div>\
																			</div>\
																		</td>\
																	</tr>\
																</tbody>\
															</table>\
															</div>\
															\
														</div>\
												';
												/*
												transaction_html += '\
													<h3>Order Info</h3>\
													<div>\
														<table class="table table-striped table-bordered table-advance table-hover">\
															<thead>\
																<tr>\
																  <th style="width:20%">Escrow Transaction ID</th>\
																  <th>Status</th>\
																  <th style="width:20%">Date Ordered Created</th>\
																  <th style="width:15%">Price</th>\
																</tr>\
															</thead>\
															<tbody>\
																<tr>\
																	<td>'+transaction_id+'</td>\
																	<td>'+display_desc+'</td>\
																	<td>'+date_order+'</td>\
																	<td>'+accepted_offer+'</td>\
																</tr>\
															</tbody>\
														</table>\
													</div>\
													';
												*/
												$('#loadOrderInfo').html(transaction_html);
											}
											
											if( data.main.status == 6 )	{
												$('#check_inquiry_status').remove();

												//headers
												var latest_offer_received_on = '';
												var latest_offer_message = '';
												var accepted_offer = '';
												var whoaccepted = '';
												var header_accepted_main = '';
												if( data.main.latest_offer_decided_by == 'C'  )	{
													header_accepted_main = 'You decline buyer\'s offer';
													whoaccepted = 'Seller';
													accepted_offer = data.main.latest_buyer_offer;
													latest_offer_message = data.main.latest_buyer_offer_message;
													latest_offer_received_on = data.main.latest_buyer_offer_received_on;
												}
												else	{
													header_accepted_main = 'Buyer decline your offer';
													whoaccepted = 'Buyer';
													accepted_offer = data.main.latest_offer;
													latest_offer_message = data.main.latest_offer_message;
													latest_offer_received_on = data.main.latest_offer_received_on;
												}
												
												var ID__Inquiry = '';
												var Decided__By = '';
												// table row
												$('#check_decline_status').attr('style','border: 2px solid #d21724;');
												if( data.main.latest_offer_decided_by == 'C'  )	{
													
													var str_decided_by = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_user+'">\
																<img alt="You" title="You" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_user+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Seller \
														';
													var str_sender = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_buyer+'">\
																<img alt="'+data.main.buyerfullname+'" title="'+data.main.buyerfullname+'" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_buyer+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Buyer \
														';
													
													$('#check_decline_status_decidedby').html( str_decided_by + ' <i class="fa fa-thumbs-down" ></i>');
													$('#check_decline_status_sender').html(str_sender);
													$('#inquiry-'+data.main.latest_buyer_offer_id_inquiry).attr('style','border: 2px solid #d21724;');
													ID__Inquiry = data.main.latest_buyer_offer_id_inquiry;
													Decided__By = '<b>You <i class="fa fa-thumbs-up" ></i> <b>';
												}
												else	{
													
													var str_decided_by = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_buyer+'">\
																<img alt="'+data.main.buyerfullname+'" title="'+data.main.buyerfullname+'" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_buyer+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Buyer \
														';
													var str_sender = '\
															<span class="thumb-sm avatar m-r" id="avatarsender-'+data.main.id_user+'">\
																<img alt="You" title="You" class="img-circle" src="'+api_url+'image.php?id_user='+data.main.id_user+'&amp;width=180&amp;height=180">\
															</span>\
															<br> Seller \
														';
													
													$('#check_decline_status_decidedby').html( str_decided_by + ' <i class="fa fa-thumbs-down" ></i> ');
													$('#check_decline_status_sender').html(str_sender);
													$('#inquiry-'+data.main.latest_offer_id_inquiry).attr('style','border: 2px solid #d21724;');
													ID__Inquiry = data.main.latest_offer_id_inquiry;
													Decided__By = '<b>Buyer <i class="fa fa-thumbs-up" ></i> <b>';
												}
												
												var check_inquiry_status_message = '';
												check_inquiry_status_message += '<b>US $'+accepted_offer+'</b><p>'+latest_offer_message+'</p>';
												check_inquiry_status_message += '\
													<div class="hidden-lg hidden-md hidden-sm">\
														<p><b>Decided By : </b> '+Decided__By+' </p>\
														<p><b>Status : </b> <span id="inqstatusMobile-'+ID__Inquiry+'">DECLINE</span> </p>\
														<p><b>Time : </b> '+latest_offer_received_on+' </p>\
													</div>\
													';
										
												$('#check_decline_status_custom').html('DECLINED');
												$('#check_decline_status_message').html(check_inquiry_status_message);
												$('#check_decline_status_time').html(latest_offer_received_on);
												
												//headers
												var whodeclined = '';
												var header_declined_main = '';
												if( data.main.latest_offer_decided_by == 'C'  )	{
													header_declined_main = 'You declined buyer\'s offer';
													whodeclined = 'Seller';
													var header_declined_info = 'You have been declined the buyers offer but you can still make a counter offer if that works.';
												}
												else	{
													header_declined_main = 'Buyer declined your offer';
													whodeclined = 'Buyer';
													var header_declined_info = 'Sorry, you\'re latest offer have been declined by the '+whodeclined+' but you can still make a counter offer if that works.';
												}
												
												//var header_declined_info = 'Sorry, you\'re latest offer have been declined by the '+whodeclined+' but you can still make a counter offer if that works.';
												
												var header_html = '';
												header_html += '\
													   <h2><b id="header_accepted_main">'+header_declined_main+'</b></h2>\
													   <p class="text-success container-fluid notification failure" >\
														  <i class="fa fa-check-circle fa-2"></i>\
														  <i class="fa fa-exclamation-circle fa-2"></i> \
														  <span id="header_accepted_info">'+header_declined_info+'</span>\
													  </p>\
													';
												$('#header_check_status').html(header_html);
												$('#btnDecline').attr('disabled','disabled');
											}
											
											if( data.main.status != 3 && data.main.status != 4 && data.main.status != 6 )	{
												$('#check_inquiry_status').remove();
												$('#check_decline_status').remove();

												$('#btnAccept').attr('class','btn btn-success accept-sellers-offer');
												$('#btnCounterOffer').attr('class','btn btn-default counter-offer');
												$('#btnDecline').attr('class','btn btn-danger btn-default decline-offer');
												
												$('#btnAccept').removeAttr('disabled');
												$('#btnCounterOffer').removeAttr('disabled');
												$('#btnDecline').removeAttr('disabled');
												
											}
											
											$('#id_parent').val(data.main.id_inquiry);
											$('#id_user').val(data.main.id_user);
											$('#id_buyer').val(data.main.id_buyer);
											$('#domain').val(data.main.domain);
											$('#sender').val(data.main.sender);
											$('#decided_by').val(data.main.decided_by);
											if( data.main.decided_by == 'C' )	{
												$('#offer').val(data.main.latest_buyer_offer);
											}
											else	{
												$('#offer').val(data.main.latest_offer);
											}
											
											$('#showloading').attr('style','display:none;');
											$('.overlay').hide();
											
											if( data.main.status == 3 || data.main.status == 4 )	{
												console.log( '$(\'#btnSendOfferMessage\').click()' ); 
												//$('#btnSendOfferMessage').click();
											}
											
											$('#sidebar_menu li').removeAttr('class');
											$('#li_sales').attr('class','active');
											
										}, 1000);
									});
									return false;
								};
								$scope.loadWholeOffer = function(id)	{
									$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'get',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/sales_view_offers/?id_inquiry=' + id,
										//url: 'ng/sales_view_offers/?id_inquiry=' + $stateParams.idInquiry,
									}).success(function(data, status, headers, config)	{
										$('#showloading').attr('style','display:none;');
										$('.overlay').hide();
											
											console.log($state);
											console.log($scope);
											//console.log(data);
											//console.log(status);
											console.log(headers);
											console.log(config);
											
											//$state.go($state.current.name, {}, {reload: true});	// working but replacing the html content does not
											setTimeout(function(){
												//window.localStorage.setItem('content', data);
												//$scope.testdata = window.localStorage.getItem('content');
												//$scope.renderHtml = function (htmlCode) {
													//return $sce.trustAsHtml(htmlCode);
												//};
												//$scope.testdata = window.localStorage.getItem('content');
												//$scope.testdata_a = $sce.trustAsHtml(data);
												//$scope.testdata_a = data;
												//$scope.testdata_a = data;
												
												//$('.app-content-body').html(data);
												//$('#load_main_content').html(data);
												//$scope.$apply();
												//$scope.data = data;
												//$scope.datatest = 'HELLO WORLD JAMES';
												//$scope.datatest = data;

												$state.go($state.current.name, {}, {reload: true});	// working but replacing the html content does not
												//$state.go('app.salesview', {'idInquiry': '408', }, {reload: true});	// working but replacing the html content does not
												//$scope.$apply();
												// $location.path('app.salesviewoffers');
												//$scope.buyer_recent_offer = data;
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_sales').attr('class','active');
												
                                            }, 1000);
									});
									return false;
								};
								$scope.acceptOffer = function()	{
									$('#btn_accept_offer').attr('disabled','disabled');
									$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'post',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/sales_view_offers/?id_inquiry=' + $stateParams.idInquiry,
										data: getFormData( $(".enquiryFormAcceptOffer") )
									}).success(function(data, status, headers, config){
										$('#btn_accept_offer').removeAttr('disabled');
										$('#showloading').attr('style','display:none;');
										//$('.overlay').hide();
										//window.location.reload();
										setTimeout(function()	{
											$('#confirmModal').modal('hide');
											//$('#btnRefreshSalesOffer').click();
											$('#btnRefresh').click();
											
											$('#sidebar_menu li').removeAttr('class');
											$('#li_sales').attr('class','active');
											
										}, 1000);
									});
									return false;
								};
								$scope.declineOffer = function()	{
									$('#btn_decline_offer').attr('disabled','disabled');
									$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'post',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/sales_view_offers/?id_inquiry=' + $stateParams.idInquiry,
										data: getFormData( $(".enquiryFormDeclinetOffer") )
									}).success(function(data, status, headers, config){
										$('#btn_decline_offer').removeAttr('disabled');
										$('#showloading').attr('style','display:none;');
										//$('.overlay').hide();
										//window.location.reload();
										setTimeout(function()	{
											$('#confirmDeclineModal').modal('hide');
											//$('#btnRefreshSalesOffer').click();
											$('#btnRefresh').click();
											
											$('#sidebar_menu li').removeAttr('class');
											$('#li_sales').attr('class','active');
											
										}, 1000);
									});
									return false;
								};
							}
						}
					},
                  cache: false
              })
              .state('app.fesettings', {
					url: 'fesettings',
					views: {
						'':{
							templateUrl: function(params)	{
								console.log(params);
								return 'ng/fesettings';
							},
							cache: false,
							controller: function($scope, $http, toaster, $stateParams)	{
								$scope.submitFrontEndSettings = function(){
										//$('#processbtn').attr('disabled','disabled');
										//$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/update-settings',
											data: getFormData( $("#formFeSettings") )
										}).success(function(data, status, headers, config){
											//$('#processbtn').removeAttr('disabled');
											//$('#showloading').attr('style','display:none;');
											//$('.overlay').hide();
											setTimeout(	function()	{
												$('#btnRefresh').click();
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											}, 1000);
										});
									return false;
								};
								
								$scope.getFrontEndSettings = function(userid)	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_settings').attr('class','active');
									$('#li_fe_settings').attr('class','active');
									
									$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'post',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: 'ng/json/update-settings/?action=get_fe_settings',
										data: { userid : userid }
									}).success(function(data, status, headers, config)	{
										$('#showloading').attr('style','display:none;');
										$('.overlay').hide();
											console.log(data);
											setTimeout(	function()	{
												
												if( data.is_vis_brandable > 0 )
													$('#brandable').prop('checked', true);
												else
													$('#brandable').prop('checked', false);
												
												if( data.is_vis_premium > 0 )	
													$('#premium').prop('checked', true);
												else
													$('#premium').prop('checked', false);
												
												if( data.is_vis_under1k > 0 )	
													$('#under1k').prop('checked', true);
												else
													$('#under1k').prop('checked', false);
											
												if( data.is_vis_websites > 0 )
													$('#websites').prop('checked', true);
												else
													$('#websites').prop('checked', false);
												
												if( data.is_vis_sold > 0 )
													$('#sold').prop('checked', true);
												else
													$('#sold').prop('checked', false);
												
												if( data.is_vis_checkout > 0 )
													$('#checkout').prop('checked', true);
												else
													$('#checkout').prop('checked', false);
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_settings').attr('class','active');
												$('#li_fe_settings').attr('class','active');
												
                                            }, 1000);
									});
									return false;
								};
								
							}
						}
					},
				  cache: false
              })
			  .state('app.domaindropped', {
				  url: 'domaindropped',
				  views: {
					  '':{
							templateUrl: function(params)	{
								console.log(params);
								return 'ng/droppeddomains';
							},
							cache: false,
							controller: function($scope, $http, toaster, $stateParams)	{
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_dropped_domains').attr('class','active');
									return false;
								}
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=5)	{
										$scope.currentPage = currentPage;
										$scope.totalItems = totalRecords;
										$scope.maxSize = maxSize;
										if( $scope.maxSize > 1 )	{
											$('.pagination').attr('style','visibility:block;');
										}
										else	{
											$('.pagination').attr('style','visibility:hidden;');
										}
								};
								$scope.loadPage = function(page, keyword = '')	{
									
									$scope.setActiveMenu();
									
									var view_limit = $('#view_limit').val();
									var view_publish = $('#view_publish').val();
									var view_status = 'dropped';
									var maxSize = 5;
									var currentPage = 1;
									var sortby = $('#sortby').val();
									var sortorder = $('#sortorder').val();

									//Initial values...
									$('.overlay').show();
									$state.params.loadingMessage = 'Loading records...';
									//$state.params.loadingMessage = '<img src="/static/admin/ajax-loader-bar.gif">';
									$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
									//$scope.domains = [];
									
									var ajax_url = 'ng/json/domains?action=fetch_domain_records'+'&page='+page+'&limit='+view_limit+'&sortby='+sortby+'&sortorder='+sortorder+'&status='+view_status;

									$('#domain-search-input').attr('disabled','disabled');
									
									if(keyword)	{
										ajax_url = ajax_url + '&search='+keyword.trim();
										$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
									}

									$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
									$('.overlay').show();
									$http({
										method: 'get',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: ajax_url,
									}).success(function(data, status, headers, config)	{
										
										$('.overlay').hide();
										
										console.log(data);
										if( data.result_code != undefined )	{
											if( data.result_code == 0 )	{
												if(data.formatted_records)	{
													$scope.domains = data.formatted_records;
													if( view_limit != 'all')	{
														if( view_limit > 25 )	{
															$state.params.range = '('+(view_limit * (page -1))+' - '+(view_limit * (page))+') / '+(data.rows)+' records';
														}
														else	{
															$state.params.range = '('+(25 * (page -1))+' - '+(25 * (page))+') / '+(data.rows)+' records';
														}
														maxSize = 5;
													}
													else	{
														$state.params.range = 'All records';
														page = 1;
														maxSize = 1;
													}
													$('#load_list_loading_image').html('');
													if( data.rows == $scope.domains.length )	{
														$state.params.range = 'All records';
														page = 1;
														maxSize = 1;
													}
													$scope.updatePagination(page, data.rows, maxSize);

													
												}
											}
											else	{
												//alert('No records found');
												//Set variables
												$state.params.range = 'No records found';
												//$state.params.loadingMessage = $scope.$state.params.portfolio;
												$('#load_list_loading_image').html('');
												
												//update Pagination to first page/no records.
												$scope.updatePagination(1, 0);
												$('.overlay').hide();
											}
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										$scope.setActiveMenu();
									});
									return false;
								};
							}
					  }
				  },
				  cache: false
			  })
			  .state('app.domainsold', {
				  url: 'domainsold',
				  views: {
					  '':{
							templateUrl: function(params)	{
								console.log(params);
								return 'ng/mysolddomains';
							},
							cache: false,
							controller: function($scope, $http, toaster, $stateParams)	{
								$scope.moreOderDetails = function(id,id_buyer)	{
									$('#more-details-modal-'+id).modal();
									return false;
								}
								$scope.moreDetails = function(id,id_buyer)	{
									
										$('#sidebar_menu li').removeAttr('class');
										$('#li_domains').attr('class','active');
										$('#li_sold_domains').attr('class','active');
									
										$('.overlay').show();
										$http({
											method: 'get',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/domains/?&action=get_order_details&id_buyer='+id_buyer,
											data: '',
										}).success(function(data, status, headers, config)	{
											console.log(data);
											setTimeout(	function()	{
												if( data.status != undefined)	{
													if( data.status == 'success')	{
														$('#buyerfullname').val( data.info.first_name + ' ' + data.info.last_name );
														$('#buyeremail').val( data.info.email );
														$('#more-details-modal-'+id).modal();
													}
													else	{
														toaster.pop('error', 'BUYER UNKNOWN', 'Buyer info not found.');
														alert('Buyer info not found');
													}
												}
												else	{
													toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
													alert('Session expired.');
													window.location.reload();
												}
												$('.overlay').hide();
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_domains').attr('class','active');
												$('#li_sold_domains').attr('class','active');
												
											}, 1000);
										});
									return false;
								}
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=5)	{
										$scope.currentPage = currentPage;
										$scope.totalItems = totalRecords;
										$scope.maxSize = maxSize;
										if( $scope.maxSize > 1 )	{
											$('.pagination').attr('style','visibility:block;');
										}
										else	{
											$('.pagination').attr('style','visibility:hidden;');
										}
								};
								$scope.loadPage = function(page, keyword = '')	{
									
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_sold_domains').attr('class','active');
									
									var view_limit = $('#view_limit').val();
									var view_publish = $('#view_publish').val();
									var view_status = 'sold';
									var maxSize = 5;
									var currentPage = 1;
									var sortby = $('#sortby').val();
									var sortorder = $('#sortorder').val();

									//Initial values...
									$('.overlay').show();
									$state.params.loadingMessage = 'Loading records...';
									//$state.params.loadingMessage = '<img src="/static/admin/ajax-loader-bar.gif">';
									$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
									//$scope.domains = [];
									
									var ajax_url = 'ng/json/domains?action=fetch_domain_records'+'&page='+page+'&limit='+view_limit+'&sortby='+sortby+'&sortorder='+sortorder+'&status='+view_status;

									$('#domain-search-input').attr('disabled','disabled');
									
									if(keyword)	{
										ajax_url = ajax_url + '&search='+keyword.trim();
										$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
									}

									$('#load_list_loading_image').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif" alt="Loading Records...." title="Loading Records....">');
									$('.overlay').show();
									$http({
										method: 'get',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: ajax_url,
									}).success(function(data, status, headers, config)	{
										
										$('.overlay').hide();
										
										console.log(data);
										if( data.result_code != undefined )	{
											if( data.result_code == 0 )	{
												if(data.formatted_records)	{
													$scope.domains = data.formatted_records;
													if( view_limit != 'all')	{
														if( view_limit > 25 )	{
															$state.params.range = '('+(view_limit * (page -1))+' - '+(view_limit * (page))+') / '+(data.rows)+' records';
														}
														else	{
															$state.params.range = '('+(25 * (page -1))+' - '+(25 * (page))+') / '+(data.rows)+' records';
														}
														maxSize = 5;
													}
													else	{
														$state.params.range = 'All records';
														page = 1;
														maxSize = 1;
													}
													$('#load_list_loading_image').html('');
													if( data.rows == $scope.domains.length )	{
														$state.params.range = 'All records';
														page = 1;
														maxSize = 1;
													}
													$scope.updatePagination(page, data.rows, maxSize);

													
												}
											}
											else	{
												//alert('No records found');
												//Set variables
												$state.params.range = 'No records found';
												//$state.params.loadingMessage = $scope.$state.params.portfolio;
												$('#load_list_loading_image').html('');
												
												//update Pagination to first page/no records.
												$scope.updatePagination(1, 0);
												$('.overlay').hide();
											}
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											alert('Session expired.');
											window.location.reload();
										}
										
										$('#sidebar_menu li').removeAttr('class');
										$('#li_domains').attr('class','active');
										$('#li_sold_domains').attr('class','active');
									});
									return false;
								};
								
							}
					  }
				  },
				  cache: false
			  })
              .state('app.domaincategories', {
					url: 'domaincategories',
					views: {
						'':{
							templateUrl: function(params)	{								
								$('#sidebar_menu li').removeAttr('class');
								$('#li_domains').attr('class','active');
								$('#li_domain_categories').attr('class','active');
													
								console.log(params);
								return 'ng/domaincategory';
							},
							cache: false,
							controller: function($scope, $http, toaster, $stateParams)	{
								$scope.addCategory = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									if( $.trim( $('#title').val() ) == '' )	{
										alert('Please provide a valid category title.');
										$('#title').attr('style','border-color:#d9534f;');
										$('#title').focus();
										$('#description').attr('style','none;');
									}
									else if( $.trim( $('#description').val() ) == '' )	{
										alert('Please provide some valid description.');
										$('#description').attr('style','border-color:#d9534f;');
										$('#description').focus();
										$('#title').attr('style','none;');
									}
									else	{
										$('#title').attr('style','none;');
										$('#description').attr('style','none;');
										$('#btnAddCategory').attr('disabled','disabled');
										//$('#showloading').attr('style','display:block;margin-top:10px;');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/category/?action=addCategory',
											data: getFormData( $("#frmDomainCategory") )
										}).success(function(data, status, headers, config){
											setTimeout(function()	{
												
												$('#frmDomainCategory #title').val('');
												$('#frmDomainCategory #description').val('');
												
												$('#btnAddCategory').removeAttr('disabled');
												//$('#showloading').attr('style','display:none;');
												//$('.overlay').hide();
												$('#btnRefresh').click();
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												$('#add-domain-category-modal').modal('hide');
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_domains').attr('class','active');
												$('#li_domain_categories').attr('class','active');
												
											}, 1000);
										});
									}
									return false;
								};
								$scope.editCategory = function(id)	{
										$('#sidebar_menu li').removeAttr('class');
										$('#li_domains').attr('class','active');
										$('#li_domain_categories').attr('class','active');
									
										$('.overlay').show();
										$http({
											method: 'get',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/category/?action=editCategory&id_category='+id
										}).success(function(data, status, headers, config){
											setTimeout(function()	{
												console.log(data);
												$('#edit_id_category').val(data.id_category);
												$('#edit_title').val(data.title);
												$('#edit_description').val(data.description);
												$('#edit-domain-category-modal').modal();
												$('.overlay').hide();
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_domains').attr('class','active');
												$('#li_domain_categories').attr('class','active');
											}, 1000);
										});
									return false;
								};
								$scope.saveEditCategory = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									if( $.trim( $('#edit_title').val() ) == '' )	{
										alert('Please provide a valid category title.');
										$('#edit_title').attr('style','border-color:#d9534f;');
										$('#edit_title').focus();
										$('#edit_description').attr('style','none;');
									}
									else if( $.trim( $('#edit_description').val() ) == '' )	{
										alert('Please provide some valid description.');
										$('#edit_description').attr('style','border-color:#d9534f;');
										$('#edit_description').focus();
										$('#edit_title').attr('style','none;');
									}
									else	{
										$('#edit_title').attr('style','none;');
										$('#edit_description').attr('style','none;');
										$('#btnEditCategory').attr('disabled','disabled');
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/category/?action=saveEditCategory',
											data: getFormData( $("#frmEditDomainCategory") )
										}).success(function(data, status, headers, config){
											setTimeout(function()	{
												$('#btnEditCategory').removeAttr('disabled');
												$('#btnRefresh').click();
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												$('#edit-domain-category-modal').modal('hide');
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_domains').attr('class','active');
												$('#li_domain_categories').attr('class','active');
											}, 1000);
										});
									}
									return false;
								};
								$scope.deleteCategory = function(id,title,countdomain)	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									if( countdomain > 0 )	{
										alert('Can\'t removed this category. There are still domains belong to this category.');
									}
									else	{
										if(confirm("Are you sure you want to remove this category '"+title+"'?"))	{
											$('.overlay').show();
											$http({
												method: 'post',
												headers: {'Content-Type': 'application/x-www-form-urlencoded'},
												url: 'ng/json/category/?action=deleteCategory',
												data: { id_category:id }
											}).success(function(data, status, headers, config){
												setTimeout(function()	{
													$('#btnRefresh').click();
													toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
													
													$('#sidebar_menu li').removeAttr('class');
													$('#li_domains').attr('class','active');
													$('#li_domain_categories').attr('class','active');
													
												}, 1000);
											});
											return false;
										}
									}
								};
								$scope.searchCategory = function()	{
									
										$('#sidebar_menu li').removeAttr('class');
										$('#li_domains').attr('class','active');
										$('#li_domain_categories').attr('class','active');
									
										$('.overlay').show();
										$http({
											method: 'post',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/category/?action=searchCategory',
											data: { keyword : $('#category-search-input').val() }
										}).success(function(data, status, headers, config){
												$scope.categories = data.categories;
												
												console.log(data.categories);
												console.log(data);
												
												setTimeout(function(){
													$('.overlay').hide();
													
													$('#sidebar_menu li').removeAttr('class');
													$('#li_domains').attr('class','active');
													$('#li_domain_categories').attr('class','active');
													
												}, 1000);
										});
									return false;
								};
								$scope.bulkEditDomainCategorySelection = function(val)	{
									
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									$scope.bulkSelectedOption = val;
									$scope.bulkEditCategories = [];
									
									//get selected categories
									$('.category_checkbox').each(function(i, e){
										if($(e).is(":checked"))	{
											$scope.bulkEditCategories.push($(e).attr('data-id_category'));
										}
									});
									
									if( $scope.bulkEditCategories.length < 1 )	{
										alert('Please select from the list.');
									}
									else	{
										//var stat_dir  = '/static/';
										//$('#loading_bar_BULK_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
										//$('#loading_bar_BULK_EDIT').html('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
										//console.log('<img src="'+stat_dir+'admin/ajax-loader-bar.gif">');
										
										if($scope.bulkSelectedOption == 'BULKEDIT')	{
											
											console.log( $scope.bulkEditCategories.join(',') );
											
											$('.overlay').show();
											$http({
												method: "GET",
												url: 'ng/json/category/?action=get_category_records&categories='+$scope.bulkEditCategories.join(','),
											}).then( function(response, status, headers, config) {

												console.log(response);
												console.log($scope.bulkEditCategoriesData);											
												$scope.bulkEditCategoriesData = response.data;

												setTimeout(function()	{
													//$('#loading_bar_BULK_EDIT').html('');
													$('.overlay').hide();
													$('#edit-bulk-category-modal').modal();
													
													$('#sidebar_menu li').removeAttr('class');
													$('#li_domains').attr('class','active');
													$('#li_domain_categories').attr('class','active');
												}, 1000);
												
											});											
										}
										if($scope.bulkSelectedOption == 'BULKDELETE')	{
											$scope.bulkDeleteCategories = [];
											var deleteCategories = {};
											var deleteCategorieslabel = {};
											$('.category_checkbox').each(function(i, e){
												if($(e).is(":checked"))	{
													//$scope.bulkDeleteCategories[$(e).attr('data-id_category')] = $(e).attr('data-countdomain');
													$scope.bulkDeleteCategories.push($(e).attr('data-id_category'));
													deleteCategories[$(e).attr('data-id_category')] = $(e).attr('data-countdomain')
													deleteCategorieslabel[$(e).attr('data-id_category')] = $(e).attr('data-title')
												}
											});
											//console.log($scope.bulkDeleteCategories);
											console.log(deleteCategories);
											
											var cnt_domain_can_be_deleted = 0;
											$scope.bulkDeleteCategoriesCanBeDeleted = [];
											$.each( deleteCategories, function(i, cat)	{
												if( cat < 1 )	{
													strhtml += '<li>'+deleteCategorieslabel[i]+'</li>';
													cnt_domain_can_be_deleted++;
													$scope.bulkDeleteCategoriesCanBeDeleted.push(i);
												}
											});
											
											var cnt_domain_cannot_be_deleted = 0;
											$.each( deleteCategories, function(i, cat)	{
												if( cat > 0 )	{
													strhtml += '<li>'+deleteCategorieslabel[i]+'</li>';
													cnt_domain_cannot_be_deleted++;
												}
											});
											
											var strhtml = '';
											if( cnt_domain_can_be_deleted > 0 )	{
												strhtml += 'Are you sure you want to remove the following categories? ';
												strhtml += '<ul>';
												$.each( deleteCategories, function(i, cat)	{
													if( cat < 1 )	{
														strhtml += '<li>'+deleteCategorieslabel[i]+'</li>';
													}
													//console.log(i);
													//console.log(cat);
												});
												strhtml += '</ul>';
											}
											if( cnt_domain_cannot_be_deleted > 0 )	{
												strhtml += '<div>The following categories can\'t be removed because there are some domain still exist.</div>';
												strhtml += '<ul>';
												$.each( deleteCategories, function(i, cat)	{
													if( cat > 0 )	{
														strhtml += '<li>'+deleteCategorieslabel[i]+'</li>';
													}
												});
											}
											
											strhtml += '<input type="hidden" name="deleteCategories" id="deleteCategories" value="'+$scope.bulkDeleteCategoriesCanBeDeleted.join(',')+'">';
											
											if( cnt_domain_can_be_deleted < 1 )	{
												$('#btnSaveDeleteBulkCategories').attr('disabled','disabled');
											}
											else	{
												$('#btnSaveDeleteBulkCategories').removeAttr('disabled','disabled');
											}
											
											$('#deleteCategoryMessage').html(strhtml);
											$('#delete-bulk-category-modal').modal();
											
										}
									}
									
									return false;
								};
								$scope.saveDeleteBulkCategories = function()	{
									
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									if( $.trim( $('#deleteCategories').val() ) != '' )	{
										$('#saveDeleteBulkCategories').attr('disabled','disabled');
										$('.overlay').show();
										$http({
											method: 'get',
											headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											url: 'ng/json/category/?action=saveDeleteBulkCategories&categories='+$('#deleteCategories').val(),
										}).success(function(data, status, headers, config){
											
											console.log(data);
											
											setTimeout(function()	{
												$('#saveDeleteBulkCategories').removeAttr('disabled');
												$('#btnRefresh').click();
												//toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												$('#delete-bulk-category-modal').modal('hide');
												
												$('#sidebar_menu li').removeAttr('class');
												$('#li_domains').attr('class','active');
												$('#li_domain_categories').attr('class','active');
												
											}, 1000);
										});
									}
									return false;
								};
								$scope.updateBulkCategories = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									var $form = $('#edit-bulk-category-modal').find('form').find('[name]');
									var $form = $form;

									var found_error = false;
									var key;
									var records = {};
									var cnt_categories_form = 0;
									$form.each(function(i, e)	{	
										console.log(e);
										console.log(e.id);
										if( $(e).val().trim() == '' )	{
											found_error = true;
											$(e).attr('style','border-color:#f05050');
											alert('Please provide a valid ' + e.name);
											return false;
										}
										else	{
											$(e).attr('style','border-color:#cfdadd');
										}
										
										if(e.name == 'id_category')	{
											key = e.value;
											records[key] = {};
											cnt_categories_form++;
										}
										records[key][e.name] = $(e).val();
									});
									
									console.log(records);
									console.log(cnt_categories_form);
									
									if( found_error )	{
										return false;
									}
									
									var cnt_records = 0;
									$('.overlay').show();
									$.each(records, function(i, e)	{
										var formdata = JSON.stringify(e);
										//console.log(formdata);
										$http({
												method: "POST",
												url: 'ng/json/category/?action=bulk_update_category_record',
												data: formdata
										}).then( 
											function(r) {													
												r = r.data;
												setTimeout(function()	{
													if(r.result_code == 200)	{
														cnt_records++;
														console.log(cnt_records);
														if( cnt_records == cnt_categories_form )	{
															$('.overlay').hide();
															$('#btnRefresh').click();
															toaster.pop(r.result.toLowerCase(), r.response_text, r.response_details);
															$('#edit-bulk-category-modal').modal('hide');
														}
														
														$('#bulk-select').val(0);
														
														$('#sidebar_menu li').removeAttr('class');
														$('#li_domains').attr('class','active');
														$('#li_domain_categories').attr('class','active');
														
													}
												}, 1000);
											},function(r){
												$('#edit-bulk-category-modal').modal('hide');
											}
										);
									});
								}
								$scope.loadDomainCategories = function(issearch=1)	{
									
									$('#sidebar_menu li').removeAttr('class');
									$('#li_domains').attr('class','active');
									$('#li_domain_categories').attr('class','active');
									
									var ajax_url = 'ng/json/category/?action=getall';
									if( issearch )	{
										var search = $.trim( $('#category-search-input').val() );
										var ajax_url = 'ng/json/category/?action=getall&search='+search;
									}
									
									$scope.alldomaincategories = [];
									//$('#showloading').attr('style','display:block;margin-top:10px;');
									$('.overlay').show();
									$http({
										method: 'get',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: ajax_url,
										//url: 'ng/json/category/?action=getall',
									}).success(function(data, status, headers, config)	{
										
										//$scope.inquiries = data.c;
										$scope.categories = data.categories;
										
										console.log(data.categories);
										console.log(data);
										
										setTimeout(function(){
											//$('#showloading').attr('style','display:none;');
											$('.overlay').hide();
											
											$('#sidebar_menu li').removeAttr('class');
											$('#li_domains').attr('class','active');
											$('#li_domain_categories').attr('class','active');
											
										}, 1000);
										
									});
									return false;
								};
							}
						}
					},
				  cache: false
              })
              .state('app.myaccount', {
                  url: 'myaccount',
                  templateUrl: 'ng/my_account',
                  cache: false,
              })
              .state('app.membership', {
                  url: 'membership',
                  templateUrl: 'ng/membership',
                  cache: false,
                  controller: function($scope, $http, toaster)	{
						$scope.setActiveMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_membership').attr('class','active');
							$('#li_membership_status').attr('class','active');
							return false;
						};
						$scope.processForm = function(type)	{
							var form = $scope.form;
							$scope.form.action = 'membership_payment';
							console.log($scope);
							console.log(form);
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: '/ng/json/update-membership',
								data: JSON.stringify(form)
							}).success(function(data, status, headers, config)	{
								toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
							});
							return false;
						};
                  }
              })
              .state('app.settings', {
                  url: 'settings',
                  template:'<div ui-view class="fade-in-up"></div>'
              })
              .state('app.settings.info', {
                  url: '/info',
                  templateUrl: 'ng/settings_info',
                  cache: false,
                  controller: function($scope, $http, toaster, $stateParams)	{
						$scope.selectEditMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_settings').attr('class','active');
							$('#li_edit_profile').attr('class','active');
							$('.overlay').hide();
							return false;
                        };
						
                        $scope.processForm = function(type)	{
							if(  $('#password').val().trim() == '' )	{
								alert('Please provide a valid new password!');
								$('#password').attr('style','border-color:#d9534f;');
								$('#password').focus();
								$('#confpwd').attr('style','border-color:none;');
							}
							else if(  $('#confpwd').val().trim() == '' )	{
								alert('Please provide a valid confirm password!');
								$('#confpwd').attr('style','border-color:#d9534f');
								$('#confpwd').focus();
								$('#password').attr('style','border-color:none;');
							}
							else if(  $('#password').val().trim() != $('#confpwd').val().trim() )	{
								alert('New password does not match with the confirm password!');
								$('#password').attr('style','border-color:#d9534f');
								$('#password').focus();
								$('#confpwd').attr('style','border-color:none;');
							}
							else	{
								$('#password').attr('style','border-color:none;');
								$('#confpwd').attr('style','border-color:none;');
								$('.overlay').show();
								var form = $scope.form;
								$scope.form.action = 'update_account';
								//console.log($scope);
								//console.log(form);
								
								console.log( (form) );
								//console.log( JSON.stringify(form) );
								console.log(  getFormData( $("#form_settings_info") ) );
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: '/ng/json/update-settings',
									//data: JSON.stringify(form)
									data: getFormData( $("#form_settings_info") )
								}).success(function(data, status, headers, config)	{
									console.log( data );
									//setTimeout(function(){ 
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										$('.overlay').hide();
									//});
								});
							}
							return false;
                        };
						$scope.uploadAvatar = function()	{
							if(  $('#file').val().trim() == '' )	{
								alert('Please select an avatar to upload.');
							}
							else	{
								$('.overlay').show();
								var avatarfile = $('#form_upload_avatar').find('[name=file]')[0].files[0]; //Get first file...
								var formData = new FormData();
								formData.append("file", avatarfile);
								$http({
									method: 'post',
									 headers: {'Content-Type': undefined },
									url: '/ng/json/update-settings?action=uploadAvatar',
									data: formData,
									transformRequest: angular.identity
								}).success(function(data, status, headers, config)	{
									if( data.result == 'SUCCESS' )	{
										var response_details = 'Your avatar file has been uploaded.';
									}
									else	{
										var response_details = data.response_details;
									}
									
									toaster.pop(data.result.toLowerCase(), data.response_text, response_details);
									console.log( data );
									setTimeout(function()	{ 
										var timestamp = new Date().getTime();
										//toaster.pop(data.result.toLowerCase(), data.response_text, response_details);
										$('#avatar_image').html('');
										$('#form_upload_avatar').find('[name=file]').val('');
										$('.overlay').hide();
										console.log('<img src="'+api_url+'image.php?id_user='+id_user+'&width=150&height=150&timestamp='+timestamp+'" >');
										$('#avatar_image').html('<img id="avatar_image_src" src="'+api_url+'image.php?id_user='+id_user+'&width=150&height=150&timestamp='+timestamp+'" >').fadeIn();
										$('#avatar_image').html('<img id="avatar_image_src" src="'+api_url+'image.php?id_user='+id_user+'&width=150&height=150&timestamp='+timestamp+'" >').fadeIn();
										
										$('#avatar_header').html('<img id="avatar_image_src" src="'+api_url+'image.php?id_user='+id_user+'&width=128&height=128&timestamp='+timestamp+'" >').fadeIn();
										
										
										//$('#avatar_image_src').attr('').;
										
									});
								},1000);
							}
							return false;
						};
                  },
				  cache: false
              })
              .state('app.settings.payment', {
                  url: '/payment',
                  templateUrl: 'ng/settings_payment',
                  cache: false,
				  controller: function($scope, $http, toaster, $stateParams)	{
                        $scope.processForm = function(type)	{
                        
							if(  $('#escrow_email').val().trim() == '' )	{
								alert('Please provide a valid Escrow username/email!');
								$('#escrow_email').attr('style','border-color:#d9534f;');
								$('#escrow_email').focus();
								
								$('#escrow_password').attr('style','border-color:none;');
								$('#escrow_partner_id').attr('style','border-color:none;');
								$('#escrow_partner_id_staging').attr('style','border-color:none;');
							}
							else if(  $('#escrow_password').val().trim() == '' )	{
								alert('Please provide a valid Escrow password!');
								$('#escrow_password').attr('style','border-color:#d9534f;');
								$('#escrow_password').focus();
								
								$('#escrow_email').attr('style','border-color:none;');
								$('#escrow_partner_id').attr('style','border-color:none;');
								$('#escrow_partner_id_staging').attr('style','border-color:none;');
							}
							else if(  $('#escrow_partner_id').val().trim() == '' )	{
								alert('Please provide a valid Escrow Partner ID (Production)');
								$('#escrow_partner_id').attr('style','border-color:#d9534f;');
								$('#escrow_partner_id').focus();
								
								$('#escrow_email').attr('style','border-color:none;');
								$('#escrow_password').attr('style','border-color:none;');
								$('#escrow_partner_id_staging').attr('style','border-color:none;');
							}
							else if(  $('#escrow_partner_id_staging').val().trim() == '' )	{
								alert('Please provide a valid Escrow Partner ID (Staging)');
								$('#escrow_partner_id_staging').attr('style','border-color:#d9534f;');
								$('#escrow_partner_id_staging').focus();
								
								$('#escrow_email').attr('style','border-color:none;');
								$('#escrow_password').attr('style','border-color:none;');
								$('#escrow_partner_id').attr('style','border-color:none;');
							}
							else	{
								$('#escrow_email').attr('style','border-color:none;');
								$('#escrow_password').attr('style','border-color:none;');
								$('#escrow_partner_id').attr('style','border-color:none;');
								$('#escrow_partner_id_staging').attr('style','border-color:none;');
								
								$('.overlay').show();
								var form = $scope.form;
								$scope.form.action = 'update_escrow';
								//console.log($scope);
								//console.log(form);
								console.log(form);
								console.log(getFormData( $("#form_settings_payment") ));
								$http({
										method: 'post',
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										url: '/ng/json/update-settings',
										//data: JSON.stringify(form),
										data: getFormData( $("#form_settings_payment") )
								}).success(function(data, status, headers, config)	{
									console.log( data );
									//setTimeout(function(){ 
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
										$('.overlay').hide();
									//});
								});
							}
							return false;
							
                        };
						
						$scope.getPaymentAppSettings = function(userid)	{
							
							$('#sidebar_menu li').removeAttr('class');
							$('#li_settings').attr('class','active');
							$('#li_payment_settings').attr('class','active');
							
							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/update-settings/?action=get_app_settings',
								data: { userid : userid }
							}).success(function(data, status, headers, config)	{
								//$('.overlay').hide();
								
									console.log(data);
									setTimeout(	function()	{

										$('#escrow_email').val(data.setting.escrow_email);
										$('#escrow_password').val(data.setting.escrow_password);
										$('#escrow_partner_id').val(data.setting.escrow_partner_id);
										$('#escrow_partner_id_staging').val(data.setting.escrow_partner_id_staging);
										
										if( data.setting.escrow_mode == 1 )	{
											$('#escrow_mode_1').prop('checked', true);
											$('#escrow_mode_0').prop('checked', false);
										}
										else	{
											$('#escrow_mode_1').prop('checked', false);
											$('#escrow_mode_0').prop('checked', true);
										}

										$('.overlay').hide();
										
										$('#sidebar_menu li').removeAttr('class');
										$('#li_settings').attr('class','active');
										$('#li_payment_settings').attr('class','active');
										
									}, 1000);
							});
							return false;
						};
						
                  },
				  cache: false
              })
              .state('app.settings.app', {
                  url: '/app',
                  templateUrl: 'ng/settings_app',
                  cache: false,
                  controller: function($scope, $http, toaster, $stateParams)	{
                        $scope.processForm = function(type)	{
							if( $('#default_premium').is(':checked') == false &&  $('#premium_price').val() < 0 )	{
								alert('"Your Budget Tab Maximum Price" can\'t have negative value.');
								return false;
							}
							else if( $('#default_premium').is(':checked') == false &&  $('#premium_offer_price').val() < 0 )	{
								alert('"Default Premium Domain Offer Price" can\'t have negative value.');
								return false;
							}
							else if( $('#default_premium').is(':checked') == false &&  $('#premium_price').val() > 10000 )	{
								alert('"Your Budget Tab Maximum Price" value can\'t have more than 10000.');
								return false;
							}
							else if( $('#default_premium').is(':checked') == false &&  $('#premium_offer_price').val() > 10000 )	{
								alert('"Default Premium Domain Offer Price" can\'t have more than 10000.');
								return false;
							}
							else	{
								var form = $scope.form;
								$scope.form.action = 'update_settings';
								//console.log($scope);
								//console.log(form);
								//console.log(JSON.stringify(form));
								console.log( getFormData( $("#settings_app") ) );
								$('.overlay').show();
								$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: '/ng/json/update-settings',
									//data: form
									//data: JSON.stringify(form)
									data: getFormData( $("#settings_app") )
								}).success(function(data, status, headers, config){
									console.log(data);
									toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
									$('.overlay').hide();
								});
							}
							return false;
						};
						
						$scope.getAppSettings = function(userid)	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_settings').attr('class','active');
							$('#li_app_settings').attr('class','active');

							$('.overlay').show();
							$http({
								method: 'post',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								url: 'ng/json/update-settings/?action=get_app_settings',
								data: { userid : userid }
							}).success(function(data, status, headers, config)	{
								//$('.overlay').hide();
								
									console.log(data);
									setTimeout(	function()	{
										
										if( data.setting.default_premium == 'Y' )
											$('#default_premium').prop('checked', true);
										else
											$('#default_premium').prop('checked', false);
										
										$('#premium_price').val(data.setting.premium_price);
										
										if( data.setting.show_in_search == 'Y' )
											$('#show_in_store').prop('checked', true);
										else
											$('#show_in_store').prop('checked', false);
										
										$('#store').val(data.setting.store_name);
										$('#website').val(data.setting.store_website);
										
										$('.overlay').hide();
										
										$('#sidebar_menu li').removeAttr('class');
										$('#li_settings').attr('class','active');
										$('#li_app_settings').attr('class','active');
										
									}, 1000);
							});
							return false;
						};
						
                  },
				  cache: false
              })
              .state('app.acquisitions', {
                  url: 'acquisitions',
                  template: '<div ui-view ></div>'
              })
              .state('app.acquisitions.watchlist', {
                  url: '/watchlist',
                  templateUrl: 'ng/acquisitions_watchlist',
                  cache: false,
                  controller: function($scope, $http, toaster){
						$scope.setActiveMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_acquisitions').attr('class','active');
							$('#li_acquisitions_watchlist').attr('class','active');
							return false;
						};
                        $scope.processForm = function(type)	{	
							var form = $scope.form;
							$scope.form.action = 'add_to_watchlist';
							console.log($scope);
							console.log(form);
							$http({
									method: 'post',
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									url: '/ng/json/update-watchlist',
									data: JSON.stringify(form)
							}).success(function(data, status, headers, config){
										toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
							});
							return false;
                        };
                  }
              })
              .state('app.reports', {
                  url: 'reports',
                  template: '<div ui-view ></div>'
              })
              .state('app.reports.visitor', {
                  url: '/visitor',
                  templateUrl: 'ng/reports_visitor',
                  cache: false,
                  controller: function($scope, toaster){
                        $scope.$state.toaster = toaster;
						$scope.setActiveMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_reports').attr('class','active');
							return false;
						};
                  }
              })
              .state('app.table.footable', {
                  url: '/footable',
                  templateUrl: 'tpl/table_footable.html'
              })
              .state('app.table.grid', {
                  url: '/grid',
                  templateUrl: 'tpl/table_grid.html',
                  resolve: load(['ngGrid','js/controllers/grid.js'])
              })
              .state('app.table.uigrid', {
                  url: '/uigrid',
                  templateUrl: 'tpl/table_uigrid.html',
                  resolve: load(['ui.grid','js/controllers/uigrid.js'])
              })
              .state('app.table.editable', {
                  url: '/editable',
                  templateUrl: 'tpl/table_editable.html',
                  controller: 'XeditableCtrl',
                  resolve: load(['xeditable','js/controllers/xeditable.js'])
              })
              .state('app.table.smart', {
                  url: '/smart',
                  templateUrl: 'tpl/table_smart.html',
                  resolve: load(['smart-table','js/controllers/table.js'])
              })
              // form
              .state('app.form', {
                  url: 'form',
                  template: '<div ui-view class="fade-in"></div>',
                  resolve: load('js/controllers/form.js')
              })
              .state('app.form.components', {
                  url: '/components',
                  templateUrl: 'tpl/form_components.html',
                  resolve: load(['ngBootstrap','daterangepicker','js/controllers/form.components.js'])
              })
              .state('app.form.elements', {
                  url: '/elements',
                  templateUrl: 'tpl/form_elements.html'
              })
              .state('app.form.validation', {
                  url: '/validation',
                  templateUrl: 'tpl/form_validation.html'
              })
              .state('app.form.wizard', {
                  url: '/wizard',
                  templateUrl: 'tpl/form_wizard.html'
              })
              .state('app.form.fileupload', {
                  url: '/fileupload',
                  templateUrl: 'tpl/form_fileupload.html',
                  resolve: load(['angularFileUpload','js/controllers/file-upload.js'])
              })
              .state('app.form.imagecrop', {
                  url: '/imagecrop',
                  templateUrl: 'tpl/form_imagecrop.html',
                  resolve: load(['ngImgCrop','js/controllers/imgcrop.js'])
              })
              .state('app.form.select', {
                  url: '/select',
                  templateUrl: 'tpl/form_select.html',
                  controller: 'SelectCtrl',
                  resolve: load(['ui.select','js/controllers/select.js'])
              })
              .state('app.form.slider', {
                  url: '/slider',
                  templateUrl: 'tpl/form_slider.html',
                  controller: 'SliderCtrl',
                  resolve: load(['vr.directives.slider','js/controllers/slider.js'])
              })
              .state('app.form.editor', {
                  url: '/editor',
                  templateUrl: 'tpl/form_editor.html',
                  controller: 'EditorCtrl',
                  resolve: load(['textAngular','js/controllers/editor.js'])
              })
              .state('app.form.xeditable', {
                  url: '/xeditable',
                  templateUrl: 'tpl/form_xeditable.html',
                  controller: 'XeditableCtrl',
                  resolve: load(['xeditable','js/controllers/xeditable.js'])
              })
              // pages
              .state('app.page', {
                  url: 'page',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              .state('app.page.profile', {
                  url: '/profile',
                  templateUrl: 'ng/page_profile',
                  cache: false,
                  controller: function($scope, $http, toaster)	{
						$scope.setActiveMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_tools').attr('class','active');
							return false;
						};
                  }
              })
              .state('app.page.post', {
                  url: '/post',
                  templateUrl: 'tpl/page_post.html'
              })
              .state('app.page.search', {
                  url: '/search',
                  templateUrl: 'tpl/page_search.html'
              })
              .state('app.page.invoice', {
                  url: '/invoice',
                  templateUrl: 'tpl/page_invoice.html'
              })
              .state('app.page.price', {
                  url: '/price',
                  templateUrl: 'tpl/page_price.html'
              })
              .state('app.docs', {
                  url: '/docs',
                  templateUrl: 'tpl/docs.html'
              })
              // others
              .state('lockme', {
                  url: '/lockme',
                  templateUrl: 'tpl/page_lockme.html'
              })
              .state('access', {
                  url: '/access',
                  template: '<div ui-view class="fade-in-right-big smooth"></div>'
              })
              .state('access.signin', {
                  url: '/signin',
                  templateUrl: 'tpl/page_signin.html',
                  resolve: load( ['js/controllers/signin.js'] )
              })
              .state('access.signup', {
                  url: '/signup',
                  templateUrl: 'tpl/page_signup.html',
                  resolve: load( ['js/controllers/signup.js'] )
              })
              .state('access.forgotpwd', {
                  url: '/forgotpwd',
                  templateUrl: 'tpl/page_forgotpwd.html'
              })
              .state('access.404', {
                  url: '/404',
                  templateUrl: 'tpl/page_404.html'
              })

              // fullCalendar
              .state('app.calendar', {
                  url: '/calendar',
                  templateUrl: 'tpl/app_calendar.html',
                  // use resolve to load other dependences
                  resolve: load(['moment','fullcalendar','ui.calendar','js/app/calendar/calendar.js'])
              })

              // mail
              .state('app.mail', {
                  abstract: true,
                  url: '/mail',
                  templateUrl: 'tpl/mail.html',
                  // use resolve to load other dependences
                  resolve: load( ['js/app/mail/mail.js','js/app/mail/mail-service.js','moment'] )
              })
              .state('app.mail.list', {
                  url: '/inbox/{fold}',
                  templateUrl: 'tpl/mail.list.html'
              })
              .state('app.mail.detail', {
                  url: '/{mailId:[0-9]{1,4}}',
                  templateUrl: 'tpl/mail.detail.html'
              })
              .state('app.mail.compose', {
                  url: '/compose',
                  templateUrl: 'tpl/mail.new.html'
              })
              
              .state('layout', {
                  abstract: true,
                  url: '/layout',
                  templateUrl: 'tpl/layout.html'
              })
              .state('layout.fullwidth', {
                  url: '/fullwidth',
                  views: {
                      '': {
                          templateUrl: 'tpl/layout_fullwidth.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_fullwidth.html'
                      }
                  },
                  resolve: load( ['js/controllers/vectormap.js'] )
              })
              .state('layout.mobile', {
                  url: '/mobile',
                  views: {
                      '': {
                          templateUrl: 'tpl/layout_mobile.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_mobile.html'
                      }
                  }
              })
              .state('layout.app', {
                  url: '/app',
                  views: {
                      '': {
                          templateUrl: 'tpl/layout_app.html'
                      },
                      'footer': {
                          templateUrl: 'tpl/layout_footer_fullwidth.html'
                      }
                  },
                  resolve: load( ['js/controllers/tab.js'] )
              })
              .state('apps', {
                  abstract: true,
                  url: '/apps',
                  templateUrl: 'tpl/layout.html'
              })
              .state('apps.note', {
                  url: '/note',
                  templateUrl: 'tpl/apps_note.html',
                  resolve: load( ['js/app/note/note.js','moment'] )
              })
              .state('app.contact', {
                  url: 'contact',
                  templateUrl: 'tpl/table_static.html',
              })
              .state('app.weather', {
                  url: 'weather',
                  templateUrl: 'tpl/table_static.html',
              })
              .state('app.todo', {
                  url: '/todo',
                  templateUrl: 'tpl/apps_todo.html',
                  resolve: load(['js/app/todo/todo.js', 'moment'])
              })
              .state('app.todo.list', {
                  url: '/{fold}'
              })
              .state('app.note', {
                  url: '/note',
                  templateUrl: 'tpl/apps_note_material.html',
                  resolve: load(['js/app/note/note.js', 'moment'])
              })
              .state('music', {
                  url: '/music',
                  templateUrl: 'tpl/music.html',
                  controller: 'MusicCtrl',
                  resolve: load([
                            'com.2fdevs.videogular', 
                            'com.2fdevs.videogular.plugins.controls', 
                            'com.2fdevs.videogular.plugins.overlayplay',
                            'com.2fdevs.videogular.plugins.poster',
                            'com.2fdevs.videogular.plugins.buffering',
                            'js/app/music/ctrl.js', 
                            'js/app/music/theme.css'
                          ])
              })
                  .state('music.home', {
                      url: '/home',
                      templateUrl: 'tpl/music.home.html'
                  })
                  .state('music.genres', {
                      url: '/genres',
                      templateUrl: 'tpl/music.genres.html'
                  })
                  .state('music.detail', {
                      url: '/detail',
                      templateUrl: 'tpl/music.detail.html'
                  })
                  .state('music.mtv', {
                      url: '/mtv',
                      templateUrl: 'tpl/music.mtv.html'
                  })
                  .state('music.mtvdetail', {
                      url: '/mtvdetail',
                      templateUrl: 'tpl/music.mtv.detail.html'
                  })
                  .state('music.playlist', {
                      url: '/playlist/{fold}',
                      templateUrl: 'tpl/music.playlist.html'
                  })
              .state('app.material', {
                  url: '/material',
                  template: '<div ui-view class="wrapper-md"></div>',
                  resolve: load(['js/controllers/material.js'])
                })
                .state('app.material.button', {
                  url: '/button',
                  templateUrl: 'tpl/material/button.html'
                })
                .state('app.material.color', {
                  url: '/color',
                  templateUrl: 'tpl/material/color.html'
                })
                .state('app.material.icon', {
                  url: '/icon',
                  templateUrl: 'tpl/material/icon.html'
                })
                .state('app.material.card', {
                  url: '/card',
                  templateUrl: 'tpl/material/card.html'
                })
                .state('app.material.form', {
                  url: '/form',
                  templateUrl: 'tpl/material/form.html'
                })
                .state('app.material.list', {
                  url: '/list',
                  templateUrl: 'tpl/material/list.html'
                })
                .state('app.material.ngmaterial', {
                  url: '/ngmaterial',
                  templateUrl: 'tpl/material/ngmaterial.html'
                });

          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      promise = promise.then( function(){
                        if(JQ_CONFIG[src]){
                          return $ocLazyLoad.load(JQ_CONFIG[src]);
                        }
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            name = module.name;
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }


      }
    ]
  );