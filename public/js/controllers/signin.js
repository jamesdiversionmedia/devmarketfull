'use strict';

/* Controllers */
  // signin controller
//app.controller('SigninFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
//app.controller('SigninFormController', ['$scope', '$http', '$state', 'toaster', function($scope, $http, $state, toaster) {
app.controller('SigninFormController', ['$scope', '$http', '$state', 'toaster', 'blockUI', function($scope, $http, $state, toaster, blockUI) {
    $scope.user = {};
    $scope.authError = null;
    $scope.login = function() {
      $scope.authError = null;
      // Try to login
      //$http.post('api/login', {email: $scope.user.email, password: $scope.user.password})
	  blockUI.start();
	  toaster.pop('wait', 'Processing...', 'Pleas wait');
	  $http.post(base_url+'/ajax_login', {email: $scope.user.email, password: $scope.user.password})
      .then(function(response) {
			console.log(response.data);
			//if ( !response.data.user ) {
			/*
			if ( response.data.error == 500 ) {
			  $scope.authError = response.data.errormsg;
			  
			}else{
			  $state.go('app.dashboard-v1');
			}
			*/
			blockUI.stop();
			toaster.clear();
			if ( response.data.error == 500 ) {
				 toaster.pop('error', 'Error', response.data.errormsg);
			}
			else	{
				window.location.href = base_url;
			}
      }, function(x) {
			$scope.authError = 'Server Error';
      });
    };


	
  }])
;