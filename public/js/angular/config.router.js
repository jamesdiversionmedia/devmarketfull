'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {

          var layout = "ng/layout";
          if(window.location.href.indexOf("material") > 0){
            layout = "tpl/blocks/material.layout.html";
            $urlRouterProvider
              .otherwise('/app/dashboard-v3');
          }else{
            $urlRouterProvider
              .otherwise('/app/dashboard-v1');
          }
          $urlRouterProvider
              .otherwise('main');

			  
		  console.log($stateProvider);
		
          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/',
                  templateUrl: base_url+'/layout',
                  resolve: load([
								'toaster', 'js/angular/controllers/toaster.js',
							//	'/js/angular/app/block-ui/angular-block-ui.min.css',
							//	'/js/angular/app/block-ui/angular-block-ui.min.js', 	
							]),
				  controller: function($scope, $http, toaster, $stateParams)	{  
					  $scope.getLoginUser = function()	{
							$http({
								method: 'get',
								headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								url: base_url+'/ajax_get_login_user'
							}).success(function(data, status, headers, config){
								console.log(data);
								$scope.login_user = data.login_user;
							});
					  };
					  //$scope.getLoginUser();
					  
				  }
              })
              .state('app.main', {
                  url: 'main',
                  templateUrl: base_url+'/main',
                  cache: false,
				  resolve: load( ['js/angular/controllers/signin.js','js/angular/controllers/chart.js'] ),
				  controller: function($scope, $http, toaster, $stateParams)	{
					//$scope.setActiveMenu();
					$scope.setActiveMenu = function()	{
						$('#sidebar_menu li').removeAttr('class');
						$('#li_dashboard').attr('class','active');
						return false;
					};
				  }
              })
              .state('app.signout', {
                  url: 'signout',
                  templateUrl: base_url+'/signout',
				  resolve: load( ['js/angular/controllers/signin.js', 'toaster', 'js/angular/controllers/toaster.js'] ),
                  cache: false,
				  controller: function($scope, $http, toaster, $stateParams)	{
					  window.location.href = base_url;
				  }
              })
              .state('app.signin', {
                  url: 'signin',
                  templateUrl: base_url+'/signin',
                  resolve: load( ['js/angular/controllers/signin.js', 'toaster', 'js/angular/controllers/toaster.js'] )
              })
              .state('app.signinemail', {
                  url: 'signinemail',
                  templateUrl: base_url+'/signinemail',
                  resolve: load( ['js/angular/controllers/signinprocessemail.js', 'toaster', 'js/angular/controllers/toaster.js'] )
              })
              .state('app.signup', {
                  url: 'signup',
                  templateUrl: base_url+'/signup',
                  resolve: load( [
									'js/angular/controllers/signup.js',
									'toaster', 'js/angular/controllers/toaster.js',
							] )
              })
              .state('app.forgotpwd', {
				  url: 'forgotpwd',
                  templateUrl: base_url+'/forgotpwd',
              })
              .state('app.page404', {
				  url: 'page404',
                  templateUrl: base_url+'/page404',
              })
              .state('app.pendingdesignsnew', {
                  url: 'pendingdesignsnew',
                  templateUrl: base_url+'/pendingdesignsnew',
                  cache: false,
              })
              .state('app.ui.scroll', {
                  url: '/scroll',
                  templateUrl: 'tpl/ui_scroll.html',
                  resolve: load('js/angular/controllers/scroll.js')
              })
              .state('app.page.profile', {
                  url: '/profile',
                  templateUrl: '/static/angular/src/tpl/page_profile.html'
              })
              .state('app.myprofile', {
                  url: 'myprofile',
				  cache: false,
				  resolve: load([
								'toaster', 'js/angular/controllers/toaster.js',
								'angularFileUpload','js/controllers/file-upload.js',
					]),

					views: {
						'':{
							templateUrl: function(params)	{
								return base_url+'/myprofile';
							},
							resolve: load([
											'toaster', 'js/angular/controllers/toaster.js',
											'angularFileUpload','js/controllers/file-upload.js',
									]),
							cache: false,
							controller: function($scope, $http, FileUploader, toaster, blockUI, $stateParams)	{
								
								// --------------- start for upload profile pic ---------------
								var uploader = $scope.uploader = new FileUploader({
									url: base_url+'/ajax_upload_profile_pic',
									headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
									queueLimit: 1,
								});
								
								// FILTERS
								uploader.filters.push({
									name: 'customFilter',
									fn: function(item /*{File|FileLikeObject}*/, options) {
										return this.queue.length < 1;
										//return this.queue.length < 10;
									}
								});
								
								// CALLBACKS
								uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
									//console.info('onWhenAddingFileFailed', item, filter, options);
								};
								uploader.onAfterAddingFile = function(fileItem) {
									//console.info('onAfterAddingFile', fileItem);
								};
								uploader.onAfterAddingAll = function(addedFileItems) {
									//console.info('onAfterAddingAll', addedFileItems);
								};
								uploader.onBeforeUploadItem = function(item) {
									//console.info('onBeforeUploadItem', item);
									item.headers = { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') };
								};
								uploader.onProgressItem = function(fileItem, progress) {
									//console.info('onProgressItem', fileItem, progress);
									blockUI.start();
								};
								uploader.onProgressAll = function(progress) {
									//console.info('onProgressAll', progress);
								};
								uploader.onSuccessItem = function(fileItem, response, status, headers) {
									//console.info('onSuccessItem', fileItem, response, status, headers);
									blockUI.stop();
									toaster.pop(response.result.toLowerCase(), response.response_text, response.response_details);
									$scope.basicinfo = response.profile_data;
									
									$('#header_profile_pic_left').attr('src',response.profile_data.profile_pic);
									$('#header_profile_pic_left').attr('alt',response.profile_data.first_name + ' ' +response.profile_data.last_name);
									$('#header_profile_pic_left').attr('title',response.profile_data.first_name + ' ' +response.profile_data.last_name);
									
									$('#header_profile_pic_right').attr('src',response.profile_data.profile_pic);
									$('#header_profile_pic_right').attr('alt',response.profile_data.first_name + ' ' +response.profile_data.last_name);
									$('#header_profile_pic_right').attr('title',response.profile_data.first_name + ' ' +response.profile_data.last_name);
									$('#header_fullname_right').attr('alt',response.profile_data.first_name + ' ' +response.profile_data.last_name);
									$('#header_fullname_right').attr('alt',response.profile_data.first_name + ' ' +response.profile_data.last_name);
									
									blockUI.stop();
								};
								uploader.onErrorItem = function(fileItem, response, status, headers) {
									//console.info('onErrorItem', fileItem, response, status, headers);
								};
								uploader.onCancelItem = function(fileItem, response, status, headers) {
									//console.info('onCancelItem', fileItem, response, status, headers);
								};
								uploader.onCompleteItem = function(fileItem, response, status, headers) {
									//console.info('onCompleteItem', fileItem, response, status, headers);
									blockUI.stop();
									$scope.basicinfo = response.profile_data;
									//toaster.pop(response.result.toLowerCase(), response.response_text, response.response_details);
									blockUI.stop();
								};
								uploader.onCompleteAll = function() {
									//console.info('onCompleteAll');
								};
								console.info('uploader', uploader);
								
								$scope.clear = function () {
									angular.element("input[type='file']").val(null);
									uploader.clearQueue();
								};
								
								// --------------- end for upload profile pic ---------------

								$scope.settings = {};
								
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_profile').attr('class','active');
									
									$('[data-toggle=tooltip]').tooltip();
									return false;
								};
								
								$scope.basic_info_tab = function()	{
									$scope.setActiveMenu();
									$('#myprofile_nav li').removeAttr('class');
									$('#li_basic_info_tab').attr('class','active');
									
									$('#des_basic_info').show('slow');
									$('#des_settings').hide('slow');
									$('#des_other_info').hide('slow');
									$('#des_profilepic_info').hide('slow');
									
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_get_basic_info',
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.basicinfo = data;
										//console.log($scope.logos);
										//if( data.result_code != undefined )	{
											
											blockUI.stop();
										//}
										//else	{
											//toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											//window.location.reload();
										//}
										blockUI.stop();
									});
									return false;
								}
								
								$scope.settings_tab = function(id_designer)	{
									$scope.setActiveMenu();
									$('#myprofile_nav li').removeAttr('class');
									$('#li_settings_tab').attr('class','active');
									
									$('#des_basic_info').hide('slow');
									$('#des_settings').show('slow');
									$('#des_other_info').hide('slow');
									$('#des_profilepic_info').hide('slow');
									
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_get_settings_info',
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.settings = data.data;
										//console.log($scope.logos);
										if( data.result_code != undefined )	{
											//toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											blockUI.stop();
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											window.location.reload();
										}
										blockUI.stop();
									});
									return false;
								}
								
								$scope.other_info_tab = function()	{
									$scope.setActiveMenu();
									$('#myprofile_nav li').removeAttr('class');
									$('#li_other_info_tab').attr('class','active');
									
									$('#des_basic_info').hide('slow');
									$('#des_settings').hide('slow');
									$('#des_other_info').show('slow');
									$('#des_profilepic_info').hide('slow');
									return false;
								}
								
								$scope.profile_pic_tab = function()	{
									$scope.setActiveMenu();
									$('#myprofile_nav li').removeAttr('class');
									$('#li_profile_pic_tab').attr('class','active');
									
									$('#des_basic_info').hide('slow');
									$('#des_settings').hide('slow');
									$('#des_other_info').hide('slow');
									$('#des_profilepic_info').show('slow');
									return false;
								}
								
								$scope.saveSettingsInfo = function()	{
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'post',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_save_settings_info',
										data: getFormData( $("#frm_settings_info") )
									}).success(function(data, status, headers, config){
										console.log(data);
										//$scope.logos = data.items;
										//console.log($scope.logos);
										if( data.result_code != undefined )	{
											toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
											blockUI.stop();
										}
										else	{
											toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
											window.location.reload();
										}
										blockUI.stop();
									});
									return false;
								}
		
								
							}
						}
					}
			  })
              .state('app.requestlogodetails', {
					url: 'requestlogodetails/?&id_request=:idRequest&id_assign=:idAssign',
					cache: false,
					resolve: load([
									'toaster', 'js/angular/controllers/toaster.js',
									'angularFileUpload','js/angular/controllers/file-upload.js',
									'js/angular/controllers/lightbox.js',
							]),
					views: {
						'':{
							templateUrl: function(params)	{	
								console.log(params);
								return base_url+'/requestlogodetails?&id_request=' + params.idRequest +'&id_assign=' + params.idAssign;
							},
							cache: false,
							controller: function($scope, $http, Lightbox, FileUploader, toaster, blockUI, $stateParams)	{
								  $scope.toggleDiv = function(id)	{
									$('#'+id).toggle('slow');
								  }
								  
								  $scope.images = [];
								  $scope.openLightboxModal = function (orig_image, logo_image, index) {
									 $scope.images = [
										{
										  'url': orig_image ,
										  'thumbUrl': logo_image,
										},
									 ];
									Lightbox.openModal($scope.images, index);
								  };
								
								
								$scope.comment = '';
								$scope.domain = '';
								// --------------- start for upload messages files ---------------
								var uploader = $scope.uploader = new FileUploader({	
									url: base_url+'/ajax_upload_messages_files?&id_request=' + $('#txt_id_request').val() + '&id_assign=' + $('#txt_id_assign').val() + '&comment=' + $scope.comment + '&domain=' + $scope.domain,
									headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								});
								
								// FILTERS
								uploader.filters.push({
									name: 'customFilter',
									fn: function(item /*{File|FileLikeObject}*/, options) {
										return this.queue.length < 1;
									}
								});

								
								// CALLBACKS
								uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
									//console.info('onWhenAddingFileFailed', item, filter, options);
									$scope.comment = $('#txt_comment').val();
								};
								uploader.onAfterAddingFile = function(fileItem) {
									//console.info('onAfterAddingFile', fileItem);
									$scope.comment = $('#txt_comment').val();
								};
								uploader.onAfterAddingAll = function(addedFileItems) {
									//console.info('onAfterAddingAll', addedFileItems);
									$scope.comment = $('#txt_comment').val();
								};
								uploader.onBeforeUploadItem = function(item) {
									$scope.comment = $('#txt_comment').val();
									$scope.domain = $('#txt_domain').val();
									//alert($scope.comment);
									item.url = base_url+'/ajax_upload_messages_files?&id_request=' + $('#txt_id_request').val() + '&id_assign=' + $('#txt_id_assign').val() + '&comment=' + $scope.comment + '&domain=' + $scope.domain;
									item.headers = { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') };
									//console.info('onBeforeUploadItem', item);
								};
								uploader.onProgressItem = function(fileItem, progress) {
									//console.info('onProgressItem', fileItem, progress);
									blockUI.start();
								};
								uploader.onProgressAll = function(progress) {
									//console.info('onProgressAll', progress);
									blockUI.start();
								};
								uploader.onSuccessItem = function(fileItem, response, status, headers) {
									//console.info('onSuccessItem', fileItem, response, status, headers);
									toaster.pop(response.result.toLowerCase(), response.response_text, response.response_details);
									blockUI.stop();
									$scope.details = response.data;
									$scope.comments = response.items;
									$scope.login_user = response.login_user;
									//$scope.logoitems_rows = response.logoitems_rows;
									//$scope.logoitems = response.logoitems;

									if( response.result == 'SUCCESS' )	{
										$('#txt_comment').val('');
										$('#txt_comment').attr('style','border-color:none;');
									}
									$scope.clear();
									blockUI.stop();
								};
								uploader.onErrorItem = function(fileItem, response, status, headers) {
									toaster.pop('error', 'Error', 'There were error when uploading the file');
									//console.info('onErrorItem', fileItem, response, status, headers);
								};
								uploader.onCancelItem = function(fileItem, response, status, headers) {
									//console.info('onCancelItem', fileItem, response, status, headers);
								};
								uploader.onCompleteItem = function(fileItem, response, status, headers) {
									//console.info('onCompleteItem', fileItem, response, status, headers);
									//toaster.pop(response.result.toLowerCase(), response.response_text, response.response_details);
									blockUI.stop();
									$scope.details = response.data;
									$scope.comments = response.items;
									$scope.login_user = response.login_user;
									//$scope.logoitems_rows = response.logoitems_rows;
									//$scope.logoitems = response.logoitems;
									blockUI.stop();
								};
								uploader.onCompleteAll = function() {
									//console.info('onCompleteAll');
									blockUI.stop();
								};
								console.info('uploader', uploader);
								
								$scope.clear = function () {
									angular.element("input[type='file']").val(null);
									uploader.clearQueue();
								};
								
								// --------------- end for upload messages files ---------------
								
								$scope.attachFile =  function()	{
									angular.element("input[type='file']").val(null);
									uploader.clearQueue();
									$('#upload_section').toggle('slow');
								}
								
								$scope.downloadFile =  function(logo_filename)	{
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_downloadfile?file='+logo_filename
									}).success(function(data, status, headers, config){
										blockUI.stop();
										if( data.result_code != undefined )
											console.log(data);
										else
											window.location.reload();
										
										blockUI.stop();
									});
									return false;
								}
								
								$scope.req = {};
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_requests').attr('class','active');
									//$('#li_requests_pending').attr('class','active');
									return false;
								};
								
								$scope.getRequestLogoDetails = function(id)	{	
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_getrequestlogodetails/'+id
									}).success(function(data, status, headers, config){
										$scope.details = data.data;
										$scope.comments = data.items;
										$scope.login_user = data.login_user;
										$scope.logoitems_rows = data.logoitems_rows;
										$scope.logoitems = data.logoitems;

										//console.log($scope.details);
										blockUI.stop();
										if( data.result_code != undefined )
											console.log(data);
										else
											window.location.reload();
										
										blockUI.stop();
									});
									return false;
								};
								
								$scope.sendComment = function()	{	
									$scope.setActiveMenu();
									
									if( $.trim( $('#txt_comment').val() ) == '' )	{
										$('#txt_comment').attr('style','border-color:#d9534f;');
										$('#txt_comment').focus();
										alert('Please provide a valid message.');
									}
									else	{
										$('#txt_comment').attr('style','none;');
										blockUI.start();
										$http({
											method: 'post',
											//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
											data: getFormData( $("#frmSendComment") ), 
											url: base_url+'/ajax_send_comment'
										}).success(function(data, status, headers, config){
											
											console.log(data);
											
											$scope.details = data.data;
											$scope.comments = data.items;
											
											blockUI.stop();
											if( data.result_code != undefined )	{
												console.log(data);
												$('#txt_comment').val('');
											}
											else
												window.location.reload();
											
											blockUI.stop();
										});
									}
									return false;
								};
								
								$scope.acceptRequest = function(id_assign,id_request,domain)	{
									if(confirm('Just click/press OK to confirm that you really want to accept the request for domain "'+domain+'"'))	{
										$scope.setActiveMenu();
										blockUI.start();
										$http({
											method: 'post',
											//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
											data: { id_assign: id_assign, id_request: id_request },
											url: base_url+'/ajax_accept_request'
										}).success(function(data, status, headers, config){
											console.log(data);
											$scope.logos = data.items;
											console.log($scope.logos);
											
											blockUI.stop();											
											if( data.result_code != undefined )	{
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												$('#btn_accept').hide('slow');
												$('#btn_reject').hide('slow');
												
												if(data.result_code == 'No Access')	{
													setTimeout(function()	{
														location.href = base_url;
													}, 5000);
												}
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											blockUI.stop();
										});
										return false;
									}
								}
								
								$scope.rejectRequest = function(id_assign,id_request,domain)	{
									if(confirm('Are you really sure that you want to reject the request for domain "'+domain+'" ? '))	{
										$scope.setActiveMenu();
										blockUI.start();
										$http({
											method: 'post',
											//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
											data: { id_assign: id_assign, id_request: id_request },
											url: base_url+'/ajax_reject_request'
										}).success(function(data, status, headers, config){
											console.log(data);
											$scope.logos = data.items;
											console.log($scope.logos);
											
											blockUI.stop();											
											if( data.result_code != undefined )	{
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												$('#btn_accept').hide('slow');
												$('#btn_reject').hide('slow');
												
												if(data.result_code == 'No Access')	{
													setTimeout(function()	{
														location.href = base_url;
													}, 5000);
												}
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											blockUI.stop();
										});
										return false;
									}
								}
								
							}
						}
					}
              })
              .state('app.pendingrequests', {
					url: 'pendingrequests',
					cache: false,
					views: {
						'':{
							templateUrl: function(params)	{
								return base_url+'/pendingrequests';
							},
							cache: false,
							controller: function($scope, $http, toaster, blockUI, $stateParams)	{
								
								$scope.goToMainPage = function()	{
									window.location.href = base_url;
								}
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_requests').attr('class','active');
									$('#li_requests_pending').attr('class','active');
									return false;
								};
								
								$scope.totalItems = 0;
								$scope.currentPage = 1;
								$scope.maxSize = 5;
								$scope.maxPageNumber = 10;
								$scope.itemsPerPage = 100;
								$scope.pagination = {
									currentPage:  1
								};
								
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=10, itemsPerPage=100)	{
									$scope.pagination = {
										currentPage: currentPage
									};
									$scope.maxPageNumber = Math.ceil( totalRecords / itemsPerPage );
									$scope.totalItems = totalRecords;
									$scope.maxSize = maxSize;
									$scope.itemsPerPage = itemsPerPage;
									console.log('numPerPage - ' + $scope.itemsPerPage);
									console.log('maxSize - ' + $scope.maxSize);
									console.log('totalItems - ' + $scope.totalItems);
									//console.log('currentPage - ' + $scope.currentPage);
									console.log('currentPage - ' + $scope.pagination.currentPage);
									console.log('maxPageNumber - ' + $scope.maxPageNumber);
									if( $scope.maxSize > 1 )	{
										$('.pagination').attr('style','visibility:block;');
									}
									else	{
										$('.pagination').attr('style','visibility:hidden;');
									}
								};
								$scope.selectPage = function(pageNumber){
									$scope.getRequestDesigns('P',pageNumber);
								};
								
								//selector start row per page
								$scope.rowsPerPageOptions = [
																{ name: '10 Rows Per Page', value: 10 }, 
																{ name: '20 Rows Per Page', value: 20 }, 
																{ name: '30 Rows Per Page', value: 30 }
															];
								$scope.row_per_page = $scope.rowsPerPageOptions[0].value;
								//selector end row per page
								
								$scope.getRequestDesigns = function(reqstatus='P',page=1)	{	
									$scope.params_range = '';
									$scope.currentPage = page;
									if( parseInt($scope.currentPage) < 1 )	{
										$scope.currentPage = 1;
									}
									$scope.pagination = {
										currentPage:  page
									};
									
									var view_limit = $scope.row_per_page;
									var maxSize = 5;
									
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_getrequestdesigns/'+reqstatus+'?'+'&page='+page+'&limit='+view_limit
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.logos = data.items;
										$scope.count_rows = data.rows;;
										
										blockUI.stop();
										//setTimeout(function()	{
											if( data.result_code != undefined )	{
												if( data.items != undefined )	{
													if( data.items.length > 0 )	{
														$scope.params_range = '('+(data.disp_page)+' - '+(data.disp_limit)+') / '+(data.rows)+' records';
														$scope.maxPageNumber = Math.ceil( data.rows / view_limit );
														if($scope.maxPageNumber == $scope.currentPage)	{
															$scope.params_range = '('+(data.disp_page)+' - '+(data.rows)+') / '+(data.rows)+' records';
														}
														$scope.updatePagination(page, data.rows, maxSize, view_limit);
													}
													else	{
														$scope.params_range = 'No records found';
														$scope.updatePagination(1, 0);
													}
												}
												else	{
													$scope.params_range = 'No records found';
													$scope.updatePagination(1, 0);
												}
											}
											else	{
												window.location.reload();
											}
											blockUI.stop();
										//}, 1000);

									});
									return false;
								};
								
								$scope.acceptRequest = function(id_assign,id_request,domain)	{
									if(confirm('Just click/press OK to confirm that you really want to accept the request for domain "'+domain+'"'))	{
										$scope.setActiveMenu();
										blockUI.start();
										$http({
											method: 'post',
											//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
											data: { id_assign: id_assign, id_request: id_request },
											url: base_url+'/ajax_accept_request'
										}).success(function(data, status, headers, config){
											console.log(data);
											$scope.logos = data.items;
											console.log($scope.logos);
											
											blockUI.stop();											
											if( data.result_code != undefined )	{
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												if( data.items != undefined )	{
													if( data.items.length > 0 )
														$('#for_empty').attr('class','hidden');
													else
														$('#for_empty').removeAttr('class');
												}
												else
													$('#for_empty').removeAttr('class');
												
												if(data.result_code == 'No Access')	{
													setTimeout(function()	{
														location.href = base_url;
													}, 5000);
												}
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											blockUI.stop();
										});
										return false;
									}
								}
								
								$scope.rejectRequest = function(id_assign,id_request,domain)	{
									if(confirm('Are you really sure that you want to reject the request for domain "'+domain+'" ? '))	{
										$scope.setActiveMenu();
										blockUI.start();
										$http({
											method: 'post',
											//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
											headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
											data: { id_assign: id_assign, id_request: id_request },
											url: base_url+'/ajax_reject_request'
										}).success(function(data, status, headers, config){
											console.log(data);
											$scope.logos = data.items;
											console.log($scope.logos);
											
											blockUI.stop();											
											if( data.result_code != undefined )	{
												toaster.pop(data.result.toLowerCase(), data.response_text, data.response_details);
												if( data.items != undefined )	{
													if( data.items.length > 0 )
														$('#for_empty').attr('class','hidden');
													else
														$('#for_empty').removeAttr('class');
												}
												else
													$('#for_empty').removeAttr('class');
												
												if(data.result_code == 'No Access')	{
													setTimeout(function()	{
														location.href = base_url;
													}, 5000);
												}
											}
											else	{
												toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
												alert('Session expired.');
												window.location.reload();
											}
											blockUI.stop();
										});
										return false;
									}
								}
								
							}
						}
					}
			  })
			  
              .state('app.acceptedrequests', {
                  url: 'acceptedrequests',
				  cache: false,
					views: {
						'':{
							templateUrl: function(params)	{
								return base_url+'/acceptedrequests';
							},
							cache: false,
							controller: function($scope, $http, toaster, blockUI, $stateParams)	{
								$scope.goToMainPage = function()	{
									window.location.href = base_url;
								}
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_requests').attr('class','active');
									$('#li_requests_accepted').attr('class','active');
									return false;
								};
								
								$scope.totalItems = 0;
								$scope.currentPage = 1;
								$scope.maxSize = 5;
								$scope.maxPageNumber = 10;
								$scope.itemsPerPage = 100;
								$scope.pagination = {
									currentPage:  1
								};
								
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=10, itemsPerPage=100)	{
									$scope.pagination = {
										currentPage: currentPage
									};
									$scope.maxPageNumber = Math.ceil( totalRecords / itemsPerPage );
									$scope.totalItems = totalRecords;
									$scope.maxSize = maxSize;
									$scope.itemsPerPage = itemsPerPage;
									console.log('numPerPage - ' + $scope.itemsPerPage);
									console.log('maxSize - ' + $scope.maxSize);
									console.log('totalItems - ' + $scope.totalItems);
									//console.log('currentPage - ' + $scope.currentPage);
									console.log('currentPage - ' + $scope.pagination.currentPage);
									console.log('maxPageNumber - ' + $scope.maxPageNumber);
									if( $scope.maxSize > 1 )	{
										$('.pagination').attr('style','visibility:block;');
									}
									else	{
										$('.pagination').attr('style','visibility:hidden;');
									}
								};
								$scope.selectPage = function(pageNumber){
									$scope.getRequestDesigns('C',pageNumber);
								};
								
								//selector start row per page
								$scope.rowsPerPageOptions = [
																{ name: '10 Rows Per Page', value: 10 }, 
																{ name: '20 Rows Per Page', value: 20 }, 
																{ name: '30 Rows Per Page', value: 30 }
															];
								$scope.row_per_page = $scope.rowsPerPageOptions[0].value;
								//selector end row per page
								
								$scope.getRequestDesigns = function(reqstatus='C',page=1)	{	
									$scope.params_range = '';
									$scope.currentPage = page;
									if( parseInt($scope.currentPage) < 1 )	{
										$scope.currentPage = 1;
									}
									$scope.pagination = {
										currentPage:  page
									};
									
									var view_limit = $scope.row_per_page;
									var maxSize = 5;
							
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_getrequestdesigns/'+reqstatus+'?'+'&page='+page+'&limit='+view_limit
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.logos = data.items;
										$scope.count_rows = data.rows;
										
										blockUI.stop();
										//setTimeout(function()	{
											if( data.result_code != undefined )	{
												if( data.items != undefined )	{
													if( data.items.length > 0 )	{
														$scope.params_range = '('+(data.disp_page)+' - '+(data.disp_limit)+') / '+(data.rows)+' records';
														$scope.maxPageNumber = Math.ceil( data.rows / view_limit );
														if($scope.maxPageNumber == $scope.currentPage)	{
															$scope.params_range = '('+(data.disp_page)+' - '+(data.rows)+') / '+(data.rows)+' records';
														}
														$scope.updatePagination(page, data.rows, maxSize, view_limit);
													}
													else	{
														$scope.params_range = 'No records found';
														$scope.updatePagination(1, 0);
													}
												}
												else	{
													$scope.params_range = 'No records found';
													$scope.updatePagination(1, 0);
												}
											}
											else	{
												window.location.reload();
											}
											blockUI.stop();
										//}, 1000);

									});
									return false;
								};
								
								
							}
						}
					}
			  })
			  
              .state('app.completedrequests', {
                  url: 'completedrequests',
				  cache: false,
					views: {
						'':{
							templateUrl: function(params)	{
								return base_url+'/completedrequests';
							},
							cache: false,
							controller: function($scope, $http, toaster, blockUI, $stateParams)	{
								$scope.goToMainPage = function()	{
									window.location.href = base_url;
								}
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_requests').attr('class','active');
									$('#li_requests_completed').attr('class','active');
									return false;
								};
								
								$scope.totalItems = 0;
								$scope.currentPage = 1;
								$scope.maxSize = 5;
								$scope.maxPageNumber = 10;
								$scope.itemsPerPage = 100;
								$scope.pagination = {
									currentPage:  1
								};
								
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=10, itemsPerPage=100)	{
									$scope.pagination = {
										currentPage: currentPage
									};
									$scope.maxPageNumber = Math.ceil( totalRecords / itemsPerPage );
									$scope.totalItems = totalRecords;
									$scope.maxSize = maxSize;
									$scope.itemsPerPage = itemsPerPage;
									console.log('numPerPage - ' + $scope.itemsPerPage);
									console.log('maxSize - ' + $scope.maxSize);
									console.log('totalItems - ' + $scope.totalItems);
									//console.log('currentPage - ' + $scope.currentPage);
									console.log('currentPage - ' + $scope.pagination.currentPage);
									console.log('maxPageNumber - ' + $scope.maxPageNumber);
									if( $scope.maxSize > 1 )	{
										$('.pagination').attr('style','visibility:block;');
									}
									else	{
										$('.pagination').attr('style','visibility:hidden;');
									}
								};
								$scope.selectPage = function(pageNumber) {
									$scope.getRequestDesigns('F',pageNumber);
								};
								//selector start row per page
								$scope.rowsPerPageOptions = [
																{ name: '10 Rows Per Page', value: 10 }, 
																{ name: '20 Rows Per Page', value: 20 }, 
																{ name: '30 Rows Per Page', value: 30 }
															];
								$scope.row_per_page = $scope.rowsPerPageOptions[0].value;
								//selector end row per page
								
								$scope.getRequestDesigns = function(reqstatus='F',page=1)	{	
									$scope.params_range = '';
									$scope.currentPage = page;
									if( parseInt($scope.currentPage) < 1 )	{
										$scope.currentPage = 1;
									}
									$scope.pagination = {
										currentPage:  page
									};
									
									var view_limit = $scope.row_per_page;
									var maxSize = 5;
									
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_getrequestdesigns/'+reqstatus+'?'+'&page='+page+'&limit='+view_limit
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.logos = data.items;
										$scope.count_rows = data.rows;
										
										blockUI.stop();
										//setTimeout(function()	{
											if( data.result_code != undefined )	{
												if( data.items != undefined )	{
													if( data.items.length > 0 )	{
														$scope.params_range = '('+(data.disp_page)+' - '+(data.disp_limit)+') / '+(data.rows)+' records';
														$scope.maxPageNumber = Math.ceil( data.rows / view_limit );
														if($scope.maxPageNumber == $scope.currentPage)	{
															$scope.params_range = '('+(data.disp_page)+' - '+(data.rows)+') / '+(data.rows)+' records';
														}
														$scope.updatePagination(page, data.rows, maxSize, view_limit);
													}
													else	{
														$scope.params_range = 'No records found';
														$scope.updatePagination(1, 0);
													}
												}
												else	{
													$scope.params_range = 'No records found';
													$scope.updatePagination(1, 0);
												}
											}
											else	{
												window.location.reload();
											}
											blockUI.stop();
										//}, 1000);

									});
									return false;
								};
								
								
							}
						}
					}
			  })
			  
              .state('app.rejectedrequests', {
                  url: 'rejectedrequests',
				  cache: false,
					views: {
						'':{
							templateUrl: function(params)	{
								return base_url+'/rejectedrequests';
							},
							cache: false,
							controller: function($scope, $http, toaster, blockUI, $stateParams)	{
								$scope.goToMainPage = function()	{
									window.location.href = base_url;
								}
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_requests').attr('class','active');
									$('#li_requests_rejected').attr('class','active');
									return false;
								};
								
								$scope.totalItems = 0;
								$scope.currentPage = 1;
								$scope.maxSize = 5;
								$scope.maxPageNumber = 10;
								$scope.itemsPerPage = 100;
								$scope.pagination = {
									currentPage:  1
								};
								
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=10, itemsPerPage=100)	{
									$scope.pagination = {
										currentPage: currentPage
									};
									$scope.maxPageNumber = Math.ceil( totalRecords / itemsPerPage );
									$scope.totalItems = totalRecords;
									$scope.maxSize = maxSize;
									$scope.itemsPerPage = itemsPerPage;
									console.log('numPerPage - ' + $scope.itemsPerPage);
									console.log('maxSize - ' + $scope.maxSize);
									console.log('totalItems - ' + $scope.totalItems);
									//console.log('currentPage - ' + $scope.currentPage);
									console.log('currentPage - ' + $scope.pagination.currentPage);
									console.log('maxPageNumber - ' + $scope.maxPageNumber);
									if( $scope.maxSize > 1 )	{
										$('.pagination').attr('style','visibility:block;');
									}
									else	{
										$('.pagination').attr('style','visibility:hidden;');
									}
								};
								$scope.selectPage = function(pageNumber){
									$scope.getRequestDesigns('R',pageNumber);
								};
								
								//selector start row per page
								$scope.rowsPerPageOptions = [
																{ name: '10 Rows Per Page', value: 10 }, 
																{ name: '20 Rows Per Page', value: 20 }, 
																{ name: '30 Rows Per Page', value: 30 }
															];
								$scope.row_per_page = $scope.rowsPerPageOptions[0].value;
								//selector end row per page
								
								$scope.getRequestDesigns = function(reqstatus='R',page=1)	{	
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_getrequestdesigns/'+reqstatus+'?'+'&page='+page+'&limit='+view_limit
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.logos = data.items;
										$scope.count_rows = data.rows;
										
										blockUI.stop();
										//setTimeout(function()	{
											if( data.result_code != undefined )	{
												if( data.items != undefined )	{
													if( data.items.length > 0 )	{
														$scope.params_range = '('+(data.disp_page)+' - '+(data.disp_limit)+') / '+(data.rows)+' records';
														$scope.maxPageNumber = Math.ceil( data.rows / view_limit );
														if($scope.maxPageNumber == $scope.currentPage)	{
															$scope.params_range = '('+(data.disp_page)+' - '+(data.rows)+') / '+(data.rows)+' records';
														}
														$scope.updatePagination(page, data.rows, maxSize, view_limit);
													}
													else	{
														$scope.params_range = 'No records found';
														$scope.updatePagination(1, 0);
													}
												}
												else	{
													$scope.params_range = 'No records found';
													$scope.updatePagination(1, 0);
												}
											}
											else	{
												window.location.reload();
											}
											blockUI.stop();
										//}, 1000);

									});
									return false;
								};
								
								
							}
						}
					}
			  })
			  
              .state('app.expiredrequests', {
                  url: 'expiredrequests',
				  cache: false,
					views: {
						'':{
							templateUrl: function(params)	{
								return base_url+'/expiredrequests';
							},
							cache: false,
							controller: function($scope, $http, toaster, blockUI, $stateParams)	{
								$scope.goToMainPage = function()	{
									window.location.href = base_url;
								}
								$scope.setActiveMenu = function()	{
									$('#sidebar_menu li').removeAttr('class');
									$('#li_line').attr('class','line dk hidden-folded');
									$('#li_line_main').attr('class','line dk hidden-folded');
									$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
									$('#li_requests').attr('class','active');
									$('#li_requests_expired').attr('class','active');
									return false;
								};
								
								$scope.totalItems = 0;
								$scope.currentPage = 1;
								$scope.maxSize = 5;
								$scope.maxPageNumber = 10;
								$scope.itemsPerPage = 100;
								$scope.pagination = {
									currentPage:  1
								};
								
								$scope.updatePagination = function(currentPage, totalRecords, maxSize=10, itemsPerPage=100)	{
									$scope.pagination = {
										currentPage: currentPage
									};
									$scope.maxPageNumber = Math.ceil( totalRecords / itemsPerPage );
									$scope.totalItems = totalRecords;
									$scope.maxSize = maxSize;
									$scope.itemsPerPage = itemsPerPage;
									console.log('numPerPage - ' + $scope.itemsPerPage);
									console.log('maxSize - ' + $scope.maxSize);
									console.log('totalItems - ' + $scope.totalItems);
									//console.log('currentPage - ' + $scope.currentPage);
									console.log('currentPage - ' + $scope.pagination.currentPage);
									console.log('maxPageNumber - ' + $scope.maxPageNumber);
									if( $scope.maxSize > 1 )	{
										$('.pagination').attr('style','visibility:block;');
									}
									else	{
										$('.pagination').attr('style','visibility:hidden;');
									}
								};
								$scope.selectPage = function(pageNumber){
									$scope.getRequestDesigns('E',pageNumber);
								};
								
								//selector start row per page
								$scope.rowsPerPageOptions = [
																{ name: '10 Rows Per Page', value: 10 }, 
																{ name: '20 Rows Per Page', value: 20 }, 
																{ name: '30 Rows Per Page', value: 30 }
															];
								$scope.row_per_page = $scope.rowsPerPageOptions[0].value;
								//selector end row per page
								
								$scope.getRequestDesigns = function(reqstatus='E',page=1)	{	
									$scope.params_range = '';
									$scope.currentPage = page;
									if( parseInt($scope.currentPage) < 1 )	{
										$scope.currentPage = 1;
									}
									$scope.pagination = {
										currentPage:  page
									};
									
									var view_limit = $scope.row_per_page;
									var maxSize = 5;
									
									$scope.setActiveMenu();
									blockUI.start();
									$http({
										method: 'get',
										//headers: {'Content-Type': 'application/x-www-form-urlencoded'},
										headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
										url: base_url+'/ajax_getrequestdesigns/'+reqstatus
									}).success(function(data, status, headers, config){
										console.log(data);
										$scope.logos = data.items;
										$scope.count_rows = data.rows;
										
										blockUI.stop();
										//setTimeout(function()	{
											if( data.result_code != undefined )	{
												if( data.items != undefined )	{
													if( data.items.length > 0 )	{
														$scope.params_range = '('+(data.disp_page)+' - '+(data.disp_limit)+') / '+(data.rows)+' records';
														$scope.maxPageNumber = Math.ceil( data.rows / view_limit );
														if($scope.maxPageNumber == $scope.currentPage)	{
															$scope.params_range = '('+(data.disp_page)+' - '+(data.rows)+') / '+(data.rows)+' records';
														}
														$scope.updatePagination(page, data.rows, maxSize, view_limit);
													}
													else	{
														$scope.params_range = 'No records found';
														$scope.updatePagination(1, 0);
													}
												}
												else	{
													$scope.params_range = 'No records found';
													$scope.updatePagination(1, 0);
												}
											}
											else	{
												window.location.reload();
											}
											blockUI.stop();
										//}, 1000);

									});
									return false;
								};
								
								
							}
						}
					}
			  })
			  
              .state('app.sales', {
                  url: 'sales?:id_sale:type',
                  templateUrl: 'ng/table_sales',
                  cache: false,
				  controller: function($scope, $http, toaster, $stateParams)	{
				  }
			  })
              .state('app.completeddesigns', {
                  url: 'completeddesigns',
                  templateUrl: base_url+'/completeddesigns',
                  cache: false,
              })
              .state('app.uploadedlogo', {
                  url: 'uploadedlogo',
                  templateUrl: base_url+'/uploadedlogo',
                  cache: false,
					resolve: load([
						'toaster', 'js/angular/controllers/toaster.js',
						'js/angular/app/angular-isotope/dist/angular-isotope.js',
						'js/angular/controllers/lightbox.js',
						'angularFileUpload','js/angular/controllers/file-upload.js',
						'js/angular/app/angular-isotope/dist/jquery.main.js',
					]),
				  controller: function($scope, $http, Lightbox, FileUploader, toaster, blockUI, $stateParams)	{
								 $scope.images = [];
								 $scope.openLightboxModal = function (orig_image, logo_image, index) {
									 $scope.images = [
										{
										  'url': orig_image ,
										  'thumbUrl': logo_image,
										},
									 ];
									Lightbox.openModal($scope.images, index);
								};
						
								$scope.all_valid = {};
								$scope.all_invalid = {};
								$scope.count_valid = 0;
								$scope.count_invalid = 0;
								// --------------- start for upload messages files ---------------
								var uploader = $scope.uploader = new FileUploader({	
									url: base_url+'/ajax_upload_logos',
									headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								});
								
								// FILTERS
								uploader.filters.push({
									name: 'customFilter',
									fn: function(item /*{File|FileLikeObject}*/, options) {
										return this.queue.length < 1;
									}
								});

								
								// CALLBACKS
								uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
									//console.info('onWhenAddingFileFailed', item, filter, options);
									//$scope.comment = $('#txt_comment').val();
								};
								uploader.onAfterAddingFile = function(fileItem) {
									//console.info('onAfterAddingFile', fileItem);
									//$scope.comment = $('#txt_comment').val();
								};
								uploader.onAfterAddingAll = function(addedFileItems) {
									//console.info('onAfterAddingAll', addedFileItems);
									//$scope.comment = $('#txt_comment').val();
								};
								uploader.onBeforeUploadItem = function(item) {
									//$scope.comment = $('#txt_comment').val();
									//$scope.domain = $('#txt_domain').val();
									//alert($scope.comment);
									item.url = base_url+'/ajax_upload_logos';
									item.headers = { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') };
									//console.info('onBeforeUploadItem', item);
								};
								uploader.onProgressItem = function(fileItem, progress) {
									//console.info('onProgressItem', fileItem, progress);
									blockUI.start();
								};
								uploader.onProgressAll = function(progress) {
									//console.info('onProgressAll', progress);
									blockUI.start();
								};
								uploader.onSuccessItem = function(fileItem, response, status, headers) {
									//console.info('onSuccessItem', fileItem, response, status, headers);
									//toaster.pop(response.result.toLowerCase(), response.response_text, response.response_details);
									console.log(response);
									blockUI.stop();
									//$scope.details = response.data;
									//$scope.comments = response.items;
									$scope.all_images = response.logo_images;
									$scope.all_images_orig = response.logo_images_orig;
									$scope.all_valid = response.all_valid;
									$scope.all_invalid = response.all_invalid;
									$scope.count_valid = response.count_valid;
									$scope.count_invalid = response.count_invalid;
									$scope.logos = response.items;
									$scope.count_rows = response.rows;
									$scope.clear();
									blockUI.stop();
								};
								uploader.onErrorItem = function(fileItem, response, status, headers) {
									toaster.pop('error', 'Error', 'There were error when uploading the file');
									//console.info('onErrorItem', fileItem, response, status, headers);
								};
								uploader.onCancelItem = function(fileItem, response, status, headers) {
									//console.info('onCancelItem', fileItem, response, status, headers);
								};
								uploader.onCompleteItem = function(fileItem, response, status, headers) {
									//console.info('onCompleteItem', fileItem, response, status, headers);
									//toaster.pop(response.result.toLowerCase(), response.response_text, response.response_details);
									blockUI.stop();
									//$scope.details = response.data;
									//$scope.comments = response.items;
									$scope.all_images = response.logo_images;
									$scope.all_images_orig = response.logo_images_orig;
									$scope.all_valid = response.all_valid;
									$scope.all_invalid = response.all_invalid;
									$scope.count_valid = response.count_valid;
									$scope.count_invalid = response.count_invalid;
									$scope.logos = response.items;
									$scope.count_rows = response.rows;
									blockUI.stop();
								};
								uploader.onCompleteAll = function() {
									//console.info('onCompleteAll');
									blockUI.stop();
								};
								console.info('uploader', uploader);
								
								$scope.clear = function () {
									angular.element("input[type='file']").val(null);
									uploader.clearQueue();
								};
								
								// --------------- end for upload messages files ---------------
						
						$scope.attachFile =  function()	{
							angular.element("input[type='file']").val(null);
							uploader.clearQueue();
							$('#upload_section').toggle('slow');
						}
						$scope.setActiveMenu = function()	{
							$('#sidebar_menu li').removeAttr('class');
							$('#li_line').attr('class','line dk hidden-folded');
							$('#li_line_main').attr('class','line dk hidden-folded');
							$('#li_legend').attr('class','hidden-folded padder m-t m-b-sm text-muted text-xs');
							$('#li_logos').attr('class','active');
							$('#li_logos_pending').attr('class','active');
							return false;
						};
						
						$scope.totalItems = 0;
						$scope.currentPage = 1;
						$scope.maxSize = 5;
						$scope.maxPageNumber = 10;
						$scope.itemsPerPage = 100;
						$scope.pagination = {
							currentPage:  1
						};
						
						$scope.updatePagination = function(currentPage, totalRecords, maxSize=10, itemsPerPage=100)	{
							$scope.pagination = {
								currentPage: currentPage
							};
							$scope.maxPageNumber = Math.ceil( totalRecords / itemsPerPage );
							$scope.totalItems = totalRecords;
							$scope.maxSize = maxSize;
							$scope.itemsPerPage = itemsPerPage;
							console.log('numPerPage - ' + $scope.itemsPerPage);
							console.log('maxSize - ' + $scope.maxSize);
							console.log('totalItems - ' + $scope.totalItems);
							//console.log('currentPage - ' + $scope.currentPage);
							console.log('currentPage - ' + $scope.pagination.currentPage);
							console.log('maxPageNumber - ' + $scope.maxPageNumber);
							if( $scope.maxSize > 1 )	{
								$('.pagination').attr('style','visibility:block;');
							}
							else	{
								$('.pagination').attr('style','visibility:hidden;');
							}
						};
						$scope.selectPage = function(pageNumber){
								$scope.getUploadedLogos(pageNumber);
						};

						
						$scope.selected_filterseller = $('#id_sellr').val();
						// start selector for sort_field
						$scope.sortFieldSelector = [
													{ name: 'Date Uploaded', value: 'Date Uploaded' }, 
													{ name: 'Domain', value: 'domain' }, 
												];
						$scope.sort_field_selector = $scope.sortFieldSelector[0].value;
						// end selector for sort_field
						
						// start selector for sort_by
						$scope.sortBySelector = [
													{ name: 'ASC', value: 'asc' }, 
													{ name: 'DESC', value: 'desc' }, 
												];
						$scope.sort_by_selector = $scope.sortBySelector[0].value;
						// end selector for sort_field
						
						//selector start row per page
						$scope.rowsPerPageOptions = [
														{ name: '16 Rows Per Page', value: 16 }, 
														{ name: '24 Rows Per Page', value: 24 }, 
														{ name: '40 Rows Per Page', value: 40 }
													];
						$scope.row_per_page = $scope.rowsPerPageOptions[0].value;
						//selector end row per page
						
						$scope.getUploadedLogos = function(page)	{ 
							$scope.params_range = '';
							$scope.currentPage = page;
							if( parseInt($scope.currentPage) < 1 )	{
								$scope.currentPage = 1;
							}
							$scope.pagination = {
								currentPage:  page
							};
							
							var sort_field = $scope.sort_field_selector;
							var sort_by = $scope.sort_by_selector;
							var filter_by_id_seller = $scope.selected_filterseller;
							var txt_search = $('#search_text').val();
							var view_limit = $scope.row_per_page;
							//var view_limit = $('#view_limit').val();
							var maxSize = 5;
							
							$scope.setActiveMenu();
							blockUI.start();
							$http({
								method: 'get',
								headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								url: base_url+'/ajax_get_uploaded_logos?'+'&page='+page+'&limit='+view_limit
							}).success(function(data, status, headers, config){
								console.log(data);
								$scope.logos = data.items;
								$scope.count_rows = data.rows;
								
								blockUI.stop();
								//setTimeout(function()	{
									if( data.result_code != undefined )	{
										//console.log(data);
										if( data.items != undefined )	{
											if( data.items.length > 0 )	{
												$('#for_empty').attr('class','hidden');
												$scope.params_range = '('+(data.disp_page)+' - '+(data.disp_limit)+') / '+(data.rows)+' records';
												$scope.maxPageNumber = Math.ceil( data.rows / view_limit );
												if($scope.maxPageNumber == $scope.currentPage)	{
													$scope.params_range = '('+(data.disp_page)+' - '+(data.rows)+') / '+(data.rows)+' records';
												}
												$scope.updatePagination(page, data.rows, maxSize, view_limit);
											}
											else	{
												$scope.params_range = 'No records found';
												$scope.updatePagination(1, 0);
											}
											//initSameHeight();
										}
										else	{
											$('#for_empty').removeAttr('class');
											//$state.params.range = 'No records found';
											$scope.updatePagination(1, 0);
										}
									}
									else	{
										//toaster.pop('error', 'SESSION EXPIRE', 'Session already expire.');
										//alert('Session expired.');
										//window.location.reload();
									}
									blockUI.stop();
								//}, 1000);

							});
							return false;
						};
				  },
              })
              .state('app.submitlogo', {
                  url: 'submitlogo',
                  templateUrl: base_url+'/submitlogo',
                  cache: false,
              })
              .state('app.ui', {
                  url: '/ui',
                  template: '<div ui-view class="fade-in-up"></div>'
              })
              .state('app.ui.buttons', {
                  url: '/buttons',
                  templateUrl: 'tpl/ui_buttons.html'
              })

          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      promise = promise.then( function(){
                        if(JQ_CONFIG[src]){
                          return $ocLazyLoad.load(JQ_CONFIG[src]);
                        }
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            name = module.name;
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }


      }
    ]
  );