'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$http', '$state', 'toaster', 'blockUI', function($scope, $http, $state, toaster, blockUI) {
    $scope.user = {};
    $scope.authError = null;

    $scope.displayMessage = function() {
		  toaster.pop('error', 'Error', 'Can\'t confirm your email. Invalid key.');
    };
	
    $scope.login = function() {
		  $scope.authError = null;
		  blockUI.start();
		  toaster.pop('wait', 'Processing...', 'Pleas wait');
		  $http.post(base_url+'/ajax_login', {email: $scope.user.email, password: $scope.user.password})
		  .then(function(response) {
				console.log(response.data);

				blockUI.stop();
				toaster.clear();
				if ( response.data.error == 500 ) {
					 toaster.pop('error', 'Error', response.data.errormsg);
				}
				else	{
					window.location.href = base_url;
				}
		  }, function(x) {
				$scope.authError = 'Server Error';
		  });
    };


	
  }])
;