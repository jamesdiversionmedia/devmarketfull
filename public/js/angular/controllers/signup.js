'use strict';

// signup controller
//app.controller('SignupFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
app.controller('SignupFormController', ['$scope', '$http', '$state', 'toaster', 'blockUI', function($scope, $http, $state, toaster, blockUI) {
    $scope.user = {};
    $scope.authError = null;
    $scope.signup = function() {
      $scope.authError = null;
      // Try to create
      //$http.post('api/signup', {name: $scope.user.name, email: $scope.user.email, password: $scope.user.password})
	  blockUI.start();
	  toaster.pop('wait', 'Processing...', 'Pleas wait');
	  $http.post(base_url+'/ajax_signup', {firstname: $scope.user.fname, lastname: $scope.user.lname, email: $scope.user.email, password: $scope.user.password})
      .then(function(response) {
		    //console.log(response);
			//console.log(response.data);
			/*
			if ( !response.data.user ) {
			  $scope.authError = response;
			}else{
			  $state.go('app.dashboard-v1');
			}
			*/
			
			blockUI.stop();
			toaster.clear();
			toaster.pop(response.data.status.toLowerCase(), response.data.status, response.data.message);
			
      }, function(x) {
        $scope.authError = 'Server Error';
      });
    };
  }])
 ;