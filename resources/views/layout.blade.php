<?php if( !isset($session['id_user']) && !isset($session['friendly']) )	{	?>

	  <!-- content -->
	  <div>
		<div ui-butterbar></div>
		<a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
		<div class="fade-in-up" ui-view></div>
	  </div>
	  <!-- /content -->
	
<?php	}	else	{	?>

		@include('navheader')
		@include('navsidebar')
	  <!-- content -->
	  <div id="content" class="app-content" role="main">
		<div ui-butterbar></div>
		<a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
		<div class="app-content-body fade-in-up" ui-view></div>
		
	  </div>
	  <!-- /content -->

	
	  
	  <!-- footer -->
	  <footer id="footer" class="app-footer" role="footer">
		<div class="wrapper b-t bg-light">
		  <span class="pull-right">2.2.0 <a href ui-scroll-to="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
		  &copy; <?php echo date("Y"); ?> Copyright.
		</div>
	  </footer>
	  <!-- / footer -->
	  
	
	  
		<toaster-container toaster-options="{
		  'closeButton': false,
		  'debug': false,
		  'position-class': 'toast-top-right',
		  'onclick': null,
		  'showDuration': '200',
		  'hideDuration': '1000',
		  'timeOut': '5000',
		  'extendedTimeOut': '1000',
		  'showEasing': 'swing',
		  'hideEasing': 'linear',
		  'showMethod': 'fadeIn',
		  'hideMethod': 'fadeOut'
		}"></toaster-container>
		
		
<?php }	?>
