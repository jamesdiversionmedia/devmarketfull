<div class="container w-xl w-auto-xs" ng-init="app.settings.container = false;">
  <img class="img-responsive" src="<?php echo config('app.BASE_URL'); ?>/img/zd-transparent-white.png">
  <!-- <a href class="navbar-brand block m-t"> {{ config('app.name') }} </a> -->
  <div class="m-b-lg">
    <div class="wrapper text-center">
	  <h4>Forgot Password?</h4>
      <p>Input your email to reset your password</p>
    </div>
    <form name="reset" ng-init="isCollapsed=true">
      <div class="list-group list-group-sm">
        <div class="list-group-item">
          <input type="email" placeholder="Email" ng-model="email" class="form-control no-border" required>
        </div>
      </div>
      <button type="submit" ng-disabled="reset.$invalid" class="btn btn-lg btn-primary btn-block"  ng-click="isCollapsed = !isCollapsed" > <i class="glyphicon glyphicon-send"></i> Send</button>
    </form>
	<!--
    <div collapse="isCollapsed" class="m-t">
      <div class="alert alert-success">
        <p>A reset link sent to your email address, please check it in 7 days. <a ui-sref="access.signin" class="btn btn-sm btn-success">Sign in</a></p>
      </div>
    </div>
	-->

	  <div class="line line-dashed"></div>
	  <p class="text-center"><small>Do not have an account?</small></p>
	  <a ui-sref="app.signup" href="#/signup" class="btn btn-lg btn-default btn-block"> <i class="glyphicon glyphicon-registration-mark"></i> Create an account</a>
	  
	  <div class="line line-dashed"></div>
	  <p class="text-center"><small>Already have an account?</small></p>
	  <a ui-sref="app.signin" href="#/signin" class="btn btn-lg btn-default btn-block"> <i class="glyphicon glyphicon-log-in"> </i> Sign in</a>
	  
	
  </div>
  <div class="clearfix"></div>
  <div class="text-center">
	<p><small class="text-muted">ZenDomains by <a href="http://diversionmedia.com/" target="_blank">Diversion Media<br>&copy; <?php echo date("Y"); ?></small></p>
  </div>
</div>