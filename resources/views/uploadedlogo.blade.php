
<div class="wrapper-md">

	   <div class="tab-container">
			<a style="min-width:90px;" title="All" ui-sref="app.uploadedlogos({tab:'All'})" class="btn m-b-xs btn-default btn-rounded domain-tabs All_tab"> All <span><small class="text-muted" id="alllogos_cnt"> </small></span> </a>
			<a style="min-width:90px;" title="Accepted" ui-sref="app.uploadedlogos({tab:'Pending'})" class="btn m-b-xs btn-default btn-rounded domain-tabs Pending_tab">Pending <span><small class="text-muted" id="pendinglogos_cnt"> </small></span></a>
			<a style="min-width:90px;" title="Accepted" ui-sref="app.uploadedlogos({tab:'Accepted'})" class="btn m-b-xs btn-default btn-rounded domain-tabs Accepted_tab">Accepted <span><small class="text-muted" id="acceptedlogos_cnt"> </small></span></a>
			<a style="min-width:90px;" title="Rejected" ui-sref="app.uploadedlogos({tab:'Rejected'})" class="btn m-b-xs btn-default btn-rounded domain-tabs Rejected_tab">Rejected <span><small class="text-muted" id="rejectedlogos_cnt"> </small></span></a>
			
			<a id="btnRefresh" class="pull-right btn m-b-xs w-xs btn-default btn-rounded domain-tabs" ng-click="getUploadedLogos(currentPage);" type="button" style=""><i class="glyphicon glyphicon-refresh"></i></a>	
	  </div>
	 
		
		<!-- <div class="bg-light lter b-b wrapper-md" style="padding-bottom:50px;"> -->
		<div class="bg-light lter b-b wrapper-md1 panel panel-heading">
		  <!-- <div class="m-n font-thin h3">
			Uploaded Logos
			-->
			 <div class="pull-left">
				<div class="pull-left">
					<button onclick="$('#more_filters').toggle('slow');cursor:pointer;" class="btn btn-default pull-left1"> <i class="glyphicon glyphicon-plus-sign"></i></button>
				</div>
				<div class="pull-left">
					<uib-pagination rotate="false" boundary-links="true" items-per-page="itemsPerPage" max-size="maxSize" total-items="totalItems" ng-model="pagination.currentPage" ng-change="selectPage(pagination.currentPage)" class="m-t-none m-b pull-left1" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
				</div>
				<div class="pull-left">
					<small class="text-muted inline m-t-sm m-b-sm pull-left1" style="margin-left:10px;">@{{ params_range }}</small>
				</div>
			</div>
			<div class="pull-right">
				<button ng-click="attachFile();" class="btn btn-default" type="button"> <i class="glyphicon glyphicon-upload"></i> Submit Logos </button>
			</div>	
			  <div class="form-group pull-right col-md-3 ">
				<div class="input-group">
					<input class="form-control" type="text" placeholder="Enter Domain" id="search_text" name="search_text" ng-keypress="searchLogoDomainEnter($event)" >
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" ng-click="getUploadedLogos(currentPage);"> <i class="glyphicon glyphicon-search"></i> Search</button>
					</span>
				</div>
			  </div>
			
			 <div class="clearfix"></div>
			 <div class="left" id="more_filters" style="display:none;">
				 <div class="container-fluid1 panel1 panel-default1">
					 <div class="pull-left">
						<span onclick="$('#more_filters').hide('slow');cursor:pointer;" class="btn btn-sm btn-default"> <i class="glyphicon glyphicon-minus-sign"></i></span>
					 </div>
					 <div class="pull-left">
						<select class="input-sm form-control w-sm inline v-middle" id="view_limit" name="view_limit" ng-model="row_per_page" required ng-options='option.value as option.name for option in rowsPerPageOptions' ng-change="getUploadedLogos(1)"></select>
						
						<select class="input-sm form-control w-sm inline v-middle" id="sort_field" name="sort_field" ng-model="sort_field_selector" required ng-options='option.value as option.name for option in sortFieldSelector' ng-change="getUploadedLogos(currentPage);"></select>
						
						<select class="input-sm form-control w-sm inline v-middle" id="sort_by" name="sort_by" ng-model="sort_by_selector" required ng-options='option.value as option.name for option in sortBySelector' ng-change="getUploadedLogos(currentPage);"></select>
						
						<select class="input-sm form-control w-sm inline v-middle pull-left1" id="id_sellr" name="id_sellr">
							<option value="0" selected="selected"> ---Filter By Seller --- </option>
						</select>
						
					 </div> 
				</div>
				<br><br>
			</div>
		</div>
		
		<!-- <div class="wrapper-md"> -->
		<div class="">
		  <div class="row">
			<div class="col-sm-12">

			
			
						  <div id="upload_section" style="display:none;border:1px solid #ccc;padding:5px;margin-bottom:20px;" class="col panel" nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
							<div class="wrapper-md">
							  <b>Bulk Upload</b> 
							  <span onclick="$('#upload_section').hide('slow');" type="button" class="btn btn-default btn-xs pull-right" style="margin-left:10px;">Close</span>
							  <p>Upload a zip file that contain all your logos(png file).</p>
							  <p>Please <a class="text-info" onclick="$('#logo_details_instruction').toggle('slow');">click here</a> to view the details for the logo standard that will be uploaded.</p>
							  <div id="logo_details_instruction" style="display:none;border:1px solid #bbb;padding:5px;margin-bottom:20px;">
								<div style="clear:both;"></div>
								<span onclick="$('#logo_details_instruction').hide('slow');" type="button" class="btn btn-default btn-xs pull-right" style="margin-left:10px;">Close</span>
								
								<ol>
									<li> Logos or images extension should be in "png"(portable network graphics) file format.</li>
									<li> Logos filename should be in this format "domain.com" which mean the full basename is "domain.com.png" 
										 <table class="table table-condensed table-hover">
											<thead>
												<tr><th>Filename</th><th>Basename</th></tr>
											</thead>
											<tbody>
												<tr><td>tiveni.com</td><td>tiveni.com.png</td></tr>
												<tr><td>tolasa.com</td><td>tolasa.com.png</td></tr>
												<tr><td>travelsavant.com</td><td>travelsavant.com.png</td></tr>
												<tr><td>tugaco.com</td><td>tugaco.com.png</td></tr>
											</tbody>
										 </table>
									</li>
									<li> Logo dimension should have only 612x311 or width=612px and height=311px. 
										 <br> <b>Note : </b> Observe proper margin which the <b>minimum</b> of 25px from each side(top, bottom, left, right). So margin can have more than 25px but not less than 25px;
										 <div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center text-danger">
												<h3>Correct</h3>
												<img class="img-responsive" src="/img/urbanisto2.png" style="border:1px solid #ddd;" />
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center text-danger">
												<h3>Wrong</h3>
												<img class="img-responsive" src="/img/urbanisto1.png" style="border:1px solid #ddd;" />
											</div>
										 </div>
									</li>
								</ol>
							  </div>
							  <p><a href="/samplelogos.zip" style="text-decoration:underline">View the sample Zip</a></p>
							  <div>
								  <!--<span class="pull-left"><input type="file" nv-file-select="" uploader="uploader" multiple  ng-click="clear()" /></span>-->
								  <span class="pull-left"><input type="file" nv-file-select="" uploader="uploader" ng-click="clear()" /></span>
								  
								  <!--<span class="pull-right">Queue length: <b class="badge bg-info">@{{ uploader.queue.length }}</b></span>-->
							  </div>
							  <br><br>
							  <table class="table bg-white-only b-a">
								  <thead>
									  <tr>
										  <th width="50%">Name</th>
										  <th ng-show="uploader.isHTML5">Size</th>
										  <th ng-show="uploader.isHTML5">Progress</th>
										  <th>Status</th>
										  <th>Actions</th>
									  </tr>
								  </thead>
								  <tbody>
									  <tr ng-repeat="item in uploader.queue">
										  <td>
											<strong>@{{ item.file.name }}</strong>
											<br>
											<span ng-if="item.file.size/1024/1024 >= 32" class="text-danger">
												  Filesize should be less than <b>32 MB</b>
											</span>
										  </td>
										  <td ng-show="uploader.isHTML5" nowrap>
											<span ng-if="item.file.size/1024/1024 >= 32" class="text-danger">
												  @{{ item.file.size/1024/1024|number:2 }} MB
											</span>
											<span ng-if="item.file.size/1024/1024 < 32" class="text-success">
												  @{{ item.file.size/1024/1024|number:2 }} MB
											</span>
										  </td>
										  <td ng-show="uploader.isHTML5">
											  <div class="progress progress-sm m-b-none m-t-xs">
												  <div class="progress-bar bg-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
											  </div>
										  </td>
										  <td class="text-center">
											  <span ng-show="item.isSuccess" class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
											  <span ng-show="item.isCancel" class="text-warning"><i class="glyphicon glyphicon-ban-circle"></i></span>
											  <span ng-show="item.isError" class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
										  </td>
										  <td nowrap>
											<span ng-if="item.file.size/1024/1024 >= 32">
											  <button type="button" readonly disabled class="btn btn-default btn-xs">
												  Upload
											  </button>
											</span>
											<span ng-if="item.file.size/1024/1024 < 32">
											  <button type="button" class="btn btn-default btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
												  Upload
											  </button>
											</span>
											
											  <button type="button" class="btn btn-default btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
												  Cancel
											  </button>
											  <button type="button" class="btn btn-default btn-xs" ng-click="item.remove()">
												  Remove
											  </button>
										  </td>
									  </tr>
								  </tbody>
							  </table>
							  
							<!--
							  <div>
								<div>
								  <p>Queue progress:</p>
								  <div class="progress bg-light dker" style="">
									  <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
								  </div>
								</div>
								<button type="button" class="btn btn-addon btn-success" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
								  <i class="fa fa-arrow-circle-o-up"></i> Upload all
								</button>
								<button type="button" class="btn btn-addon btn-warning" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
								  <i class="fa fa-ban"></i> Cancel all
								</button>
								<button type="button" class="btn btn-addon btn-danger" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
									<i class="fa fa-trash-o"></i> Remove all
								</button>
							  </div>
							-->
							 
							 <span ng-if="count_valid > 0">
								<br>
								<div class="panel-heading1" style="border:1px solid #ccc;padding:20px;">
									<div class="pull-left col-sm-12" onclick="$('#valid_result').toggle('slow');" style="cursor:pointer;">
										<div class="pull-left input-group text-success">
											(@{{ count_valid }}) Logo successfully uploaded. <br><br>
										</div>
										<i class="fa fa-caret-down pull-right"></i>
									</div>
									<div class="clearfix"></div>
									<div id="valid_result">
										<table class="table table-condensed b-t b-b">
											<thead>
												<th>Domain</th>
												<th>Uploaded Logo</th>
											</thead>
											<tbody>
											 <tr class="sort-item " ng-repeat="(index, domain_name) in all_valid">
												<td>@{{ domain_name }}</td>
												<td>
													<img class="img img-responsive" src="@{{ all_images[index] }}" ng-click="openLightboxModal('@{{ all_images_orig[index] }}', '@{{ all_images[index] }}', 0)">
													<img class="img img-responsive hidden" src="@{{ all_images_orig[index] }}">
												</td>
											 </tr>
											</tbody>
										</table> 
									</div>
								</div>
							 </span>
							 <span ng-if="count_invalid > 0">
								<br>
								<div class="panel-heading1" style="border:1px solid #ccc;padding:20px;">
									<div class="pull-left col-sm-12"  onclick="$('#invalid_result').toggle('slow');" style="cursor:pointer;">
										<div class="pull-left input-group text-danger">
											(@{{ count_invalid }}) Invalid Logo. <br><br>
										</div>
										<i class="fa fa-caret-down pull-right"></i>
									</div>
									<div class="clearfix"></div>
									<div id="invalid_result">
										<p><small class="text-muted">Possible cause of error are invalid file type, no domain extension found in the filename and invalid filename(or domain name) format.</small></p>
										<table class="table table-condensed b-t b-b">
											<thead>
												<th>Filename</th>
											</thead>
											<tbody>
											 <tr class="sort-item " ng-repeat="(index, domain_name) in all_invalid">
												<td>@{{ domain_name }}</td>
											 </tr>
											</tbody>
										</table> 
									</div>
								</div>
							 </span>
							 
							</div>
						  </div>
						  
						 
						 
			
						<div ng-init="getUploadedLogos(1);" id="domain-rows" >
						<span ng-if="count_rows > 0">
							<div class="sort-item isotope-item item col-xs-12 col-sm-6 col-md-3 col-lg-3 pull-left" ng-repeat="logo in logos">
								<div class="container-fluid panel panel-default pull-left">
									<div class="img-holder">
										<br/>
										<img class="img img-responsive" src="@{{logo.logo_pic}}" ng-click="openLightboxModal('@{{logo.logo_orig_pic}}', '@{{logo.logo_pic}}', 0)" src="@{{comment.logo_pic}}"> 
										<img class="img-responsive hidden" src="@{{logo.logo_orig_pic}}">
									</div>
									<div>
										<br/>
										<span class="pull-left text-info" >@{{logo.domain}}</span>
										<span ng-if="logo.status == 'P'">
											<span class="pull-right btn btn-xs btn-default" uib-tooltip="Pending"><i class="glyphicon glyphicon-option-horizontal"></i></span>
										</span>
										<span ng-if="logo.status == 'A'">
											<span class="pull-right btn btn-xs btn-default btn-success" uib-tooltip="Design already accepted."><i class="glyphicon glyphicon-thumbs-up"></i>   </span>
										</span>
										<span ng-if="logo.status == 'R'">
											<span class="pull-right btn btn-xs btn-default btn-danger" uib-tooltip="Design already rejected."><i class="glyphicon glyphicon-thumbs-up"></i>  </span>
										</span>
										
										<a class="clickable pull-right btn btn-xs btn-default" ui-sref="app.requestlogodetails({ idRequest: '@{{logo.id_request}}', idAssign: '@{{logo.id_assign}}' })" href="#/requestlogodetails/?idRequest=@{{logo.id_request}}&idAssign=@{{logo.id_assign}}" uib-tooltip="View Details Request"><i class="glyphicon glyphicon-eye-open"></i></a>
										<br>
										
										<hr/>
									</div>
								</div>
							</div>
						 </span>
						 <span ng-if="count_rows < 1">
							<p>No Logos Found</p>
						 </span>
						 </div>
					
					
					
				 
				
			 
			  
			  <!--
			  <div class="text-center m-t-lg m-b-lg">
				<ul class="pagination pagination-md">
				  <li><a href><i class="fa fa-chevron-left"></i></a></li>
				  <li class="active"><a href>1</a></li>
				  <li><a href>2</a></li>
				  <li><a href>3</a></li>
				  <li><a href>4</a></li>
				  <li><a href>5</a></li>
				  <li><a href><i class="fa fa-chevron-right"></i></a></li>
				</ul>
			  </div>
			  -->


			</div>
		  </div>
		</div>


</div>