			<div class="container w-xxl w-auto-xs">

			  <!-- toaster directive -->
			  <toaster-container toaster-options="{'position-class': 'toast-top-right', 'close-button':true}"></toaster-container>
			  <!-- / toaster directive -->

			  <a href class="navbar-brand block m-t" ng-init="displayMessage()">{{ config('app.name') }}</a>
			  <div class="panel panel-default">
				
					<?php
					if( strtolower($key_validation_status) == 'success' ) 	{	
						$text_color = 'text-success';
					}
					else	{	
						$text_color = 'text-danger';
					}
					?>
				
				  <div class="panel-body  text-center <?php echo $text_color; ?>" align="center">
					<?php echo $key_validation_status; ?>
					<br>
					<?php echo $key_validation_result;?>
					<br><br>
					<div class="list-group bg-info auto m-b-sm m-b-lg">
						<a class="list-group-item" ng-click="goToMainPage()">
							<i class="fa fa-fw fa-unlock-alt m-r-xs"></i> Go to Sign in
						</a>
					</div>
				  </div>
			  </div>
			  <div class="text-center" ng-include="'{{ config('app.BASE_URL') }}/tpl/blocks/page_footer.html'">
				<p><small class="text-muted">ZendDomains by <a href="http://diversionmedia.com/" target="_blank">Diversion Media<br>&copy; 2017</small></p>
			  </div>
			</div>
