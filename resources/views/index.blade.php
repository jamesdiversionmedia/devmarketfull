<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="" data-ng-app="app">
<head>
  <meta charset="utf-8" />
  <title>{{ config('app.name') }}</title>
  <link rel="icon" href="img/icon.ico" type="image/x-icon">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="description" content="{{ config('app.name') }}" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="../libs/assets/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="../libs/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="../libs/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="../libs/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />

  <link rel="stylesheet" href="css/font.css" type="text/css" />
  <link rel="stylesheet" href="css/app.css" type="text/css" />
  <script type="text/javascript"> var base_url = '<?php echo config('app.BASE_URL'); ?>'</script>
</head>
<body>
<div class="app app-header-fixed" id="app" ui-view></div>
<!-- build:js js/app.angular.js -->
  <!-- jQuery -->
  <script src="../libs/jquery/jquery/dist/jquery.js"></script>  
  <!-- Bootstrap -->
  <script src="../libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Angular -->
  <script src="../libs/angular/angular/angular.js"></script>
  
  <script src="../libs/angular/angular-animate/angular-animate.js"></script>
  <script src="../libs/angular/angular-aria/angular-aria.js"></script>
  <script src="../libs/angular/angular-cookies/angular-cookies.js"></script>
  <script src="../libs/angular/angular-messages/angular-messages.js"></script>
  <script src="../libs/angular/angular-resource/angular-resource.js"></script>
  <script src="../libs/angular/angular-sanitize/angular-sanitize.js"></script>
  <script src="../libs/angular/angular-touch/angular-touch.js"></script>
  
  <script src="../libs/angular/angular-ui-router/release/angular-ui-router.js"></script> 
  <script src="../libs/angular/ngstorage/ngStorage.js"></script>
  <script src="../libs/angular/angular-ui-utils/ui-utils.js"></script>

  <!-- bootstrap -->
  <script src="../libs/angular/angular-bootstrap/ui-bootstrap-tpls.js"></script>
  <!-- lazyload -->
  <script src="../libs/angular/oclazyload/dist/ocLazyLoad.js"></script>
  <!-- translate -->
  <script src="../libs/angular/angular-translate/angular-translate.js"></script>
  <script src="../libs/angular/angular-translate-loader-static-files/angular-translate-loader-static-files.js"></script>
  <script src="../libs/angular/angular-translate-storage-cookie/angular-translate-storage-cookie.js"></script>
  <script src="../libs/angular/angular-translate-storage-local/angular-translate-storage-local.js"></script>
  
  <!-- App -->
  <script src="js/angular/app.js"></script>
  <script src="js/angular/config.js"></script>
  <script src="js/angular/config.lazyload.js"></script>
  <script src="js/angular/config.router.js"></script>
  <script src="js/angular/main.js"></script>
  <script src="js/angular/services/ui-load.js"></script>
  
  	<script src="js/angular/services/ui-jp.config.js"></script>
	<script src="js/angular/services/ui-jp.js"></script>

  <script src="js/angular/filters/fromNow.js"></script>
  <script src="js/angular/directives/setnganimate.js"></script>
  <script src="js/angular/directives/ui-butterbar.js"></script>
  <script src="js/angular/directives/ui-focus.js"></script>
  <script src="js/angular/directives/ui-fullscreen.js"></script>
  <script src="js/angular/directives/ui-jq.js"></script>
  <script src="js/angular/directives/ui-module.js"></script>
  <script src="js/angular/directives/ui-nav.js"></script>
  <script src="js/angular/directives/ui-scroll.js"></script>
  <script src="js/angular/directives/ui-shift.js"></script>
  <script src="js/angular/directives/ui-toggleclass.js"></script>
  <script src="js/angular/controllers/bootstrap.js"></script>
  
  <!-- block-ui -->
  <link rel="stylesheet" href="/js/angular/app/block-ui/angular-block-ui.min.css" type="text/css" />
  <script src="/js/angular/app/block-ui/angular-block-ui.min.js"></script>
  
  <!--lightbox -->
  <link rel="stylesheet" href="/js/angular/app/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.css">
  <script src="/js/angular/app/angular-bootstrap-lightbox/dist/ui-bootstrap-tpls-1.3.2.js"></script>
  <script src="/js/angular/app/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.js"></script>
  
  <!--tooltip -->
  <!--
  <link href="/js/angular/app/angular-tooltips/dist/angular-tooltips.min.css" rel="stylesheet" type="text/css" />
  <script src="/js/angular/app/angular-tooltips/dist/angular-tooltips.min.js"></script>
  -->
  
</body>
</html>