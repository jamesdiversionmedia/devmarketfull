<div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="basic_info_tab()">
  <div class="col">
    <div style="background:url(img/c4.jpg) center center; background-size:cover">
      <div class="wrapper-lg bg-white-opacity">
        <div class="row m-t">
          <div class="col-sm-7">
            <a href class="thumb-lg pull-left m-r">
			  <?php /*
              <img src="<?php echo API_URL; ?>image.php?id_user=<?php echo $_SESSION['id_user']; ?>&width=128&height=128" alt="<?php echo $_SESSION['fullname']; ?>" title="<?php echo $_SESSION['fullname']; ?>" class="img-circle">
			  */ ?>
			  <img src="@{{basicinfo.profile_pic}}" alt="@{{basicinfo.first_name}} @{{basicinfo.last_name}}" title="@{{basicinfo.first_name}} @{{basicinfo.last_name}}" class="img-circle" class="img-circle">
            </a>
            <div class="clear m-b">
              <div class="m-b m-t-sm">
                <span class="h3 text-black">@{{basicinfo.first_name}} @{{basicinfo.last_name}}</span>
                <small class="m-l">London, UK</small>
              </div>
              <p class="m-b">
                <a href class="btn btn-sm btn-bg btn-rounded btn-default btn-icon"><i class="fa fa-twitter"></i></a>
                <a href class="btn btn-sm btn-bg btn-rounded btn-default btn-icon"><i class="fa fa-facebook"></i></a>
                <a href class="btn btn-sm btn-bg btn-rounded btn-default btn-icon"><i class="fa fa-google-plus"></i></a>
              </p>
              <a href class="btn btn-sm btn-success btn-rounded"> <i class="glyphicon glyphicon-star"></i> Top Rated </a>
            </div>
          </div>
          <div class="col-sm-5">
            <div class="pull-right pull-none-xs text-center">
              <a href class="m-b-md inline m">
                <span class="h3 block font-bold">5</span>
                <small>Inprogress Design</small>
              </a>
              <a href class="m-b-md inline m">
                <span class="h3 block font-bold">87</span>
                <small>Completed Design</small>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrapper bg-white b-b">
      <ul class="nav nav-pills nav-sm" id="myprofile_nav">
        <li id="li_basic_info_tab"><a href ng-click="basic_info_tab()">Basic Info</a></li>
        <li id="li_settings_tab"><a href ng-click="settings_tab()">Settings</a></li>
		<li id="li_other_info_tab"><a href ng-click="other_info_tab()">Other Info</a></li>
		<li id="li_profile_pic_tab"><a href ng-click="profile_pic_tab()">Profile Pic</a></li>
      </ul>
    </div>
    <div class="padder">
	
	
      <div class="panel panel-default" id="des_basic_info" style="display:block;margin-top:20px;">
        <div class="panel-heading font-bold">Basic Info</div>
        <div class="panel-body">
          <form role="form">
            <div class="form-group">
              <label>Firstname</label>
              <input type="text" class="form-control" placeholder="Enter firstname" value="@{{basicinfo.first_name}}">
            </div>
			<div class="form-group">
              <label>Lastname</label>
              <input type="text" class="form-control" placeholder="Enter lastname" value="@{{basicinfo.last_name}}">
            </div>
			<div class="checkbox">
              <label class="i-checks">
                <input name="gender" type="radio"> <i></i> Male
              </label>
              <label class="i-checks">
                <input name="gender" type="radio"><i></i> Female
              </label>
            </div>
			<div class="form-group">
              <label>Birthdate</label>
              <input type="text" class="form-control" placeholder="Enter Birthdate">
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
          </form>
        </div>
      </div>
	  
	 
      <div class="panel panel-default" id="des_settings" style="display:none;margin-top:20px;">
        <div class="panel-heading font-bold">Settings</div>
        <div class="panel-body">
          <form role="form" id="frm_settings_info" name="frm_settings_info" method="post">
            <div class="form-group">
              <label>No. of pending designs can handle </label>
              <input name="no_pending_designs" id="no_pending_designs" type="text" class="form-control" placeholder="Enter a number" value="@{{settings.no_of_pending_logo_can_handle}}" >
            </div>
			<div class="checkbox">
              <label class="i-checks-test" data-toggle="tooltip" data-placement="top" alt="Uncheck this if you want yourself unavailable so that you can't received any new task." title="Uncheck this if you want yourself unavailable so that you can't received any new task.">
			  <span ng-if="settings.is_available == '1' || settings.is_available == ''">
				<input name="is_available" id="is_available" type="checkbox" checked="checked" > <i></i> set my profile available?
			  </span>
			  <span ng-if="settings.is_available == '0'"><input name="is_available" id="is_available" type="checkbox" > <i></i> set my profile available?</span>
              </label>
            </div>
			</br>
            <button ng-click="saveSettingsInfo()" type="button" class="btn btn-sm btn-primary">Save</button>
          </form>
        </div>
      </div>
	
      <div class="panel panel-default" id="des_other_info" style="display:block;margin-top:20px;">
        <div class="panel-heading font-bold">Other Info</div>
        <div class="panel-body">
          <form role="form">
			<div class="form-group">
              <label>Organization</label>
              <input type="text" class="form-control" placeholder="Enter Organization">
            </div>
			<div class="form-group">
              <label>Country/Region</label>
				<select class="form-control">
				  <option>....</option>
				</select>
            </div>
			<div class="form-group">
              <label>State</label>
				<select class="form-control">
				  <option>....</option>
				</select>
            </div>
			<div class="form-group">
              <label>Zip</label>
              <input type="text" class="form-control" placeholder="Enter zip" >
            </div>
			<div class="form-group">
              <label>City</label>
              <input type="text" class="form-control" placeholder="Enter city" >
            </div>
			<div class="form-group">
              <label>Work Phone</label>
              <input type="text" class="form-control" placeholder="Enter work phone" >
            </div>
			<div class="form-group">
              <label>Home Phone</label>
              <input type="text" class="form-control" placeholder="Enter home phone" >
            </div>
			<div class="form-group">
              <label>Mobile Phone</label>
              <input type="text" class="form-control" placeholder="Enter mobile phone" >
            </div>
			<div class="form-group">
              <label>Fax</label>
              <input type="text" class="form-control" placeholder="Enter fax" >
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
          </form>
        </div>
      </div>	 

      <div class="panel panel-default" id="des_profilepic_info" style="display:block;margin-top:20px;" nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
        <div class="panel-heading font-bold">Profile Pic</div>
        <div class="panel-body">
          <form role="form">
            <div class="form-group">
              <label>Upload your profile pic</label>
			  <input type="file" nv-file-select="" uploader="uploader" ng-click="clear()" />
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
          </form>
		  
		  <table class="table bg-white-only b-a">
			  <thead>
				  <tr>
					  <th width="50%">Name</th>
					  <th ng-show="uploader.isHTML5">Size</th>
					  <th ng-show="uploader.isHTML5">Progress</th>
					  <th>Status</th>
					  <th>Actions</th>
				  </tr>
			  </thead>
			  <tbody>
				  <tr ng-repeat="item in uploader.queue">
					  <td><strong>@{{ item.file.name }}</strong></td>
					  <td ng-show="uploader.isHTML5" nowrap>@{{ item.file.size/1024/1024|number:2 }} MB</td>
					  <td ng-show="uploader.isHTML5">
						  <div class="progress progress-sm m-b-none m-t-xs">
							  <div class="progress-bar bg-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
						  </div>
					  </td>
					  <td class="text-center">
						  <span ng-show="item.isSuccess" class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
						  <span ng-show="item.isCancel" class="text-warning"><i class="glyphicon glyphicon-ban-circle"></i></span>
						  <span ng-show="item.isError" class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
					  </td>
					  <td nowrap>
						  <button type="button" class="btn btn-default btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
							  Upload
						  </button>
						  <button type="button" class="btn btn-default btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
							  Cancel
						  </button>
						  <button type="button" class="btn btn-default btn-xs" ng-click="item.remove()">
							  Remove
						  </button>
					  </td>
				  </tr>
			  </tbody>
		  </table>

		  
        </div>
      </div>
	 
	 
    </div>
  </div>
  <div class="col w-lg bg-light lter b-l bg-auto">
    <div class="wrapper">
      <div class="">
        <h4 class="m-t-xs m-b-xs">Who to follow</h4>
        <ul class="list-group no-bg no-borders pull-in">
          <li class="list-group-item">
            <a herf class="pull-left thumb-sm avatar m-r">
              <img src="img/a4.jpg" alt="..." class="img-circle">
              <i class="on b-white bottom"></i>
            </a>
            <div class="clear">
              <div><a href>Chris Fox</a></div>
              <small class="text-muted">Designer, Blogger</small>
            </div>
          </li>
          <li class="list-group-item">
            <a herf class="pull-left thumb-sm avatar m-r">
              <img src="img/a5.jpg" alt="..." class="img-circle">
              <i class="on b-white bottom"></i>
            </a>
            <div class="clear">
              <div><a href>Mogen Polish</a></div>
              <small class="text-muted">Writter, Mag Editor</small>
            </div>
          </li>
          <li class="list-group-item">
            <a herf class="pull-left thumb-sm avatar m-r">
              <img src="img/a6.jpg" alt="..." class="img-circle">
              <i class="busy b-white bottom"></i>
            </a>
            <div class="clear">
              <div><a href>Joge Lucky</a></div>
              <small class="text-muted">Art director, Movie Cut</small>
            </div>
          </li>
          <li class="list-group-item">
            <a herf class="pull-left thumb-sm avatar m-r">
              <img src="img/a7.jpg" alt="..." class="img-circle">
              <i class="away b-white bottom"></i>
            </a>
            <div class="clear">
              <div><a href>Folisise Chosielie</a></div>
              <small class="text-muted">Musician, Player</small>
            </div>
          </li>
        </ul>
      </div>
      <div class="panel b-a">
        <h4 class="font-thin padder">Latest Tweets</h4>
        <ul class="list-group">
          <li class="list-group-item">
              <p>Wellcome <a href class="text-info">@Drew Wllon</a> and play this web application template, have fun1 </p>
              <small class="block text-muted"><i class="fa fa-fw fa-clock-o"></i> 2 minuts ago</small>
          </li>
          <li class="list-group-item">
              <p>Morbi nec <a href class="text-info">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>
              <small class="block text-muted"><i class="fa fa-fw fa-clock-o"></i> 1 hour ago</small>
          </li>
          <li class="list-group-item">                     
              <p><a href class="text-info">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis</p>
              <small class="block text-muted"><i class="fa fa-fw fa-clock-o"></i> 2 hours ago</small>
          </li>
        </ul>
      </div>
      <div class="panel b-light clearfix">
        <div class="panel-body">
          <a href class="thumb pull-left m-r">
            <img src="img/a0.jpg" class="img-circle">
          </a>
          <div class="clear">
            <a href class="text-info">@Mike Mcalidek <i class="fa fa-twitter"></i></a>
            <small class="block text-muted">2,415 followers / 225 tweets</small>
            <a href class="btn btn-xs btn-success m-t-xs">Follow</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" >
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
</script>