    <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-dark">
      <div class="aside-wrap">
        <div class="navi-wrap">
          <!-- user -->
          <div class="clearfix hidden-xs text-center hide" id="aside-user">
            <div class="dropdown wrapper">
              <a href="app.page.profile">
                <span class="thumb-lg w-auto-folded avatar m-t-sm">
                  <img src="img/a0.jpg" class="img-full" alt="...">
                </span>
              </a>
              <a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
                <span class="clear">
                  <span class="block m-t-sm">
                    <strong class="font-bold text-lt">John.Smith</strong> 
                    <b class="caret"></b>
                  </span>
                  <span class="text-muted text-xs block">Art Director</span>
                </span>
              </a>
              <!-- dropdown -->
              <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                <li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
                  <span class="arrow top hidden-folded arrow-info"></span>
                  <div>
                    <p>300mb of 500mb used</p>
                  </div>
                  <div class="progress progress-xs m-b-none dker">
                    <div class="progress-bar bg-white" data-toggle="tooltip" data-original-title="50%" style="width: 50%"></div>
                  </div>
                </li>
                <li>
                  <a href>Settings</a>
                </li>
                <li>
                  <a ui-sref="app.myprofile" href="#/myprofile">Profile</a>
                </li>
                <li>
                  <a href>
                    <span class="badge bg-danger pull-right">3</span>
                    Notifications
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="page_signin.html">Logout</a>
                </li>
              </ul>
              <!-- / dropdown -->
            </div>
            <div class="line dk hidden-folded"></div>
          </div>
          <!-- / user -->

          <!-- nav -->
          <nav ui-nav class="navi clearfix">
            <ul class="nav" id="sidebar_menu">
			  <li id="li_line_main" class="line dk hidden-folded"></li>
              <li id="li_legend" class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                <span>Navigation</span>
              </li>
              <li id="li_dashboard">
                <a ui-sref="app.main" href="#/main">
                   <i class="glyphicon glyphicon-stats icon text-primary-dker"></i>
                  <span>Dashboard</span>
                </a>
              </li>
              <li id="li_logos">
                <a  ui-sref="app.uploadedlogo" href="#/uploadedlogo">
                  <i class="glyphicon glyphicon-th-large icon text-success"></i>
                  <span class="font-bold">Logos</span>
                </a>
              </li>
              <li id="li_requests">
                <a href class="auto">      
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="glyphicon glyphicon-edit"></i>
                  <span class="font-bold">Requests</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li id="li_requests_pending">
                    <a ui-sref="app.pendingrequests" href="#/pendingrequests">
                      <span>Pending</span>
                    </a>
                  </li>
                  <li id="li_requests_accepted">
                    <a ui-sref="app.acceptedrequests" href="#/acceptedrequests">
                      <span>Accepted</span>
                    </a>
                  </li>
                  <li id="li_requests_completed">
                    <a ui-sref="app.completedrequests" href="#/completedrequests">
                      <span>Completed</span>
                    </a>
                  </li>
                  <li id="li_requests_rejected">
                    <a ui-sref="app.rejectedrequests" href="#/rejectedrequests">
                      <span>Rejected</span>
                    </a>
                  </li>
				  <li id="li_requests_expired">
                    <a ui-sref="app.expiredrequests" href="#/expiredrequests">
                      <span>Expired</span>
                    </a>
                  </li>
                </ul>
              </li>
			  

			  
			<!--
			<li id="li_logos">
				<a href="" class="auto">
				  <span class="pull-right text-muted">
					<i class="fa fa-fw fa-angle-right text"></i>
					<i class="fa fa-fw fa-angle-down text-active"></i>
				  </span>
				  <i class="glyphicon glyphicon-th-large icon text-success"></i>
				  <span class="font-bold">Logos</span>
				</a>
				<ul class="nav nav-sub dk">
				  <li id="li_logos_pending">
					<a ui-sref="app.uploadedlogo" href="#/uploadedlogo">
					  <span>Logos</span>
					</a>
				  </li>
				 <li id="li_logos_upload">
					<a ui-sref="app.submitlogo" href="#/submitlogo">
					  <span>Upload</span>
					</a>
				   </li>
				</ul>
			  </li>
			  -->
			  
              <li id="li_unreadmessages">
                <a href="#">
                  <b class="badge bg-info pull-right">9</b>
                  <i class="glyphicon glyphicon-envelope icon text-info-lter"></i>
                  <span class="font-bold">Unread Messages</span>
                </a>
              </li>
			  <!-- *******
              <li id="li_files">
                <a>
                  <i class="glyphicon glyphicon-file icon"></i>
                  <span>Files</span>
                </a>
              </li>
              <li id="li_payments">
                <a href="ui_chart.html">
                  <i class="glyphicon glyphicon-signal"></i>
                  <span>Payments</span>
                </a>
              </li>
			  *** -->
			  <li id="li_line" class="line dk hidden-folded"></li>
              <li id="li_legend" class="hidden-folded padder m-t m-b-sm text-muted text-xs">          
                <span>Your Stuff</span>
              </li>  
              <li id="li_profile">
                <a ui-sref="app.myprofile" href="#/myprofile">
                  <i class="icon-user icon text-success-lter"></i>
                  <span>Profile</span>
                </a>
              </li>
              <li id="li_logout">
                <a ui-sref="app.signout" href="#/signout">
                  <i class="glyphicon glyphicon-log-out"></i>
                  <span>Logout</span>
                </a>
              </li>
			  <!-- *******
              <li id="li_documents">
                <a href>
                  <i class="icon-question icon"></i>
                  <span>Documents</span>
                </a>
              </li>
			  *** -->
			  
            </ul>
          </nav>
          <!-- nav -->

          <!-- aside footer -->
		  <!--
          <div class="wrapper m-t">
            <div class="text-center-folded">
              <span class="pull-right pull-none-folded">60%</span>
              <span class="hidden-folded">Milestone</span>
            </div>
            <div class="progress progress-xxs m-t-sm dk">
              <div class="progress-bar progress-bar-info" style="width: 60%;">
              </div>
            </div>
            <div class="text-center-folded">
              <span class="pull-right pull-none-folded">35%</span>
              <span class="hidden-folded">Release</span>
            </div>
            <div class="progress progress-xxs m-t-sm dk">
              <div class="progress-bar progress-bar-primary" style="width: 35%;">
              </div>
            </div>
          </div>
		  -->
          <!-- / aside footer -->
        </div>
      </div>
  </aside>
  <!-- / aside -->
