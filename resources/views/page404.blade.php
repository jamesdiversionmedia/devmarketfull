<div class="container w-xxl w-auto-xs" ng-init="app.settings.container = false;">
  <div class="text-center m-b-lg">
    <h1 class="text-shadow text-white">404</h1>
  </div>
  <div class="list-group bg-info auto m-b-sm m-b-lg">
	<?php if( isset($session['id_user']) && isset($session['friendly']) )	{	?>
			<a href="#/" class="list-group-item">
			  <i class="fa fa-chevron-right text-muted"></i>
			  <i class="fa fa-fw fa-mail-forward m-r-xs"></i> Goto application
			</a>
	<?php	}	else	{	?>
		<a ui-sref="app.signin" href="#/signin" class="list-group-item">
		  <i class="fa fa-chevron-right text-muted"></i>
		  <i class="fa fa-fw fa-sign-in m-r-xs"></i> Sign in
		</a>
		<a ui-sref="app.signup" href="#/signup" class="list-group-item">
		  <i class="fa fa-chevron-right text-muted"></i>
		  <i class="fa fa-fw fa-unlock-alt m-r-xs"></i> Sign up
		</a>
	<?php	}	?>
  </div>
  <div class="text-center" ng-include="'<?php echo config('app.BASE_URL'); ?>tpl/blocks/page_footer.html'">
    {% include 'blocks/page_footer.html' %}
  </div>
</div>