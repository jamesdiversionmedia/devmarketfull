    <div class="container w-xl w-auto-xs" ng-controller="SignupFormController" ng-init="app.settings.container = false;">
	
	  <!-- toaster directive -->
	  <toaster-container toaster-options="{'position-class': 'toast-top-right', 'close-button':true}"></toaster-container>
	  <!-- / toaster directive -->
	
	  <img class="img-responsive" src="<?php echo config('app.BASE_URL'); ?>/img/zd-transparent-white.png">
	  <!-- <a href class="navbar-brand block m-t"> {{ config('app.name') }} </a> -->
      <div class="m-b-lg">
        <div class="wrapper text-center">
          <h4>Sign up to find interesting thing</h4>
        </div>
        <form name="form" class="form-validation">
          <div class="text-danger wrapper text-center" ng-show="authError">
              
          </div>
          <div class="list-group list-group-sm">
            <div class="list-group-item">
              <input placeholder="Firstname" class="form-control no-border" ng-model="user.fname" required>
            </div>
            <div class="list-group-item">
              <input placeholder="Lastname" class="form-control no-border" ng-model="user.lname" required>
            </div>
            <div class="list-group-item">
              <input type="email" placeholder="Email" class="form-control no-border" ng-model="user.email" required>
            </div>
            <div class="list-group-item">
               <input type="password" placeholder="Password" class="form-control no-border" ng-model="user.password" required>
            </div>
          </div>
          <div class="checkbox m-b-md m-t-none">
            <label class="i-checks">
              <input type="checkbox" ng-model="agree" required><i></i> Agree the <a href>terms and policy</a>
            </label>
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="signup()" ng-disabled='form.$invalid'> <i class="glyphicon glyphicon-saved"></i> Sign up</button>
          <div class="line line-dashed"></div>
          <p class="text-center"><small>Already have an account?</small></p>
          <a ui-sref="app.signin" href="#/signin" class="btn btn-lg btn-default btn-block"> <i class="glyphicon glyphicon-log-in"> </i> Sign in</a>
        </form>
      </div>
	  <div class="text-center">
		<p><small class="text-muted">ZenDomains by <a href="http://diversionmedia.com/" target="_blank">Diversion Media<br>&copy; <?php echo date("Y"); ?></small></p>
	  </div>
    </div>