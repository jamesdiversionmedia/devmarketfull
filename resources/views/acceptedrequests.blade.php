
		<!--
		<div class="bg-light lter b-b wrapper-md">
		  <h1 class="m-n font-thin h3">Post</h1>
		</div>
		-->
		<div class="wrapper-md" ng-init="getRequestDesigns('C',1);">
		  <div class="row">
			<div class="col-sm-12">
			  <div class="blog-post">                   
				<div class="panel">
				  <div class="wrapper-lg">
					<!--<h2 class="m-t-none"><a href>Accepted</a></h2>-->
					
					<div class="panel-heading">
						<div class="pull-left text-left">
							<h3 class="m-t-none"><a href>Accepted</a> </h3>
						</div>
						<div class="pull-right">
							<uib-pagination rotate="false" boundary-links="true" items-per-page="itemsPerPage" max-size="maxSize" total-items="totalItems" ng-model="pagination.currentPage" ng-change="selectPage(pagination.currentPage)" class="m-t-none m-b pull-left" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></uib-pagination>
							<small class="text-muted inline m-t-sm m-b-sm pull-left" style="margin-left:10px;">@{{ params_range }}</small>
							<select class="pull-left input-sm form-control w-sm inline v-middle" id="view_limit" name="view_limit" style="margin-left:10px;"  ng-model="row_per_page" required ng-options='option.value as option.name for option in rowsPerPageOptions' ng-change="getRequestDesigns('C',1)"></select>
							<button ng-click="getRequestDesigns('C',currentPage);"  type="button" class="btn btn-sm btn-default"  style="margin-left:10px;"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>
						</div>
						<div class="clearfix"></div>
						<div class="pull-left">
							<p>List of request that you have confirm or accepted.</p> 
						</div>
						<div class="clearfix"></div>
					</div>
					
					<table class="table table-striped b-t b-b">
						<thead>
						  <tr>
							<th>Domain</th>
							<th>Date Requested</th>
							<th>Date Accepted</th>
							<th>Assign Method</th>
							<th>Seller</th>
							<th>Action</th>
						  </tr>
						</thead>
						<tbody ng-if="count_rows > 0">
							<tr ng-repeat="logo in logos">
								<td>@{{logo.domain}}</td>
								<td>@{{logo.requested_date}}</td>
								<td>@{{logo.confirmed_date}}</td>
								<td>@{{logo.assign_method}}</td>
								<td>@{{logo.sellerfullname}}</td>
								<td>
									<a ui-sref="app.requestlogodetails({ idRequest: '@{{logo.id_request}}', idAssign: '@{{logo.id_assign}}' })" href="#/requestlogodetails/?idRequest=@{{logo.id_request}}&idAssign=@{{logo.id_assign}}" class="btn btn-sm btn-success">
										<i class="glyphicon glyphicon-upload"></i> Submit Logo
									</a>
									<a ui-sref="app.requestlogodetails({ idRequest: '@{{logo.id_request}}', idAssign: '@{{logo.id_assign}}' })" href="#/requestlogodetails/?idRequest=@{{logo.id_request}}&idAssign=@{{logo.id_assign}}" class="btn btn-sm btn-info">
										<i class="glyphicon glyphicon-eye-open"></i> Details
									</a>
								</td>
							</tr>
						</tbody>
						 <tbody ng-if="count_rows < 1">
							<tr><td colspan="6">No records found.</td></tr>
						</tbody>
					</table>
					
					
				  </div>
				</div>

			  </div>
			  
			  <!--
			  <div class="text-center m-t-lg m-b-lg">
				<ul class="pagination pagination-md">
				  <li><a href><i class="fa fa-chevron-left"></i></a></li>
				  <li class="active"><a href>1</a></li>
				  <li><a href>2</a></li>
				  <li><a href>3</a></li>
				  <li><a href>4</a></li>
				  <li><a href>5</a></li>
				  <li><a href><i class="fa fa-chevron-right"></i></a></li>
				</ul>
			  </div>
			  -->


			</div>
		  </div>
		</div>


