
		<!--
		<div class="bg-light lter b-b wrapper-md">
		  <h1 class="m-n font-thin h3">Post</h1>
		</div>
		-->
		<div class="wrapper-md" ng-init="getRequestLogoDetails('<?php echo $_GET['id_request']; ?>');">
		  <div class="row">
		  
			
			<div class="col-sm-12">
			  <div class="blog-post">
				<div class="panel">
				  <div class="wrapper-md">
					<!--<h2 class="m-t-none"><a href>Request Logo Details</a></h2>-->
					
					<div class="panel-heading">
						<div class="pull-left text-left">
							<h3 class="m-t-none"><a href>Request Logo Details</a></h3>
						</div>
						<div class="pull-right text-right">
							<button ng-click="getRequestLogoDetails('<?php echo $_GET['id_request']; ?>');" type="button" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-refresh"></i> Refresh</button>
						</div>
						<div class="clearfix"></div>
					</div>
					
					<table class="table table-condensed">
						<thead>
						<tbody>
							<tr>
								<td><b>Domain</b></td>
								<td>@{{details.domain}}</td>
							</tr>
							<tr>
								<td><b>Date Requested</b></td>
								<td>@{{details.requested_date}}</td>
							</tr>
							<tr>
								<td><b>Seller</b></td>
								<td>@{{details.seller_fullname}}</td>
							</tr>
							<tr>
								<td><b>Assign Method</b></td>
								<td>@{{details.assign_method}}</td>
							</tr>
							<tr>
								<td><b>Status</b></td>
								<td>
									<span alt="@{{details.text_status_description}}" title="@{{details.text_status_description}}">
										<!-- @{{details.text_status}} -->
										<!-- It should be "Assigned" but since this is for designer then we will use "Accepted" -->
										<span uib-tooltip='@{{details.text_status_description}}' ng-if="details.status == 'A'"> Accepted <i class="glyphicon glyphicon-info-sign"></i> </span> 
										<span uib-tooltip='@{{details.text_status_description}}' ng-if="details.status != 'A'">
											@{{details.text_status}}
											<i class="glyphicon glyphicon-info-sign"></i>
										</span> 
										
									</span>
									<span ng-if="details.status == 'P'">
									<?php if( isset($_GET['id_assign']) )	{	?>
										<button id="btn_accept" ng-click="acceptRequest('<?php echo $_GET['id_assign']; ?>','@{{details.id_request}}','@{{details.domain}}')" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-thumbs-up"></i> Accept</button>
										<button id="btn_reject" ng-click="rejectRequest('<?php echo $_GET['id_assign']; ?>','@{{details.id_request}}','@{{details.domain}}')" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-thumbs-down"></i> Reject</button>
									<?php } ?>
									</span>
								</td>
							</tr>
							<tr ng-if="details.status == 'AP' || details.status == 'C'">
								<td><b>Accepted Logo</b></td>
								<td>
								  <img class="img-responsive" ng-click="openLightboxModal('@{{details.orig_pic}}', '@{{details.logo_pic}}', 0)" src="@{{details.logo_pic}}">
								  <img class="img-responsive hidden" src="@{{details.orig_pic}}">
								</td>
							</tr>
						</tbody>
					</table>
					
					
					
							
							
							<div class="panel panel-default">

								<div class="panel-heading" ng-click="toggleDiv('div_submitted_logos');" >
									All Submitted Logos <i class="fa fa-caret-down" style="float:right;"></i>
								</div>
								<div class="panel-body" id="div_submitted_logos" >
								<span ng-if="logoitems_rows > 0">
									<br>
									<div class="sort-item isotope-item item col-xs-12 col-sm-6 col-md-3 col-lg-3 pull-left" ng-repeat="logo in logoitems">
										<div class="container-fluid panel panel-default pull-left" style="padding-bottom:0px;">
											<div class="img-holder">
												<br/>
												<img class="img img-responsive" src="@{{logo.logo_pic}}" ng-click="openLightboxModal('@{{logo.orig_pic}}', '@{{logo.logo_pic}}', 0)" src="@{{logo.logo_pic}}"> 
												<img class="img-responsive hidden" src="@{{logo.orig_pic}}">
											</div>
											
											<br>
											<span ng-if="details.status != 'AP' && details.status != 'C'">
												<span ng-if="logo.status == 'P'">
													<span disabled class="pull-right btn btn-xs btn-default disabled" uib-tooltip="Pending"> Pending </span>
												</span>
											</span>
											<span ng-if="logo.status == 'A'">
												<span disabled class="pull-right btn btn-xs btn-default btn-success disabled" uib-tooltip="This design already accepted."><i class="glyphicon glyphicon-thumbs-up"></i> Accepted Design </span>
											</span>
											<span ng-if="logo.status == 'R'">
												<span disabled class="pull-right btn btn-xs btn-default btn-success disabled" uib-tooltip="This design already rejected."><i class="glyphicon glyphicon-thumbs-up"></i> Rejected Design </span>
											</span>
											<br><br>
										</div>
									</div>
								 </span>
								 <span ng-if="logoitems_rows < 1">
									<p>No Logos Submitted</p>
								 </span>
								 </div>
							</div>
							
							
							
					
					
					<div >
					  <!-- chat -->
					  <div class="panel panel-default">
					  <!--<div>-->
						<div class="panel-heading" ng-click="toggleDiv('div_messages');">
							Chat / Message History <i class="fa fa-caret-down" style="float:right;"></i>
						</div>
						<div class="panel-body" id="div_messages">
						  <div class="m-b">
							<a href class="pull-left thumb-sm avatar"><img class="img-responsive" src="@{{details.seller_pic}}" alt="@{{details.seller_fullname}}" title="@{{details.seller_fullname}}"></a>
							<div class="m-l-xxl">
							  <div class="pos-rlt wrapper b b-light r r-2x">
								<span class="arrow left pull-up"></span>
								<p class="m-b-none">@{{details.message}}</p>
							  </div>
							  <small class="text-muted"><i class="fa fa-ok text-success"></i>@{{details.requested_date}}</small>
							  <!--<small class="text-muted"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>-->
							</div>
						  </div>
						   
						  <div class="m-b" ng-repeat="comment in comments">
						  
							<span ng-if="comment.user_type != 'L'">
								<a href class="pull-left thumb-sm avatar">
									<img class="img-responsive" src="@{{comment.sender_pic}}" alt="@{{comment.sender_fullname}}" title="@{{comment.sender_fullname}}"></a>
								<div class="m-l-xxl">
								  <div class="pos-rlt wrapper b b-light r r-2x">
									<span class="arrow left pull-up"></span>
									<p class="m-b-none">
										@{{comment.comment}}
										<span ng-if="comment.id_checklist > 0">
										  <span ng-if="comment.comment != ''"><br><br></span>
										  <b>Uploaded Logo</b><br><br>
										  <img class="img-responsive" ng-click="openLightboxModal('@{{comment.orig_pic}}', '@{{comment.logo_pic}}', 0)" src="@{{comment.logo_pic}}">
										  <img class="img-responsive hidden" src="@{{comment.orig_pic}}">
										  <br>
										  <button ng-click="openLightboxModal('@{{comment.orig_pic}}', '@{{comment.logo_pic}}', 0)" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-eye-open"></i> View </button>
										  <!--<button ng-click="downloadFile('@{{comment.logo_filename}}')" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-download"></i> Download </button>-->
										  <a href="/ajax_downloadfile?file=@{{comment.logo_filename}}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-download"></i> Download </a>
										</span>
									</p>
								  </div>
								  <small class="text-muted"><i class="fa fa-ok text-success"></i>@{{comment.comment_on_date}}</small>
								</div>
							</span>
							<span ng-if="comment.user_type == 'L'">
								<a href class="pull-right thumb-sm avatar">
									<img class="img-responsive" src="@{{comment.sender_pic}}" alt="@{{comment.sender_fullname}}" title="@{{comment.sender_fullname}}" ></a>
								<div class="m-r-xxl">
								  <div class="pos-rlt wrapper bg-primary r r-2x">
									<span class="arrow right pull-up arrow-primary"></span>
									<p class="m-b-none">
										@{{comment.comment}}
										<span ng-if="comment.id_checklist > 0">
										  <span ng-if="comment.comment != ''"><br><br></span>
										  <b>Uploaded Logo</b><br><br>
										  
										  <img ng-if="comment.logo_status == 'A'" style="border: 3px solid #5cb85c;" class="img-responsive" ng-click="openLightboxModal('@{{comment.orig_pic}}', '@{{comment.logo_pic}}', 0)" src="@{{comment.logo_pic}}">
										  
										  <img ng-if="comment.logo_status != 'A'" style="border: 1px solid #fff;" class="img-responsive" ng-click="openLightboxModal('@{{comment.orig_pic}}', '@{{comment.logo_pic}}', 0)" src="@{{comment.logo_pic}}">
										  
										  <img class="img-responsive hidden" src="@{{comment.orig_pic}}">
										  <br>
										  <button ng-click="openLightboxModal('@{{comment.orig_pic}}', '@{{comment.logo_pic}}', 0)" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-eye-open"></i> View </button>
										  <!--<button ng-click="downloadFile('@{{comment.logo_filename}}')" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-download"></i> Download </button>-->
										  <a href="/ajax_downloadfile?file=@{{comment.logo_filename}}" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-download"></i> Download </a>
										  
											<span ng-if="details.status != 'A'"> 
												<button disabled="disabled" ng-if="comment.logo_status == 'A'" class="btn btn-xs btn-success" alt="Seller have accepted this design." title="Seller have accepted this design."><i class="glyphicon glyphicon-thumbs-up"></i> Accepted Design </button>
											</span>

										</span>
									</p>
								  </div>
								  <small class="text-muted">@{{comment.comment_on_date}}</small>
								</div>
							</span>
						  
						  </div>
						  
						  <!--
						  <div class="m-b">
							<a href class="pull-right thumb-sm avatar"><img src="img/a3.jpg" class="img-circle" alt="..."></a>
							<div class="m-r-xxl">
							  <div class="pos-rlt wrapper bg-primary r r-2x">
								<span class="arrow right pull-up arrow-primary"></span>
								<p class="m-b-none">Lorem ipsum dolor sit amet, conse <br>adipiscing eli...<br>:)</p>
							  </div>
							  <small class="text-muted">1 minutes ago</small>
							</div>
						  </div>
						  -->
						</div>
						
						

						
						
						
						
						
						<footer class="panel-footer">
						  <!-- chat form -->
						  <div>
							<a class="pull-left thumb-xs avatar">
								<img class="img-responsive" src="@{{login_user.user_pic}}" class="img-circle" alt="@{{login_user.user_fullname}}" title="@{{login_user.user_fullname}}">
							</a>
							<form class="m-b-none m-l-xl" id="frmSendComment">
							  <div class="input-group">
							    <!--<input id="txt_id_request" name="txt_id_request" type="hidden" value="@{{details.id_request}}"  />-->
								<input id="txt_id_request" name="txt_id_request" type="hidden" value="<?php echo $_GET['id_request']; ?>"  />
								<input id="txt_id_assign" name="txt_id_assign" type="hidden" value="<?php echo $_GET['id_assign']; ?>"  />
								<input id="txt_domain" name="txt_domain" type="hidden" value="@{{details.domain}}" >
								<input id="txt_comment" name="txt_comment" type="text" class="form-control" placeholder="Say something"  >
								<span class="input-group-btn">
								  <button ng-click="sendComment();" class="btn btn-default" type="button"> <i class="glyphicon glyphicon-send"></i> Send Message </button>
								  <button ng-click="attachFile ();" class="btn btn-default" type="button"> <i class="glyphicon glyphicon-upload"></i> Attach File </button>
								</span>
							  </div>
							</form>
						  </div>
						</footer>
						
						  <div id="upload_section" style="display:none;border:1px solid #ccc;padding:5px;" class="col" nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
							<div class="wrapper-md">
							  <div>
								  <!--<span class="pull-left"><input type="file" nv-file-select="" uploader="uploader" multiple  ng-click="clear()" /></span>-->
								  <span class="pull-left"><input type="file" nv-file-select="" uploader="uploader" ng-click="clear()" /></span>
								  <span onclick="$('#upload_section').hide('slow');" type="button" class="btn btn-default btn-xs pull-right" style="margin-left:10px;">Close</span>
								  <!--<span class="pull-right">Queue length: <b class="badge bg-info">@{{ uploader.queue.length }}</b></span>-->
							  </div>
							  <br><br>
							  <table class="table bg-white-only b-a">
								  <thead>
									  <tr>
										  <th width="50%">Name</th>
										  <th ng-show="uploader.isHTML5">Size</th>
										  <th ng-show="uploader.isHTML5">Progress</th>
										  <th>Status</th>
										  <th>Actions</th>
									  </tr>
								  </thead>
								  <tbody>
									  <tr ng-repeat="item in uploader.queue">
										  <td><strong>@{{ item.file.name }}</strong></td>
										  <td ng-show="uploader.isHTML5" nowrap>@{{ item.file.size/1024/1024|number:2 }} MB</td>
										  <td ng-show="uploader.isHTML5">
											  <div class="progress progress-sm m-b-none m-t-xs">
												  <div class="progress-bar bg-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
											  </div>
										  </td>
										  <td class="text-center">
											  <span ng-show="item.isSuccess" class="text-success"><i class="glyphicon glyphicon-ok"></i></span>
											  <span ng-show="item.isCancel" class="text-warning"><i class="glyphicon glyphicon-ban-circle"></i></span>
											  <span ng-show="item.isError" class="text-danger"><i class="glyphicon glyphicon-remove"></i></span>
										  </td>
										  <td nowrap>
											  <button type="button" class="btn btn-default btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
												  Upload
											  </button>
											  <button type="button" class="btn btn-default btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
												  Cancel
											  </button>
											  <button type="button" class="btn btn-default btn-xs" ng-click="item.remove()">
												  Remove
											  </button>
										  </td>
									  </tr>
								  </tbody>
							  </table>
							  
							<!--
							  <div>
								<div>
								  <p>Queue progress:</p>
								  <div class="progress bg-light dker" style="">
									  <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
								  </div>
								</div>
								<button type="button" class="btn btn-addon btn-success" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
								  <i class="fa fa-arrow-circle-o-up"></i> Upload all
								</button>
								<button type="button" class="btn btn-addon btn-warning" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
								  <i class="fa fa-ban"></i> Cancel all
								</button>
								<button type="button" class="btn btn-addon btn-danger" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
									<i class="fa fa-trash-o"></i> Remove all
								</button>
							  </div>
							-->
							 
							</div>
						  </div>
						
						
					  </div>
					  <!-- /chat -->
					</div>
					
					

					
				  </div>
				</div>

			  </div>
			</div>
				
		


			
		  </div>
		</div>

		
<script>
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}
</script>

