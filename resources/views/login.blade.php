<div class="container w-xl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">

  <!-- toaster directive -->
  <toaster-container toaster-options="{'position-class': 'toast-top-right', 'close-button':true}"></toaster-container>
  <!-- / toaster directive -->

  <img class="img-responsive" src="<?php echo config('app.BASE_URL'); ?>/img/zd-transparent-white.png">
  <!-- <a href class="navbar-brand block m-t"> {{ config('app.name') }} </a> -->
  <div class="m-b-lg">
	<div class="wrapper text-center">
	  <!-- <strong>Sign in to get in touch</strong> -->
	  <h4>Sign in to get in touch</h4>
	  <div class="hidden">test</div>
	</div>
	<form name="form" class="form-validation">
	  <div class="text-danger wrapper text-center" ng-show="authError">
		  
	  </div>
	  <div class="list-group list-group-sm">
		<div class="list-group-item">
		  <input type="email" placeholder="Email" class="form-control no-border" ng-model="user.email" required>
		</div>
		<div class="list-group-item">
		   <input type="password" placeholder="Password" class="form-control no-border" ng-model="user.password" required>
		</div>
	  </div>
	  <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'> <i class="glyphicon glyphicon-log-in"> </i> Log in</button>
	  <div class="text-center m-t m-b"><a ui-sref="app.forgotpwd" href="#/forgotpwd">Forgot password?</a></div>
	  <div class="line line-dashed"></div>
	  <p class="text-center"><small>Do not have an account?</small></p>
	  <a ui-sref="app.signup" href="#/signup" class="btn btn-lg btn-default btn-block"> <i class="glyphicon glyphicon-registration-mark"></i> Create an account</a>
	</form>
  </div>
  <div class="text-center">
	<p><small class="text-muted">ZenDomains by <a href="http://diversionmedia.com/" target="_blank">Diversion Media<br>&copy; <?php echo date("Y"); ?></small></p>
  </div>
</div>

