	function keepSession()	{			
		if( $('#span_unread_messages_count').attr('class') != undefined )	{
			var ajax_url = 'ng/json/keepsession?&inreadpage=1';
		}
		else	{
			var ajax_url = 'ng/json/keepsession';
		}

		$.ajax({
			type: "GET",
			url: ajax_url,
			async: false
		}).success(function(r)	{
			try	{
				r = $.parseJSON(r);
				console.log(r);
				if(r.result != 'SUCCESS')	{
					window.location.href="<?php echo APP_URL ?>login?h="+encodeURIComponent(window.location.hash);;
				}
				else	{
					if( r.inquiries != undefined )	{
						if( r.inquiries.count.count > 0 )	{
							$('#mssg_count_main').html(r.inquiries.count.count);
							$('#mssg_count').html(r.inquiries.count.count);
						}
						else	{
							$('#mssg_count_main').html('');
							$('#mssg_count').html('');
						}
						
						var counter = 0;
						var msg_str = '';
						$.each( r.inquiries.count.inquiries, function(i, inquiry)	{
							if(i < 2 )	{
								//<a class="clickable" ui-sref="app.salesview({ idInquiry: '831' })" href="#/salesview/?idInquiry=831">INQ0000831</a>
								if(inquiry.offer)	{
									var inq_link = ' ui-sref="app.salesviewoffers({ idInquiry: \''+inquiry.idlink+'\' })" href="#/salesviewoffers/?idInquiry='+inquiry.idlink+'" ';
								}
								else	{
									var inq_link = ' ui-sref="app.salesview({ idInquiry: \''+inquiry.idlink+'\' })" href="#/salesview/?idInquiry='+inquiry.idlink+'" ';
								}
								
								msg_str += ' \
									  <a '+inq_link+' class="list-group-item"> \
										<span class="pull-left m-r thumb-sm"> \
										  <img alt="'+inquiry.buyerfullname+'" title="'+inquiry.buyerfullname+'" src="'+inquiry.api_url+'/image.php?id_user='+inquiry.id_buyer+'&amp;width=128&amp;height=128" class="img-circle"> \
										</span> \
										<span class="clear block m-b-none"> \
										  '+inquiry.message+' \
										  <small class="text-muted">'+inquiry.received_on_formatted+'</small> \
										</span> \
									  </a> \
									';
							}
						});
						
						if( $('#span_unread_messages_count').attr('class') != undefined )	{
							var str_mssg = '';
							var data = r.inquiries;
							
							if( data.count.count > 0 )	{
								$.each( data.count.inquiries, function( i, inquiry ) {
									var formatted_message;
									if( inquiry.message.length < 20 )	{
										formatted_message = inquiry.message;
									}
									else	{
										formatted_message = inquiry.message.substr(0, 20) + ' ...';
									}

									if(inquiry.offer)	{
										var inq_link = ' ui-sref="app.salesviewoffers({ idInquiry: \''+inquiry.idlink+'\' })" href="#/salesviewoffers/?idInquiry='+inquiry.idlink+'" ';
									}
									else	{
										var inq_link = ' ui-sref="app.salesview({ idInquiry: \''+inquiry.idlink+'\' })" href="#/salesview/?idInquiry='+inquiry.idlink+'" ';
									}
									
									str_mssg += ' \
										<a '+inq_link+' class="list-group-item"> \
											<span class="pull-left thumb-sm avatar m-r"> \
											  <img alt="'+inquiry.buyerfullname+'" title="'+inquiry.buyerfullname+'" src="'+inquiry.api_url+'/image.php?id_user='+inquiry.id_buyer+'&amp;width=128&amp;height=128"> \
											  <i class="on b-white bottom"></i> \
											</span> \
											<span class="clear"> \
											  <span>'+inquiry.buyerfullname+'</span> \
											  <small class="text-muted">'+formatted_message+'</small> \
											  <small class="text-muted clear text-ellipsis"><i class="fa fa-clock-o fa-fw m-r-xs"></i>'+inquiry.received_on_formatted+'</small> \
											</span> \
										</a> \
									';
								});
							}
							else	{
									str_mssg += '\
									  <a class="list-group-item clearfix"> \
										<span class="clear"> \
										  <span>No unread messages found.</span> \
										  <small class="text-muted clear text-ellipsis"></small> \
										</span> \
									  </a> \
									  ';
							}
							$('#ul_unread_messages').html(str_mssg);
							$('#span_unread_messages_count').html(data.count.count);
						}
						
						$('#mssg_content').html(msg_str);
					}
				}
			}
			catch(exception)	{
				console.log(exception);
				window.location.href="<?php echo APP_URL ?>login?h="+encodeURIComponent(window.location.hash);;
			}
			
			setTimeout(function(){ keepSession() }, 15000);	// call every 15 seconds
			//setTimeout(function(){ keepSession() }, 120000);	// call every 2 minutes
			//setTimeout(function(){ keepSession() }, 300000);
			
		});
	}
	keepSession();
