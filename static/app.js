$('.date-picker').datepicker({
  autoclose: true
, startDate: '0d'
});
var chart = null;
function drawChart(data, visitor, $maptitle, xaxis)
{	
	chart = new Highcharts.Chart({
		chart: {
			renderTo: 'container',
			type: 'column',
			margin: 115
		},
		title: {
			text: 'Domain Visitors'
		},
		subtitle: {
			text: 'Source: <a href="#">brandeden.com</a>'
		},
		xAxis: {
			categories: data,
			title: {
				text: xaxis,
				style: { "color": "#428bca" },
				margin: 0,
				align: 'high'
			},
			min: 0,
			max: 10
		},
		yAxis: {
			min: 0,
			minTickInterval: 1,
			title: {
				text: 'Visitors',
				align: 'high',
				style: { "color": "#428bca" }
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ''
		},
		scrollbar: {
			enabled : true,
			barBackgroundColor: 'gray',
			barBorderRadius: 7,
			barBorderWidth: 0,
			buttonBackgroundColor: 'gray',
			buttonBorderWidth: 0,
			buttonArrowColor: 'yellow',
			buttonBorderRadius: 7,
			rifleColor: 'yellow',
			trackBackgroundColor: 'white',
			trackBorderWidth: 1,
			trackBorderColor: 'silver',
			trackBorderRadius: 7
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [{
			name: $maptitle,
			data: visitor
		}]
	});
}