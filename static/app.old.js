function init() {

  // Quick find domain



  // Transactions dropdown
  $('.viewTransactions').click(function() {

    $('.orderDropdown').slideUp();

    $(this).closest('.row').next().slideDown();

    return false;

  });

  // Set page title and subpage title
  var title = PAI.pageInfo().title;
  var titleSub = PAI.pageInfo().titleSub;
  $('#pageTitle').html(title);

  // Domains table
  $('#datatable_domains').dataTable( {
      "bProcessing": true,
      "sAjaxSource": 'http://api.zendomains.dev/domains?api_key=5849189d095a159fdcb35d3769995c15&limit=50000',
      "sAjaxDataProp": "domains",
      "columnDefs": [
        {
          "render": function (name, type, domains) {
            return '<a href="'+PAI.PATH+'domains/'+domains.domain+'">' + domains.domain + '</a>';
          },
          "targets": 0
        },
        {
          "render": function (name, type, domains) {
            return '$' + domains.price;
          },
          "targets": 1
        },
        {
          "render": function (name, type, domains) {
            if(domains.id_logo != 0) {
              return 'Yes';
            } else {
              return 'No';
            }
          },
          "targets": 2
        },
        {
          "render": function (name, type, domains) {
            if(domains.status == 1) {
              return 'Active';
            }
          },
          "targets": 3
        },
        {
          "render": function (name, type, domains) {
            return domains.category;
          },
          "targets": 4
        },
        {
          "render": function (name, type, domains) {
            return domains.reg_date;
          },
          "targets": 5
        },
      ],
      "lengthMenu": [
        [20, 50, 100, 150, -1],
        [20, 50, 100, 150, "All"]
      ],
      "pageLength": 20,
      "order": [
        [1, "asc"]
      ]
  });

  // Rearrange domains
  $("ul.reArrange").sortable({ opacity: 0.6, cursor: 'move', update: function() {

    var order = $(this).sortable("serialize") + '&action=reArrange';

    $.post("/content/includes/ajax/rearrange.php", order, function(theResponse) {
      console.log(theResponse);
    });

  }});

}

PAI('on', 'pageload', function() {

  init();

});
