<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


define('API_KEY','33a3d9c20664cce5d39ff8793b5196bb');
define('API_URL',$_SERVER['ZEN_API_URL']);

Route::get('/', 'DesignersController@index');
Route::get('/layout', 'DesignersController@layout');
Route::get('/phpinfo', 'DesignersController@phpinfo');

//Route::get('/', 'DesignersController@login');
Route::get('/testpage', 'DesignersController@testcheckDifference');
Route::get('/signin', 'DesignersController@signin');
Route::get('/signinemail', 'DesignersController@process_email');
Route::get('/signout', 'DesignersController@signout');
Route::get('/forgotpwd', 'DesignersController@forgotpwd');
Route::get('/signup', 'DesignersController@signup');
Route::get('/page404', 'DesignersController@page404');
Route::get('/main', 'DesignersController@main');

Route::get('/myprofile', 'DesignersController@myprofile');
Route::get('/pendingrequests', 'DesignersController@pendingrequests');
Route::get('/acceptedrequests', 'DesignersController@acceptedrequests');
Route::get('/completedrequests', 'DesignersController@completedrequests');
Route::get('/rejectedrequests', 'DesignersController@rejectedrequests');
Route::get('/expiredrequests', 'DesignersController@expiredrequests');
Route::get('/requestlogodetails', 'DesignersController@requestlogodetails');

Route::get('/uploadedlogo', 'DesignersController@uploadedlogo');
Route::get('/submitlogo', 'DesignersController@submitlogo');
Route::get('/ajax_get_uploaded_logos', 'DesignersController@ajax_get_uploaded_logos');
Route::post('/ajax_upload_logos', 'DesignersController@ajax_upload_logos');

Route::get('/process_email', 'DesignersController@process_email');
Route::get('/accept_logo_request', 'DesignersController@accept_logo_request');
Route::get('/reject_logo_request', 'DesignersController@reject_logo_request');

Route::post('/ajax_login', 'DesignersController@ajax_login');
Route::post('/ajax_signup', 'DesignersController@ajax_signup');
Route::get('/ajax_get_login_user', 'DesignersController@ajax_get_login_user');
Route::get('/ajax_getrequestdesigns/{status}', 'DesignersController@ajax_getrequestdesigns');
Route::get('/ajax_getrequestlogodetails/{id}', 'DesignersController@ajax_getrequestlogodetails');
Route::post('/ajax_accept_request', 'DesignersController@ajax_accept_request');
Route::post('/ajax_reject_request', 'DesignersController@ajax_reject_request');
Route::post('/ajax_send_comment', 'DesignersController@ajax_send_comment');
Route::post('/ajax_upload_messages_files', 'DesignersController@ajax_upload_messages_files');
Route::get('/ajax_downloadfile', 'DesignersController@ajax_downloadfile');



Route::post('/ajax_save_settings_info', 'DesignersController@ajax_save_settings_info');
Route::get('/ajax_get_settings_info', 'DesignersController@ajax_get_settings_info');
Route::get('/ajax_get_basic_info', 'DesignersController@ajax_get_basic_info');
Route::post('/ajax_upload_profile_pic', 'DesignersController@ajax_upload_profile_pic');
Route::get('/ajax_upload_profile_pic', 'DesignersController@ajax_upload_profile_pic');