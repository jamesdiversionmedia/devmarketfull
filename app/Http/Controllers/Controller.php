<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function __construct(){
		
	}
	
	/**
		@auhtor: 		James Bolongan
		@datecreate: 	February 02, 2017
		@description: 	check if user login through session
	**/	
	public function is_user_login()	{
		
		if (version_compare(phpversion(), '5.4.0', '<')) {
			 if(session_id() == '') {
				session_start();
			 }
		 }
		 else	{
			if (session_status() == PHP_SESSION_NONE) {
				session_start();
			}
		 }
		/*
		if ((function_exists('session_status') 
		  && session_status() !== PHP_SESSION_ACTIVE) || !session_id()) {
		  session_start();
		}
		*/
		if( isset($_SESSION['friendly']) && $_SESSION['id_user'] )
			return true;
		else
			return false;
	}
	
	/**
		@auhtor: 		James Bolongan
		@datecreate: 	February 09, 2017
		@description: 	check if user login through session
	**/	
	public function is_email_key_valid()	{
		$data = array();
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t confirm your email. Invalid key.';

			if( count($vars) < 4 )	{
				$data['key'] = $_REQUEST['key'];
				$data['message'] = 'Can\'t confirm your email. Invalid key.';
				$data['status'] = 'Error';
				return $data;
			}
			
			//$user_info = $this->api_call('users/'.$vars[0]);
			$user_info = $this->api('users/'.$vars[0]);
			
			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 3 )	{
				$_data = array('emailconfirm' => '1', 'id_user'=> $user_info->id);
				
				//$result = APICall::api_call('users/emailconfirm', $_data, 'POST');
				$result = $this->api('users/emailconfirm', $_data, 'POST');
				$data['key'] = $_REQUEST['key'];
				$data['message'] = $result->response_details;
				$data['status'] = 'Success';
				return $data;
			}
			else	{
				$data['key'] = $_REQUEST['key'];
				$data['message'] = 'Can\'t confirm your email. Invalid key.';
				$data['status'] = 'Error';
				return $data;
			}
		}
		else	{
			$data['key'] = '';
			$data['message'] = '';
			$data['status'] = '';
			return $data;
		}
	}
	
	/**
		@description: 	api connection
	**/	
	function api($link, $data = false, $type = false, $api_key = '')
	{
		if (strpos($link, '?') !== false) {
			$seperator = '&';
		}
		else {
			$seperator = '?';
		}

		if ( isset($data['id_domain']) ) $id_domain = trim($data['id_domain']);
		else $id_domain = 0;

			if(!trim($api_key))
					$api_key = API_KEY;
					
			$uri = API_URL . $link . $seperator . 'api_key=' . $api_key;
			$curl = curl_init($uri);
			//echo $uri.'<br>';

		$data['api_key'] = $api_key;
		if (isset($_SESSION['id_user'])) $data['id_user'] = $_SESSION['id_user'];
		$data = json_encode($data);
		curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if ($type == 'POST') {
			if (isset($_FILES['files'])) {
				if (count($_FILES['files']['tmp_name']) > 0 AND $_POST['multiple'] == "1") {
					$count = 0;
					$_data['api_key'] = $api_key;
					foreach($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
						$_data['files[' . $count . ']'] = getCurlValue($_FILES['files']['tmp_name'][$key], $_FILES['files']['type'][$key], $_FILES['files']['name'][$key]);
						$count++;
					}

					// $_data = array('files[0]' => $files, 'files[1]' => $files, 'api_key' => $api_key);
					// return ($_data);

					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
				}
				else {
					
					
					$filename = $_FILES['file']['name'];
					$filedata = $_FILES['file']['tmp_name'];
					$filesize = $_FILES['file']['size'];

					// $curl_file = getCurlValue($_FILES['files']['tmp_name'], $_FILES['files']['type'], $_FILES['files']['name']);

					$files = array(
						'files' => getCurlValue($_FILES['files']['tmp_name'], $_FILES['files']['type'], $_FILES['files']['name']) ,
						'api_key' => $api_key,
						'id_domain' => $id_domain
					);

					// return ($files);
					$headers = array("Content-Type:multipart/form-data");
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $files);
					curl_setopt($curl, CURLOPT_INFILESIZE, $filesize);
				}
			}
			else {
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			}
		}

		if ($type == 'DELETE') {
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}

		if ($type == 'PUT') {
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}

		$curl_response = curl_exec($curl);
		
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}

		curl_close($curl);

		 //echo '<script>console.log("'.$uri.'"); console.log('.$curl_response.');</script>';

		return json_decode($curl_response);
	}
	
}
