<?php

namespace App\Http\Controllers;

use Mail;

use App;
use Request;
use Response;
use Session;
use Cookie;
use DB;
use App\Http\Controllers\Controller;
use App\Models\APICall;
use View;

class DesignersController extends Controller	
{

	public function __construct()	{
		$_POST = json_decode(file_get_contents("php://input"), true); 
	}
	
    public function index()
	{
		$data = array();
		return view('index')->with($data);
	}

    public function layout()
	{
		session_start();
		$data = array();
		$data['session'] = $_SESSION;
		return view('layout')->with($data);
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 17, 2017
		@description: 	get login user
	**/	
	public function ajax_get_login_user()	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			$login_user_array = array();
			$login_user_array['user_fullname'] = $_SESSION['fullname'];
			$login_user_array['user_pic'] = ''.API_URL.'image.php?id_user='.$_SESSION['id_user'].'&width=128&height=128&timestamp='.time();
			$data["login_user"] = $login_user_array;
			echo json_encode($data);
		}
		else	{
			$login_user_array = array();
			$login_user_array['user_fullname'] = '';
			$login_user_array['user_pic'] = '';
			$response["login_user"] = $login_user_array;
			
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response);
		}
	}
	
    public function main()
	{
		session_start();
		$data = array();
		if( $this->is_user_login() )	{
			return view('dashboard')->with($data);
		}
		else	{
			return view('login')->with($data);
			/*
			if( $this->is_email_key_valid() )	{
				return view('dashboard')->with($data);
			}
			else	{
				return view('login')->with($data);
			}
			*/
		}
	}
	
    public function page404()
	{
		session_start();
		$data = array();
		return view('page404')->with($data);
	}
	
	public function signout()
	{
		session_start();
		session_destroy();
		//$data = array();
		//return view('login')->with($data);
	}
	
    public function signin()
	{
		$data = array();
		if( $this->is_user_login() )	{
			return view('logged-in')->with($data);
		}
		else	{
			return view('login')->with($data);
		}
	}
	
    public function forgotpwd()
	{
		$data = array();
		if( $this->is_user_login() )	{
			return view('logged-in')->with($data);
		}
		else	{
			return view('forgotpwd')->with($data);
		}
	}
	
	public function signup()
	{
		$data = array();
		if( $this->is_user_login() )	{
			return view('logged-in')->with($data);
		}
		else	{
			return view('signup')->with($data);
		}
		
	}
	
	public function myprofile()	{
		if( $this->is_user_login() )	{
			return view('myprofile');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	public function pendingrequests()	{
		if( $this->is_user_login() )	{
			return view('pendingrequests');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}

	public function requestlogodetails()	{
		if( $this->is_user_login() )	{
			return view('requestlogodetails');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	public function ajax_getrequestlogodetails($id_request)	{
		if( $this->is_user_login() )	{
			$id_login_user = $_SESSION['id_user'];
			$data = (array) $this->api('/logos/get_request_logo_by_id?&id_request='.$id_request.'&id_login_user='.$id_login_user);
			echo json_encode($data);
		}
		else	{
			$response = array();
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response); 
		}
	}
	
	public function acceptedrequests()	{
		if( $this->is_user_login() )	{
			return view('acceptedrequests');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	public function completedrequests()	{
		if( $this->is_user_login() )	{
			return view('completedrequests');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	public function rejectedrequests()	{
		if( $this->is_user_login() )	{
			return view('rejectedrequests');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	public function expiredrequests()	{
		if( $this->is_user_login() )	{
			return view('expiredrequests');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	public function ajax_getrequestdesigns($status='P')	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			$id_designer = $_SESSION['id_user'];
			$page = (int) ( isset($_GET['page']) ? $_GET['page'] : 1 );
			$limit = (int) ( isset($_GET['limit']) ? $_GET['limit'] : 10 );
			$api = $this->api('/logos/get_all_request_logos_by_iddesigner_and_status?&status='.$status.'&id_designer='.$id_designer.'&page='.$page.'&limit='.$limit);
			echo json_encode($api);
		}
		/*
		else	{
			$responseObj = new stdClass();
			$responseObj->result = 'ERROR';
			$responseObj->result_code = 6;
			$responseObj->response_text = 'Get All Request Logos';
			$responseObj->response_details = 'Session Expire. You have been logged out';
			echo json_encode($responseObj);
		}
		*/
	}
	
	
	public function completeddesigns()
	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			return view('completeddesigns')->with($data);
		}
		else	{
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}

	/**
		@auhtor: 		James Bolongan
		@date: 			May 15, 2017
	**/
	public function uploadedlogo()	{
		if( $this->is_user_login() )	{
			return view('uploadedlogo');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			May 15, 2017
	**/
	public function submitlogo()	{
		if( $this->is_user_login() )	{
			return view('submitlogo');
		}
		else	{
			$data = array();
			$data['key_validation_result'] = 'Invalid Access. Please login.';
			$data['key_validation_status'] = 'Error';
			return view('pageresultmain')->with($data);
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			May 22, 2017
		@description: 	bulk upload logos
	**/
    public function ajax_upload_logos()	{
		$phpFileUploadErrors = array(
			0 => 'There is no error, the file uploaded with success',
			1 => 'The uploaded file is too large. Acceptable filesize is up to 32MB.',
			2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
			3 => 'The uploaded file was only partially uploaded',
			4 => 'No file was uploaded',
			6 => 'Missing a temporary folder',
			7 => 'Failed to write file to disk.',
			8 => 'A PHP extension stopped the file upload.',
		);
		
		if( $this->is_user_login() )	{
			$id_designer = $_SESSION['id_user'];
			$id_user = $_SESSION['id_user'];
			
			// Only accept files with these extensions
			// $whitelist = array('jpg', 'jpeg', 'png', 'gif');
			$whitelist = array('zip');
			$name      = null;
			$error     = 'No file uploaded.';
			
			if ( !empty( $_FILES ) ) {
				if ( !empty( $_FILES['file'] ) ) {
					$tmp_name = $_FILES['file']['tmp_name'];
					$name     = basename($_FILES['file']['name']);
					$error    = $_FILES['file']['error'];
					
					if ($error === UPLOAD_ERR_OK) {
						$extension = pathinfo($name, PATHINFO_EXTENSION);

						if (!in_array($extension, $whitelist)) {
							$api = $this->api('/logos/get_uploaded_logo_by_designer?&id_designer='.$id_designer);
							$error = 'Invalid file type uploaded.';
							$data['response_details'] = $error;
							$data['response_text'] = 'LOGO UPLOAD';
							$data['result'] = 'ERROR';
							$data['api_data'] = $api;
							return json_encode($data);
						} 
						else {
							$new_file_name = $this->generateRandomString().'-'.$id_user.'-'.time().'.'.$extension;

							$args['file']['id_user'] = $id_user;
							
							$link = '/logos/upload_bulk_logo_designer';
							if (strpos($link, '?') !== false) {	$seperator = '&';	}
							else {	$seperator = '?';	}
							//$curl_url = API_URL . $link . $seperator . 'api_key=' . API_KEY;
							$curl_url = API_URL . $link . $seperator . 'api_key=' . API_KEY. '&id_sender=' .$id_user . '&user_type=L' . '&web_url=' .config('app.BASE_URL') ;
							$curl = curl_init();
							curl_setopt($curl, CURLOPT_URL,$curl_url);
							curl_setopt($curl, CURLOPT_POST,1);
							//$args['file'] = new \CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $id_user.'.'.$extension);
							$args['file'] = new \CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $new_file_name);
							curl_setopt($curl, CURLOPT_POSTFIELDS, $args);
							$curl_response = curl_exec($curl);
							
							if ( $curl_response === false || $curl_response == '' ) {
								$data = curl_getinfo($curl);
								$error =  'Error occured during curl exec.';
							}
							else	{
								$error =  'Zip file successfully uploaded.';
							}
							curl_close($curl);
						}
					}
					else	{	
						$api = $this->api('/logos/get_uploaded_logo_by_designer?&id_designer='.$id_designer);
						//$error = 'Something went error';
						$error = $phpFileUploadErrors[$_FILES['file']['error']];
						$data['response_details'] = $error;
						$data['response_text'] = 'LOGO UPLOAD';
						$data['result'] = 'ERROR';
						$data['api_data'] = $api;
						return json_encode($data);
					}
					
				}
				else	{
					$api = $this->api('/logos/get_uploaded_logo_by_designer?&id_designer='.$id_designer);
					$error = 'No file being uploaded';
					$data['response_details'] = $error;
					$data['response_text'] = 'LOGO UPLOAD';
					$data['result'] = 'ERROR';
					$data['api_data'] = $api;
					return json_encode($data);
				}
			}
			else	{	
				$api = $this->api('/logos/get_uploaded_logo_by_designer?&id_designer='.$id_designer);
				$error = 'Please select a file to upload'; 
				$data['response_details'] = $error;
				$data['response_text'] = 'LOGO UPLOAD';
				$data['result'] = 'ERROR';
				$data['api_data'] = $api;
				return json_encode($data);
			}
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response);
		}
	}
	
	public function ajax_get_uploaded_logos()	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			$id_designer = $_SESSION['id_user'];
			$page = $_GET['page'];
			$limit = $_GET['limit'];
			$api = $this->api('/logos/get_uploaded_logo_by_designer?&id_designer='.$id_designer.'&page='.$page.'&limit='.$limit);
			echo json_encode($api);
		}
		/*
		else	{
			$responseObj = new stdClass();
			$responseObj->result = 'ERROR';
			$responseObj->result_code = 6;
			$responseObj->response_text = 'Get All Request Logos';
			$responseObj->response_details = 'Session Expire. You have been logged out';
			echo json_encode($responseObj);
		}
		*/
	}
	
    public function ajax_login()
	{	
		session_start();
		$api = $this->api('users/login', $_POST, 'POST');
		if($api->error == 200)	{
			if( $api->user_type != 'L' )	{
				unset($_SESSION['id_user']);
				unset($_SESSION['friendly']);
				unset($_SESSION['user_type']);
				unset($_SESSION['api_key']);
				unset($_SESSION['email']);
				unset($api->id_user);
				unset($api->friendly);
				unset($api->user_type);
				unset($api->api_key);
				unset($api->email);
				$api->error = 500;
				$api->errormsg = 'Sorry you dont have authority to access this application.';
			}
			else	{
				// If successfull login
				if($api->id_user) {
					$_SESSION['id_user'] = (int) $api->id_user;
					$_SESSION['friendly'] = $api->friendly;
					$_SESSION['type'] = $api->user_type;
					$_SESSION['api_key'] = $api->api_key;
					$_SESSION['fullname'] = $api->fullname;
					$_SESSION['first_name'] = $api->first_name;
					$_SESSION['last_name'] = $api->last_name;
					//header('Location: /'.(@$_REQUEST['h'] && trim(strtolower(@$_REQUEST['h'])) != 'out'? urldecode($_REQUEST['h']): ''));
				}
			}
		}
		echo json_encode($api);
	}
	
    public function ajax_signup()
	{	
		$input = Request::all();
		$json = array();
		
		$_data = array('first_name' => $input['firstname'], 'last_name' => $input['lastname'], 'email' => $input['email'], 'password' => $input['password'], 'identifier' => '', 'type'=> 2);
		$res = $this->api('users', $_data, 'POST');
		
		//return json_encode($res);
		
		if($res->error == 0)	{
			$data_user_info = (array) $res;
			//$data_user_info =  $input;
			$data_user_info['reg_first_name'] = $input['firstname'];
			$data_user_info['reg_last_name'] = $input['lastname'];
			$data_user_info['reg_email'] = $input['email'];
			$data_user_info['reg_password'] = $input['password'];
			
			$data_user_info['url'] = url('/');
			$data_user_info['id_user'] = $res->id_user;
			$data_user_info['friendly'] = $res->friendly;
			$data_user_info['user_type'] = $res->user_type;
			
			$json['status'] = $data_user_info;
			
			$api_res = $this->api('users/send_email_confirmation_general', $data_user_info, 'POST');
			$json['status'] = 'Success';
			$json['message'] = 'Please check your mail to verify your email address.';
			$json['api_res'] = $api_res;
		}
		else	{
			$json['status'] = 'Error';
			$json['message'] = $res->errormsg;
		}
		
		return json_encode($json);
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			February 09, 2017
		@description: 	check key and process  email confirmation
	**/
	public function process_email()	{
		session_start();
		$data_result = array();
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t confirm your email. Invalid key.';

			if( count($vars) < 4 )	{
				$data_result['key_validation_result'] = 'Can\'t confirm your email. Invalid key.';
				$data_result['key_validation_status'] = 'Error';
			}

			$user_info = $this->api('users/'.$vars[0]);				
			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 2 )	{
				$_data = array('emailconfirm' => '1', 'id_user'=> $user_info->id);
				
				$result = $this->api('users/emailconfirm', $_data, 'POST');
				$data_result['key_validation_result'] = $result->response_details;
				$data_result['key_validation_status'] = 'Success';
			}
			else	{
				$data_result['key_validation_result'] = 'Can\'t confirm your email. Invalid key.';
				$data_result['key_validation_status'] = 'Error';
			}
		}
		else	{
			$data_result['key_validation_result'] = '';
			$data_result['key_validation_status'] = '';
		}

		return view('pageresult')->with($data_result);
		
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			February 17, 2017
		@description: 	accept request through confirming the email
	**/
	public function accept_logo_request()	{
		session_start();
		$data_result = array();
		
		$key = $_REQUEST['key'];
		$key = base64_decode($key);
		$vars = explode('*_***_*',$key);

		
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t confirm the request. Invalid key.';

			if( count($vars) < 7 )	{
				$data_result['key_validation_result'] = 'Can\'t confirm the request. Invalid key.';
				$data_result['key_validation_status'] = 'Error';
			}

			$user_info = $this->api('users/'.$vars[0]);

			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 2 && $vars[4] > 0 && $vars[5] > 0 && $vars[6] == $user_info->id )	{

				
				$_data = array(
							'status' => 'C', 
							'id_user'=> $user_info->id, 
							'id_request'=> $vars[4], 
							'id_assign'=> $vars[5], 
							'id_designer'=> $vars[6], 
							'web_url'=>config('app.BASE_URL')
						);
				$result = $this->api('logos/confirm_accept_request', $_data, 'POST');

				if( $result->result == 'ERROR' )
					$data_result['key_validation_status'] = 'Error';
				else
					$data_result['key_validation_status'] = 'Success';
				
				$data_result['key_validation_result'] = $result->response_details;
			}
			else	{
				$data_result['key_validation_result'] = 'Can\'t confirm the request. Invalid key.';
				$data_result['key_validation_status'] = 'Error';
			}
		}
		else	{
			$data_result['key_validation_result'] = '';
			$data_result['key_validation_status'] = '';
		}
		return view('pageresult')->with($data_result);
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			February 17, 2017
		@description: 	reject request through confirming the email
	**/
	public function reject_logo_request()	{
		session_start();
		$data_result = array();
		
		$key = $_REQUEST['key'];
		$key = base64_decode($key);
		$vars = explode('*_***_*',$key);

		
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t reject the request. Invalid key.';

			if( count($vars) < 7 )	{
				$data_result['key_validation_result'] = 'Can\'t reject the request. Invalid key.';
				$data_result['key_validation_status'] = 'Error';
			}

			$user_info = $this->api('users/'.$vars[0]);

			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 2 && $vars[4] > 0 && $vars[5] > 0 && $vars[6] == $user_info->id )	{

				$_data = array(
							'status' => 'R', 
							'id_user'=> $user_info->id, 
							'id_request'=> $vars[4], 
							'id_assign'=> $vars[5], 
							'id_designer'=> $vars[6],
							'web_url'=>config('app.BASE_URL')
						);
				$result = $this->api('logos/confirm_reject_request', $_data, 'POST');

				if( $result->result == 'ERROR' )
					$data_result['key_validation_status'] = 'Error';
				else
					$data_result['key_validation_status'] = 'Success';
				
				$data_result['key_validation_result'] = $result->response_details;
			}
			else	{
				$data_result['key_validation_result'] = 'Can\'t reject the request. Invalid key.';
				$data_result['key_validation_status'] = 'Error';
			}
		}
		else	{
			$data_result['key_validation_result'] = '';
			$data_result['key_validation_status'] = '';
		}
		return view('pageresult')->with($data_result);
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 12, 2017
		@description: 	accept request
	**/
    public function ajax_accept_request()
	{	
		session_start();
		$input = Request::all();
		$json = array();
		$data = array();
		if( $this->is_user_login() )	{
			$post = json_decode(key($input));	//instead of $_POST
			$id_user = $_SESSION['id_user'];
			$_data = array(
						'status' => 'C', 
						'id_request'=> $_POST['id_request'], 
						'id_assign'=> $_POST['id_assign'], 
						'id_designer'=>$id_user, 
						'web_url'=>config('app.BASE_URL'),
						'from_front_end'=>1, 
					);
			$result = $this->api('logos/confirm_accept_request', $_data, 'POST');
			return json_encode($result); 
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response); 
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 13, 2017
		@description: 	reject request
	**/
    public function ajax_reject_request()
	{	
		session_start();
		$input = Request::all();
		$json = array();
		$data = array();
		if( $this->is_user_login() )	{
			$post = json_decode(key($input));	//instead of $_POST
			$id_user = $_SESSION['id_user'];
			$_data = array(
						'status' => 'R', 
						'id_request'=> $_POST['id_request'], 
						'id_assign'=> $_POST['id_assign'], 
						'id_designer'=> $id_user, 
						'web_url'=>config('app.BASE_URL'),
						'from_front_end'=>1, 
					);
			$result = $this->api('logos/confirm_reject_request', $_data, 'POST');
			
			return json_encode($result); 
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response); 
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 15, 2017
		@description: 	send comment or message
	**/
    public function ajax_send_comment()
	{	
		session_start();
		$input = Request::all();
		$json = array();
		$data = array();
		if( $this->is_user_login() )	{
			$post = json_decode(key($input));	//instead of $_POST
			$id_user = $_SESSION['id_user'];
			$_data = array(
						'id_request'=> $_POST['txt_id_request'], 
						'id_checklist'=> 0,
						'id_sender'=> $id_user, 
						'user_type'=> 'L', 
						'domain'=> $_POST['txt_domain'], 
						'comment'=> $_POST['txt_comment'], 
						'web_url'=>config('app.BASE_URL'),
					);
			$result = $this->api('logos/send_comment_request', $_data, 'POST');
			return json_encode($result);
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response); 
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 22, 2017
		@description: 	download file
	**/
	public function ajax_downloadfile()	{
		session_start();
		$input = Request::all();
		$json = array();
		$data = array();
		if( $this->is_user_login() )	{
			$filename = $_GET['file'];
			$curl_post_data = array('file' => $filename);
			$link = '/logos/download_logo_file';
			if (strpos($link, '?') !== false) {	$seperator = '&';	}
			else {	$seperator = '?';	}
			
			$curl_url = API_URL . $link . $seperator . 'api_key=' . API_KEY . '&file=' .$filename;
			$headers = array('Content-type: application/json','Authorization: '.API_KEY,);
			$fp = fopen (dirname(__FILE__) . '/localfile.tmp', 'w+'); //This is the file where we save the information

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL,$curl_url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post_data));
			curl_setopt($curl, CURLOPT_USERPWD, API_KEY);
			$file = curl_exec($curl);
			if ($file === false) 
			{
				$info = curl_getinfo($curl);
				curl_close($curl);
				die('error occured during curl exec. Additioanl info: ' . var_export($info));
			}
			curl_close($curl);
			
			if( $file != '' )	{
				header('Content-type: ' . 'application/octet-stream');
				header('Content-Disposition: ' . 'attachment; filename='.$filename.'');
				echo $file;
			}
			else	{
				echo 'File does not exist.';
			}
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response); 
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 21, 2017
		@description: 	upload messages files
	**/
    public function ajax_upload_messages_files()
	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			$post = json_decode(key($input));	//instead of $_POST
			$comment = trim($_GET['comment']);
			$domain = trim($_GET['domain']);
			$id_user = $_SESSION['id_user'];
			$id_request = (int) $_GET['id_request'];
			$id_assign = (int) $_GET['id_assign'];
			
			// Only accept files with these extensions
			$whitelist = array('jpg', 'jpeg', 'png', 'gif');
			$name      = null;
			$error     = 'No logo uploaded.';
			
			//if (isset($_FILES)) {
			if ( !empty( $_FILES ) ) {
				//if (isset($_FILES['file'])) {
				if ( !empty( $_FILES['file'] ) ) {
					$tmp_name = $_FILES['file']['tmp_name'];
					$name     = basename($_FILES['file']['name']);
					$error    = $_FILES['file']['error'];
					
					if ($error === UPLOAD_ERR_OK) {
						$extension = pathinfo($name, PATHINFO_EXTENSION);

						if (!in_array($extension, $whitelist)) {
							$data = (array) $this->api('/logos/get_request_logo_by_id?&id_request='.$id_request.'&id_login_user='.$id_user);
							$error = 'Invalid file type uploaded.';
							$data['response_details'] = $error;
							$data['response_text'] = 'LOGO UPLOAD';
							$data['result'] = 'ERROR';
							return json_encode($data);
						} 
						else {
							$new_file_name = $this->generateRandomString().'-'.$id_user.'-'.time().'.'.$extension;

							$args['file']['id_user'] = $id_user;
							
							$link = '/logos/upload_file';
							if (strpos($link, '?') !== false) {	$seperator = '&';	}
							else {	$seperator = '?';	}
							//$curl_url = API_URL . $link . $seperator . 'api_key=' . API_KEY;
							$curl_url = API_URL . $link . $seperator . 'api_key=' . API_KEY. '&id_sender=' .$id_user . '&id_request=' .$id_request . '&id_assign=' .$id_assign . '&comment=' .$comment . '&user_type=L' . '&domain=' .$domain . '&web_url=' .config('app.BASE_URL') ;
							$curl = curl_init();
							curl_setopt($curl, CURLOPT_URL,$curl_url);
							curl_setopt($curl, CURLOPT_POST,1);
							//$args['file'] = new \CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $id_user.'.'.$extension);
							$args['file'] = new \CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $new_file_name);
							curl_setopt($curl, CURLOPT_POSTFIELDS, $args);
							$curl_response = curl_exec($curl);
							
							if ( $curl_response === false || $curl_response == '' ) {
								$data = curl_getinfo($curl);
								$error =  'Error occured during curl exec.';
							}
							else	{
								$error =  'Avatar successfully uploaded.';
							}
							curl_close($curl);
						}
					}
					else	{	
						$data = (array) $this->api('/logos/get_request_logo_by_id?&id_request='.$id_request.'&id_login_user='.$id_user);
						$error = 'Something went error';
						$data['response_details'] = $error;
						$data['response_text'] = 'LOGO UPLOAD';
						$data['result'] = 'ERROR';
						return json_encode($data);
					}
				}
				else	{	
					$data = (array) $this->api('/logos/get_request_logo_by_id?&id_request='.$id_request.'&id_login_user='.$id_user);
					$error = 'No file being uploaded';
					$data['response_details'] = $error;
					$data['response_text'] = 'LOGO UPLOAD';
					$data['result'] = 'ERROR';
					return json_encode($data);
				}
			}
			else	{	
				$data = (array) $this->api('/logos/get_request_logo_by_id?&id_request='.$id_request.'&id_login_user='.$id_user);
				$error = 'Please select a file to upload'; 
				$data['response_details'] = $error;
				$data['response_text'] = 'LOGO UPLOAD';
				$data['result'] = 'ERROR';
				return json_encode($data);
			}
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response);
		}
	}
	
	public function generateRandomString($length = 10) {
		return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	}
	
    public function ajax_save_settings_info()
	{	
		session_start();
		$input = Request::all();
		$json = array();
		
		$id_user = $_SESSION['id_user'];
		$no_of_pending_logo_can_handle = $_POST['no_pending_designs'];
		if( isset($_POST['is_available']) )
			$is_available = 1;
		else
			$is_available = 0;
		
		$_data = array();
		$_data['id_user'] = $id_user;
		$_data['no_of_pending_logo_can_handle'] = $no_of_pending_logo_can_handle;
		$_data['is_available'] = $is_available;
		
		$api_res = $this->api('logos/save_designer_settings', $_data, 'POST');
		return json_encode($api_res);
	}
	
	public function ajax_get_settings_info()	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			$id_designer = $_SESSION['id_user'];
			$api = $this->api('/logos/get_designer_settings/'.$id_designer);
			echo json_encode($api);
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response);
		}
	}

	/**
		@auhtor: 		James Bolongan
		@date: 			March 17, 2017
		@description: 	get basic profile info
	**/	
	public function ajax_get_basic_info()	{
		$input = Request::all();
		$data = array();
		if( $this->is_user_login() )	{
			$id_designer = $_SESSION['id_user'];			
			$api = $this->api('/userprofile/get/'.$id_designer);
			echo json_encode($api);
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response);
		}
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			March 16, 2017
		@description: 	upload profile pic
	**/
    public function ajax_upload_profile_pic()
	{	
		session_start();
		$input = Request::all();
		$json = array();
		$data = array();
		if( $this->is_user_login() )	{
			$post = json_decode(key($input));	//instead of $_POST
			$id_user = $_SESSION['id_user'];

			// Only accept files with these extensions
			$whitelist = array('jpg', 'jpeg', 'png', 'gif');
			$name      = null;
			$error     = 'No file uploaded.';
			
			//if (isset($_FILES)) {
			if ( !empty( $_FILES ) ) {
				//if (isset($_FILES['file'])) {
				if ( !empty( $_FILES['file'] ) ) {
					$tmp_name = $_FILES['file']['tmp_name'];
					$name     = basename($_FILES['file']['name']);
					$error    = $_FILES['file']['error'];
					
					if ($error === UPLOAD_ERR_OK) {
						$extension = pathinfo($name, PATHINFO_EXTENSION);

						if (!in_array($extension, $whitelist)) {
							$error = 'Invalid file type uploaded.';
							$data['response_details'] = $error;
							$data['response_text'] = 'AVATAR UPLOAD';
							$data['result'] = 'ERROR';
							return json_encode($data);
						} 
						else {
							
							$args['file']['id_user'] = $id_user;
							
							$link = '/users/upload_avatar';
							if (strpos($link, '?') !== false) {	$seperator = '&';	}
							else {	$seperator = '?';	}
							$curl_url = API_URL . $link . $seperator . 'api_key=' . API_KEY;
							$curl = curl_init();
							curl_setopt($curl, CURLOPT_URL,$curl_url);
							curl_setopt($curl, CURLOPT_POST,1);
							$args['file'] = new \CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $id_user.'.'.$extension);
							curl_setopt($curl, CURLOPT_POSTFIELDS, $args);
							$curl_response = curl_exec($curl);
							
							if ( $curl_response === false || $curl_response == '' ) {
								$data = curl_getinfo($curl);
								//die('error occured during curl exec. Additioanl info: ' . var_export($info));
								$error =  'Error occured during curl exec.';
							}
							else	{
								$error =  'Avatar successfully uploaded.';
							}
							curl_close($curl);
						
							//$api = api_call( '/users/upload_avatar', $_FILES, 'POST' );
						}
					}
					else	{	
						$error = 'Something went error';
						$data['response_details'] = $error;
						$data['response_text'] = 'AVATAR UPLOAD';
						$data['result'] = 'ERROR';
						return json_encode($data);
					}
				}
				else	{	
					$error = 'No file being uploaded';
					$data['response_details'] = $error;
					$data['response_text'] = 'AVATAR UPLOAD';
					$data['result'] = 'ERROR';
					return json_encode($data);
				}
			}
			else	{	
				$error = 'Please select a file to upload'; 
				$data['response_details'] = $error;
				$data['response_text'] = 'AVATAR UPLOAD';
				$data['result'] = 'ERROR';
				return json_encode($data);
			}
			
		}
		else	{
			$response["result"]="ERROR";
			$response["result_code"]='No Access';
			$response["response_text"]="PERMISSION ERROR";
			$response["response_details"]="Invalid access. Pleas wait, you will be redirected to the main page.";
			return json_encode($response); 
		}
	}
	
    public function phpinfo()
	{
		phpinfo();
	}
	
    public function testcheckDifference()
	{
		echo 'Hello world';
	}

}